var fs = require('fs');

module.exports = function (grunt) {

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        copy: {
            extensionDev: {
                files: [
                    {
                        expand: true,
                        cwd: 'src/extension',
                        src: ['**', '!prod/**', '../common/*.js'],
                        flatten: true,
                        dest: 'build/extensionDev/'
                    }, {
                        expand: true,
                        cwd: 'src/vendors/common',
                        src: ['**'],
                        dest: 'build/extensionDev/vendors/'
                    }
                ]
            },
            extensionProd: {
                files: [
                    {
                        expand: true,
                        cwd: 'src/extension',
                        src: ['**', '!dev/**', '../common/*.js'],
                        flatten: true,
                        dest: 'build/extensionProd/'
                    }, {
                        expand: true,
                        cwd: 'src/vendors/common',
                        src: ['**'],
                        dest: 'build/extensionProd/vendors/'
                    }
                ]
            },
            pipedriveBridge: {
                files: [
                    {
                        expand: true,
                        cwd: 'src/vendors/common/',
                        src: ['font-awesome.min.css', 'fontawesome-webfont.woff'],
                        dest: 'build/pipedriveBridge/cdn/css/'
                    }, {
                        expand: true,
                        cwd: 'src/bridge',
                        src: ['injectBootloader.js'],
                        dest: 'build/pipedriveBridge/'
                    }, {
                        expand: true,
                        cwd: 'src/pipedriveBridge',
                        src: '*',
                        dest: 'build/pipedriveBridge/'
                    }
                ]
            },
            embedScriptBridge: {
                files: [
                    {
                        expand: true,
                        cwd: 'src/vendors/common',
                        src: ['font-awesome.min.css', 'fontawesome-webfont.woff'],
                        dest: 'build/pipedriveBridge/cdn/css/'
                    }, {
                        expand: true,
                        cwd: 'src/bridge',
                        src: '*',
                        dest: 'build/embedScriptBridge/'
                    }
                ]
            },

            moveEmbedScriptDevToServer: {
                expand: true,
                cwd: 'build/embedScriptDev/',
                src: ['embedScript.js', 'bootloader.js'],
                dest: '../static-www.kuoll.com/generated/unminified/cdn/embedScript/dev/'
            },
            moveEmbedScriptProdToServer: {
                expand: true,
                cwd: 'build/embedScriptProd/',
                src: ['embedScript.js', 'bootloader.js'],
                dest: '../static-www.kuoll.com/generated/unminified/cdn/embedScript/prod/'
            },

            moveEmbedScriptProdToPipedriveBridge: {
                expand: true,
                cwd: 'build/embedScriptProd/',
                src: ['*'],
                dest: 'build/pipedriveBridge/cdn/embedScript/dev/'
            },
            moveEmbedScriptProdToEmbedScriptBridge: {
                expand: true,
                cwd: 'build/embedScriptDev/',
                src: ['*'],
                dest: 'build/embedScriptBridge/cdn/embedScript/dev/'
            },

            moveEmbedPopupToServer: {
                expand: true,
                cwd: 'build/common',
                src: ['embedPopup.html'],
                dest: '../static-www.kuoll.com/generated/cdn/'
            },
            moveEmbedPopupToPipedriveBridge: {
                expand: true,
                cwd: 'build/common',
                src: ['embedPopup.html'],
                dest: 'build/pipedriveBridge/'
            },
            moveEmbedPopupToEmbedScriptBridge: {
                expand: true,
                cwd: 'build/common',
                src: ['embedPopup.html'],
                dest: 'build/embedScriptBridge/cdn/'
            },

            copyBootloaderToMonitoringJs: {
                expand: true,
                cwd: 'build/embedScriptProd',
                src: ['bootloader.js'],
                dest: 'build/embedScriptProd/',
                rename: (dest) => dest + 'monitoring.js'
            }
        },

        eslint: {
            target: [
                'Gruntfile.js', 'src/**/*.js', '!src/vendors/**'],
            options: {
                quiet: true
            }
        },

        concat: {
            injectScript: {
                src: ['src/common/sequentCycles.js', 'src/common/utils.js', 'src/common/logging.js', 'src/extension/channel.js',
                    'src/vendors/common/MutationObserver/*.js', 'src/common/MutationSaver.js', 'src/common/MouseCapturer.js',
                    'src/common/mutation-summary/*.js', 'src/common/reportError.js', 'src/common/ignoring.js',
                    'src/common/SequentSaver.js', 'src/vendors/common/stacktrace.js/stacktrace.js',
                    'src/vendors/common/stacktrace.js/source-map.js',
                    'src/vendors/common/stacktrace.js/stacktrace-gps.js', 'src/common/domTools.js',
                    'src/common/ExceptionDetection.js', 'src/common/embedScript.js', 'src/common/origObjects.js',
                    'src/extension/injectScript/injectBridge.js', 'src/extension/injectScript/init.js'],
                dest: 'build/common/injectScript.js'
            },
            injectScriptDev: {
                src: ['src/extension/dev/config.js', 'build/common/injectScript.js'],
                dest: 'build/extensionDev/injectScript.js',
                options: {
                    banner: '(function(){\n',
                    footer: '\n}());'
                }
            },
            injectScriptProd: {
                src: ['src/extension/prod/config.js', 'build/common/injectScript.js'],
                dest: 'build/extensionProd/injectScript.js',
                options: {
                    banner: '(function(){\n',
                    footer: '\n}());'
                }
            },

            contentScript: {
                src: ['src/common/reportError.js', 'src/extension/channel.js', 'src/common/utils.js', 'src/common/domTools.js',
                    'src/extension/contentScript/contentScript.js'],
                dest: 'build/common/contentScript.js'
            },
            contentScriptDev: {
                src: ['src/extension/dev/config.js', 'build/common/contentScript.js'],
                dest: 'build/extensionDev/contentScript.js'
            },
            contentScriptProd: {
                src: ['src/extension/prod/config.js', 'build/common/contentScript.js'],
                dest: 'build/extensionProd/contentScript.js'
            },

            embedScript: {
                src: ['src/common/**/*.js', 'src/embedScript/*.js', '!src/embedScript/bootloader.js',
                    'src/vendors/embedScript/**/*.js', 'src/vendors/common/bignumber/*.js',
                    'src/vendors/common/cryptojs/*.js', 'src/vendors/common/MutationObserver/*.js',
                    'src/vendors/common/stacktrace.js/stacktrace.js', 'src/vendors/common/stacktrace.js/source-map.js',
                    'src/vendors/common/stacktrace.js/stacktrace-gps.js', 'src/embedScript/init/init.js'],
                dest: 'build/common/embedScript.js'
            },
            embedScriptDev: {
                src: ['src/special/closureStart.js', 'src/embedScript/dev/config.js',
                    'build/common/embedScript.js', 'src/special/closureEnd.js'],
                dest: 'build/embedScriptDev/embedScript.js'
            },
            embedScriptProd: {
                src: ['src/special/closureStart.js', 'src/embedScript/prod/config.js',
                    'build/common/embedScript.js','src/special/closureEnd.js'],
                dest: 'build/embedScriptProd/embedScript.js'
            },

            bootloader: {
                src: ['src/common/origObjects.js', 'src/embedScript/polyfill.js', 'src/vendors/ie8/*.js',
                    'src/common/reportError.js', 'src/vendors/common/stacktrace.js/stacktrace.js', 'src/common/utils.js',
                    'src/vendors/common/cryptojs/**/*.js', 'src/vendors/common/bignumber/**/*.js',
                    'src/embedScript/bootloader.js'],
                dest: 'build/common/bootloader.js'
            },
            bootloaderDev: {
                src: ['src/embedScript/dev/config.js', 'build/common/bootloader.js'],
                dest: 'build/embedScriptDev/bootloader.js',
                options: {
                    banner: '(function(){ "use strict";\n',                                                                 
                    footer: '\n}());'
                }
            },
            bootloaderProd: {
                src: ['src/embedScript/prod/config.js', 'build/common/bootloader.js'],
                dest: 'build/embedScriptProd/bootloader.js',
                options: {
                    banner: '(function(){ "use strict";\n',
                    footer: '\n}());'
                }
            },
            bootloaderPipedriveBridge: {
                src: ['src/pipedriveBridge/config.js', 'build/common/bootloader.js'],
                dest: 'build/pipedriveBridge/bootloader.js',
                options: {
                    banner: '(function(){ "use strict";\n',
                    footer: '\n}());'
                }
            },
            bootloaderEmbedScriptBridge: {
                src: ['src/bridge/config.js', 'build/common/bootloader.js'],
                dest: 'build/embedScriptBridge/bootloader.js',
                options: {
                    banner: '(function(){ "use strict";\n',
                    footer: '\n}());'
                }
            },

            embedPopup: {
                src: ['src/embedScript/popup/styleTagStart.html', 'src/embedScript/popup/embedPopup.css',
                    'src/embedScript/popup/styleTagEnd.html', 'src/embedScript/popup/embedPopup.html'],
                dest: 'build/common/embedPopup.html'
            },
            embedPopupJs: {
                src: ['src/common/origObjects.js', 'src/common/reportError.js',
                    'src/embedScript/popup/embedPopup.js'],
                dest: 'build/common/embedPopup.js'
            },

            moveEmbedPopupJsDevToServer: {
                src: ['src/embedScript/dev/config.js', 'build/common/embedPopup.js'],
                dest: '../static-www.kuoll.com/generated/cdn/embedScript/dev/embedPopup.js'
            },
            moveEmbedPopupJsProdToServer: {
                src: ['src/embedScript/prod/config.js', 'build/common/embedPopup.js'],
                dest: '../static-www.kuoll.com/generated/cdn/embedScript/prod/embedPopup.js'
            },

            bridgeBootloader: {
                src: ['src/bridge/config.js', 'src/bridge/injectBootloader.js'],
                dest: 'build/embedScriptBridge/injectBootloader.js'
            },
            pipedriveBridgeBootloader: {
                src: ['src/pipedriveBridge/config.js', 'src/bridge/injectBootloader.js'],
                dest: 'build/pipedriveBridge/injectBootloader.js'
            },

            bridgeEmbedScript: {
                src: ['src/bridge/config.js', 'src/bridge/embedScriptBridge.js'],
                dest: 'build/embedScriptBridge/embedScriptBridge.js'
            },
            pipedriveBridgeEmbedScript: {
                src: ['src/pipedriveBridge/config.js', 'src/bridge/embedScriptBridge.js'],
                dest: 'build/pipedriveBridge/embedScriptBridge.js'
            },

            bridgeInjectScript: {
                src: ['src/bridge/config.js', 'src/bridge/injectScript.js'],
                dest: 'build/embedScriptBridge/injectScript.js'
            }
        },


        watch: {

            npm: {
                files: ['package.json'],
                tasks: ["auto_install"],
                options: {
                    atBegin: true
                }
            },

            extension: {
                files: ['src/extension/**/*.*'],
                tasks: ['buildExtensionDev', 'buildExtensionProd']
            },
            extensionDev: {
                files: ['src/extension/dev/**.*'],
                tasks: ['buildExtensionDev']
            },
            extensionProd: {
                files: ['src/extension/prod/**.*'],
                tasks: ['buildExtensionProd']
            },

            embedScript: {
                files: ['src/embedScript/**/*.*'],
                tasks: ['buildEmbedScriptDev', 'buildEmbedScriptProd']
            },
            embedScriptDev: {
                files: ['src/embedScript/dev/**.*'],
                tasks: ['buildEmbedScriptDev']
            },
            embedScriptProd: {
                files: ['src/embedScript/prod/**.*'],
                tasks: ['buildEmbedScriptProd']
            },

            pipedriveBridge: {
                files: ['src/pipedriveBridge/**.*'],
                tasks: ['buildPipedriveBridge']
            },

            embedScriptBridge: {
                files: ['src/bridge/**.*'],
                tasks: ['buildEmbedScriptBridge']
            },

            jsdoc: {
                files: ['src/embedScript/bootloader.js'],
                tasks: ['jsdoc'],
                options: {
                    atBegin: true
                }
            },

            common: {
                files: ['src/common/**', 'src/vendors/**', 'Gruntfile.js'],
                tasks: ['default'],

                options: {
                    atBegin: true
                }
            }

        },

        uglify: {
            pipedriveEmbedScript: {
                expand: true,
                src: ['build/pipedriveBridge/cdn/embedScript/dev/*.js', '!**/*.min.js'],
                ext: ".min.js",
                extDot: 'last'
            },

            cdn: {
                expand: true,
                src: ['build/embedScriptProd/**/*.js', '!build/embedScriptProd/**/*.min.js']
            }
        },

        compress: {
            extensionProd: {
                options: {
                    archive: 'build/deploy/extension.zip'
                },
                files: [
                    {
                        expand: true,
                        cwd: "build/extensionProd",
                        src: ['**/*.*'],
                        dest: "/"
                    }
                ]
            }
        },


        jsdoc: {
            dist: {
                src: ['src/embedScript/bootloader.js'],
                options: {
                    destination: '../static-www.kuoll.com/generated/js-doc'
                }
            }
        },

        auto_install: {
            local: {
                options: {
                    bower: false
                }
            }
        },

        newer: {
            options: {
                // Fix of bug https://github.com/tschaub/grunt-newer/issues/40#issuecomment-132520453
                override: function(details, include) {
                    //check if the file was created after details.time and force run
                    var stats = fs.statSync(details.path);
                    include(stats.ctime.getTime() > details.time.getTime());
                }
            }
        },

        buildnumber: {
            files: ['version.json']
        },

        replace: {
            dist: {
                options: {
                    patterns: [{
                        match: 'BUILD_NUMBER',
                        replacement: "<%= grunt.file.readJSON('version.json').build %>"
                    }]
                },
                files: [{
                    expand: true,
                    src: ['build/**/*.js']
                }]
            }
        },


        clean: {
            build: ['build/'],

            jsdoc: {
                src: '../static-www.kuoll.com/generated/js-doc/',
                options: {
                    force: true
                }
            },

            serverDev: {
                src: '../static-www.kuoll.com/generated/cdn/embedScript/dev/',
                options: {
                    force: true
                }
            },
            serverProd: {
                src: '../static-www.kuoll.com/generated/cdn/embedScript/prod/',
                options: {
                    force: true
                }
            }
        }


        }
    );

    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-compress');
    grunt.loadNpmTasks('grunt-eslint');
    grunt.loadNpmTasks('grunt-jsdoc');
    grunt.loadNpmTasks('grunt-newer');
    grunt.loadNpmTasks('grunt-replace');                                                            
    grunt.loadNpmTasks('grunt-build-number');
    grunt.loadNpmTasks('grunt-auto-install');

    grunt.registerTask('default', ['buildEmbedPopupDev', 'buildEmbedPopupProd', 'buildEmbedScriptDev', 'buildEmbedScriptBridge',
        'buildEmbedScriptProd', 'buildExtensionDev', 'buildExtensionProd', 'buildPipedriveBridge']);
    grunt.registerTask('rebuild', ['clean', 'newer:default']);
    grunt.registerTask('buildExtensionDev', ['newer:copy:extensionDev', 'newer:concat:injectScript',
        'newer:concat:contentScript', 'newer:concat:injectScriptDev', 'newer:concat:contentScriptDev']);
    grunt.registerTask('buildExtensionProd', ['newer:copy:extensionProd', 'newer:concat:injectScript',
        'newer:concat:contentScript', 'newer:concat:injectScriptProd', 'newer:concat:contentScriptProd', 'compress:extensionProd']);
    grunt.registerTask('buildEmbedScriptDev', ['newer:concat:embedScript', 'newer:concat:bootloader', 'buildEmbedPopupDev', 'newer:concat:embedScriptDev', 'newer:concat:bootloaderDev',
        'newer:copy:moveEmbedScriptDevToServer']);
    grunt.registerTask('buildEmbedScriptProd', ['newer:concat:embedScript', 'newer:concat:bootloader', 'buildEmbedPopupProd', 'newer:concat:embedScriptProd', 'newer:concat:bootloaderProd',
        'newer:copy:moveEmbedScriptProdToServer', 'newer:copy:moveEmbedScriptProdToPipedriveBridge']);
    grunt.registerTask('buildPipedriveBridge', ['newer:copy:pipedriveBridge', 'newer:concat:bootloader', 'newer:concat:bootloaderPipedriveBridge']);
    grunt.registerTask('buildEmbedScriptBridge', ['newer:concat:bootloader', 'newer:copy:embedScriptBridge', 'newer:concat:bootloaderEmbedScriptBridge', 'newer:concat:bridgeEmbedScript', 'newer:concat:bridgeBootloader', 'newer:concat:bridgeInjectScript']);
    grunt.registerTask('buildEmbedPopupDev', ['newer:concat:embedPopup', 'newer:concat:embedPopupJs',
        'newer:copy:moveEmbedPopupToServer', 'concat:moveEmbedPopupJsDevToServer', 'newer:copy:moveEmbedPopupToPipedriveBridge']);
    grunt.registerTask('buildEmbedPopupProd', ['newer:concat:embedPopup', 'newer:concat:embedPopupJs',
        'newer:copy:moveEmbedPopupToServer', 'concat:moveEmbedPopupJsProdToServer', 'newer:copy:moveEmbedPopupToPipedriveBridge']);

    // TODO dk, vlad: configure cloudfont_invalidate
    grunt.registerTask('deploy', ['buildnumber', 'replace', 'uglify:cdn', 'copy:copyBootloaderToMonitoringJs', 'aws_s3', /*, 'cloudfront_invalidate'*/]);

};
