var domTools = {

    getWholeDom: function () {
        return document.body.outerHTML;
    },

    saveDomSnapshot: function () {
        var index = localStorage.historyIndex || 0;
        localStorage["historyItem-" + index] = this.getWholeDom();
        localStorage.historyIndex = index + 1;
    },

    stepBack: function () {
        var history = this.history;
        document.body.outerHTML = history[history.length - 1];
    }
};
