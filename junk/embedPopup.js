var Popup = (function () {
    //TODO @Vlad move this to server, this code is not part of extension

    var server = config.resourcesServer;

    var kuollTools;

    var popupShown = false;
    var popupBtn;
    var popupIframe;

    var userId, orgIds, recordCode;

    function togglePopup(e) {
        if (popupShown) {
            popupIframe.style.display = "none";
        } else {
            popupIframe.style.display = "";
        }
        popupShown = !popupShown;
        e.stopPropagation();
    }

    function createIframe() {
        popupIframe = document.createElement("iframe");

        //TODO @dk rename embed* stuff.
        //
        var popupSrc = server + "embedPopup.html?userId=" + userId + "&orgIds=" + orgIds + "&recordCode=" + recordCode;
        popupIframe.setAttribute("src", popupSrc);
        popupIframe.setAttribute("id", "kuollPopupIframe");
        var css = 'position: fixed; left: 10px; bottom: 50px; width: 236px; z-index: 9999; border-radius: 1px; border: #C2C8CD 1px solid; ';
        popupIframe.setAttribute("style", css);
        popupIframe.style.display = "none";
        kuollTools.appendChild(popupIframe);
    }

    function createPopup(_userId, _orgIds, _recordCode) {
        if (!_userId || !_orgIds || !_recordCode) {
            throw new Error("userId, orgIds and recordCode must be specified");
        }
        userId = _userId;
        orgIds = _orgIds;
        recordCode = _recordCode;

        if (typeof kuollTools != "undefined") {
            if (kuollTools.style.display == "none") {
                kuollTools.style.display = "block";
            } else {
                recordIsStarted(recordCode);
            }
            return;
        }

        popupBtn = document.createElement("div");
        popupBtn.setAttribute("style",
            'background: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAKQWlDQ1BJQ0MgUHJvZmlsZQAASA2dlndUU9kWh8+9N73QEiIgJfQaegkg0jtIFQRRiUmAUAKGhCZ2RAVGFBEpVmRUwAFHhyJjRRQLg4Ji1wnyEFDGwVFEReXdjGsJ7601896a/cdZ39nnt9fZZ+9917oAUPyCBMJ0WAGANKFYFO7rwVwSE8vE9wIYEAEOWAHA4WZmBEf4RALU/L09mZmoSMaz9u4ugGS72yy/UCZz1v9/kSI3QyQGAApF1TY8fiYX5QKUU7PFGTL/BMr0lSkyhjEyFqEJoqwi48SvbPan5iu7yZiXJuShGlnOGbw0noy7UN6aJeGjjAShXJgl4GejfAdlvVRJmgDl9yjT0/icTAAwFJlfzOcmoWyJMkUUGe6J8gIACJTEObxyDov5OWieAHimZ+SKBIlJYqYR15hp5ejIZvrxs1P5YjErlMNN4Yh4TM/0tAyOMBeAr2+WRQElWW2ZaJHtrRzt7VnW5mj5v9nfHn5T/T3IevtV8Sbsz55BjJ5Z32zsrC+9FgD2JFqbHbO+lVUAtG0GQOXhrE/vIADyBQC03pzzHoZsXpLE4gwnC4vs7GxzAZ9rLivoN/ufgm/Kv4Y595nL7vtWO6YXP4EjSRUzZUXlpqemS0TMzAwOl89k/fcQ/+PAOWnNycMsnJ/AF/GF6FVR6JQJhIlou4U8gViQLmQKhH/V4X8YNicHGX6daxRodV8AfYU5ULhJB8hvPQBDIwMkbj96An3rWxAxCsi+vGitka9zjzJ6/uf6Hwtcim7hTEEiU+b2DI9kciWiLBmj34RswQISkAd0oAo0gS4wAixgDRyAM3AD3iAAhIBIEAOWAy5IAmlABLJBPtgACkEx2AF2g2pwANSBetAEToI2cAZcBFfADXALDIBHQAqGwUswAd6BaQiC8BAVokGqkBakD5lC1hAbWgh5Q0FQOBQDxUOJkBCSQPnQJqgYKoOqoUNQPfQjdBq6CF2D+qAH0CA0Bv0BfYQRmALTYQ3YALaA2bA7HAhHwsvgRHgVnAcXwNvhSrgWPg63whfhG/AALIVfwpMIQMgIA9FGWAgb8URCkFgkAREha5EipAKpRZqQDqQbuY1IkXHkAwaHoWGYGBbGGeOHWYzhYlZh1mJKMNWYY5hWTBfmNmYQM4H5gqVi1bGmWCesP3YJNhGbjS3EVmCPYFuwl7ED2GHsOxwOx8AZ4hxwfrgYXDJuNa4Etw/XjLuA68MN4SbxeLwq3hTvgg/Bc/BifCG+Cn8cfx7fjx/GvyeQCVoEa4IPIZYgJGwkVBAaCOcI/YQRwjRRgahPdCKGEHnEXGIpsY7YQbxJHCZOkxRJhiQXUiQpmbSBVElqIl0mPSa9IZPJOmRHchhZQF5PriSfIF8lD5I/UJQoJhRPShxFQtlOOUq5QHlAeUOlUg2obtRYqpi6nVpPvUR9Sn0vR5Mzl/OX48mtk6uRa5Xrl3slT5TXl3eXXy6fJ18hf0r+pvy4AlHBQMFTgaOwVqFG4bTCPYVJRZqilWKIYppiiWKD4jXFUSW8koGStxJPqUDpsNIlpSEaQtOledK4tE20Otpl2jAdRzek+9OT6cX0H+i99AllJWVb5SjlHOUa5bPKUgbCMGD4M1IZpYyTjLuMj/M05rnP48/bNq9pXv+8KZX5Km4qfJUilWaVAZWPqkxVb9UU1Z2qbapP1DBqJmphatlq+9Uuq43Pp893ns+dXzT/5PyH6rC6iXq4+mr1w+o96pMamhq+GhkaVRqXNMY1GZpumsma5ZrnNMe0aFoLtQRa5VrntV4wlZnuzFRmJbOLOaGtru2nLdE+pN2rPa1jqLNYZ6NOs84TXZIuWzdBt1y3U3dCT0svWC9fr1HvoT5Rn62fpL9Hv1t/ysDQINpgi0GbwaihiqG/YZ5ho+FjI6qRq9Eqo1qjO8Y4Y7ZxivE+41smsImdSZJJjclNU9jU3lRgus+0zwxr5mgmNKs1u8eisNxZWaxG1qA5wzzIfKN5m/krCz2LWIudFt0WXyztLFMt6ywfWSlZBVhttOqw+sPaxJprXWN9x4Zq42Ozzqbd5rWtqS3fdr/tfTuaXbDdFrtOu8/2DvYi+yb7MQc9h3iHvQ732HR2KLuEfdUR6+jhuM7xjOMHJ3snsdNJp9+dWc4pzg3OowsMF/AX1C0YctFx4bgccpEuZC6MX3hwodRV25XjWuv6zE3Xjed2xG3E3dg92f24+ysPSw+RR4vHlKeT5xrPC16Il69XkVevt5L3Yu9q76c+Oj6JPo0+E752vqt9L/hh/QL9dvrd89fw5/rX+08EOASsCegKpARGBFYHPgsyCRIFdQTDwQHBu4IfL9JfJFzUFgJC/EN2hTwJNQxdFfpzGC4sNKwm7Hm4VXh+eHcELWJFREPEu0iPyNLIR4uNFksWd0bJR8VF1UdNRXtFl0VLl1gsWbPkRoxajCCmPRYfGxV7JHZyqffS3UuH4+ziCuPuLjNclrPs2nK15anLz66QX8FZcSoeGx8d3xD/iRPCqeVMrvRfuXflBNeTu4f7kufGK+eN8V34ZfyRBJeEsoTRRJfEXYljSa5JFUnjAk9BteB1sl/ygeSplJCUoykzqdGpzWmEtPi000IlYYqwK10zPSe9L8M0ozBDuspp1e5VE6JA0ZFMKHNZZruYjv5M9UiMJJslg1kLs2qy3mdHZZ/KUcwR5vTkmuRuyx3J88n7fjVmNXd1Z752/ob8wTXuaw6thdauXNu5Tnddwbrh9b7rj20gbUjZ8MtGy41lG99uit7UUaBRsL5gaLPv5sZCuUJR4b0tzlsObMVsFWzt3WazrWrblyJe0fViy+KK4k8l3JLr31l9V/ndzPaE7b2l9qX7d+B2CHfc3em681iZYlle2dCu4F2t5czyovK3u1fsvlZhW3FgD2mPZI+0MqiyvUqvakfVp+qk6oEaj5rmvep7t+2d2sfb17/fbX/TAY0DxQc+HhQcvH/I91BrrUFtxWHc4azDz+ui6rq/Z39ff0TtSPGRz0eFR6XHwo911TvU1zeoN5Q2wo2SxrHjccdv/eD1Q3sTq+lQM6O5+AQ4ITnx4sf4H++eDDzZeYp9qukn/Z/2ttBailqh1tzWibakNml7THvf6YDTnR3OHS0/m/989Iz2mZqzymdLz5HOFZybOZ93fvJCxoXxi4kXhzpXdD66tOTSna6wrt7LgZevXvG5cqnbvfv8VZerZ645XTt9nX297Yb9jdYeu56WX+x+aem172296XCz/ZbjrY6+BX3n+l37L972un3ljv+dGwOLBvruLr57/17cPel93v3RB6kPXj/Mejj9aP1j7OOiJwpPKp6qP6391fjXZqm99Oyg12DPs4hnj4a4Qy//lfmvT8MFz6nPK0a0RupHrUfPjPmM3Xqx9MXwy4yX0+OFvyn+tveV0auffnf7vWdiycTwa9HrmT9K3qi+OfrW9m3nZOjk03dp76anit6rvj/2gf2h+2P0x5Hp7E/4T5WfjT93fAn88ngmbWbm3/eE8/syOll+AAAACXBIWXMAAAsTAAALEwEAmpwYAAAf7UlEQVR4Ae1deXAc1Zl/fY1GkmXZlmWbyzYQB4JDEuIYMFe0jjGxAcc2HjAYiEMSAssmFFR2k6qtCqL2j02W2l1CKlTBViWhcpBYBJIQIAcJJkAAB3PEGAg3vpEl2ZJGmpk+9/d73a1j1D0ayZY0Mv3s0Uy/83vf9b73vaOFSEKCgQQDCQYSDCQYSDCQYCDBQIKBBAMJBhIMJBhIMJBgIMFAgoEEAwkGEgwkGEgwkGAgwUCCgQQDCQYSDCQYSDCQYCDBQIKBBAMJBhIMHCEYUEbYD6WpqUnbvHmzh3KeyGSUpv37lcbGRq+lZSHimt0R1pdkHzkGlEwmo+4H3ll0M3AvWvAjI/+LlpYW0oD0OexheGZpatLBIPphbzmpELKW0RYtWmQAFcPRgenD5enDaJkZm9VQus9cdsmy+rraFUbKSLV2HNipeN4/ag3v5T8/8qu3+moVzWpT02YVmsJB3JhwY39bR/QvKe0tLZT2zXbY0yuuuH76nu6uk/L53MmO5x6ra3rKtu1cfX3tY4/e/9Nn/HweyijD4r4cBmAeWdFZn1l9e8EzbnQE+EHWjWjXEZ7rZVVVvJQy1N9Wp9X7Nv+m5c0QWGoEMEIf8GF88l0aAwHeqM7lsHr1DTc07N7dsaK3x1plOt4ZjuvMFZoOwqgghSI8xYXYO6I+nfrW4w/9/D+C2vtoF9fasAwQEvDM5Zd8zXLU7xYs0tIz2aQiFCgAoQlVUxRwgKKCIRwnawjvV1W6+v2nHr0/4MaMlsEYhfGJGiEJJTBAVd/CMV34uLr48mtPPnDw4JcKprPeFsoxLojtui6EDnzhCRcUwAN+SlJ4RrqqSkyv1b7y6K9+drfgcDyM8JVkAB+YFmfVqvVH7+w0t1mKmKG4ngXoMBZJpUBIqWsIDXiBPKFqCMJzbKFrasvUavU7jz3UspUZJUCPPeaQb+Rz8qcfA83NatNmOWxKbXnx5RtPbj+Yvzmft650FbXacUh0h1Ln+GKtQA17+PQHIN9yFN2o1p32k0+ce+pP7r59b0jD/lyDfw2qYHASeDCwNPf3mF8UmkbiAzhvIPFZhNTUQHwaf6C86zq2bbmIdISW6ciafztjeeauzNXXHSO5EcSnVilu64P8LPHR3OxyqLzquutmLVlx2e2793W+0Ftwvmw6bjXxCbo7ID6DDtQCf4OJ7+PPM1QX1oBnNOzYte9axoU09NOH/o1lgGZwJAm2adMmzfTEGgfyXaZxSc4kk3iuZYERVCVvi2vf3tX6wrJVV1xPENjRMi3aoRAfQTESxwNspKaVl33l1ddbX8qZ7o0Q5bRrWyA8VDzwKYfaYQgAIRQCw7Dr2iJXKFxy1113GaShbCcGb1pMPKO1d9991z1QcD+RLXjf8qC4pdanxVFegEaCRhDQG67t2J5aZzrKhfMXnHTOGeee+7c/PfLbVlZDFfXKK6+Quz5QgVL/ox/9yBHA8bJL1p/WePSCX+Rs9QZI/BQMn9C0Es3UlOXiO8AfyOS5KKPMfve9PQ+//fq2XUiQtIxCcKwGkA4GlOjNK0sUxVCpgqDsY/NHVe7HsYyiw3RxCpblZG1t2Xs727eG2oCG4aJF13J++0EJdKbJmRGsJuXsFZlvtbUXtuRc/TzLtjG+Y1olh9PR4JoohMoVwlYwQ8ibubMZE9KSv4tDHEEVmOxy+gHjYzFVCz7yubiCcp4p3ugrTENHE1Brecur6eix7lzy2XWbrr/+m9O3br3bEr5dMEJuL6f1yslDbUdUcAhcvuaqj56+bM1TPaa41XYV3XOo7h2NeDp0iDE1ANI5XZR1+bSMxG0cA7Ccx7HD8tyFMOn5jApGp6nZMllIjlGYQaiK65im5faaSmbrG2+8cOG6jedyrGKbAZLY3hEVaPOE0+BzMKVu7ejaWnD1JaC7zYkdOmv4+BkdjgciC7iWHACz8ZSNGzemkYZKm8tnABBeZn7+H7sawZFzOe9EKMUsA9sf9jcNGli0qgcjMWcr8/a0dz5+3srMjSzoDwnS5TlsPZMkgyJA/K1bt1pr1lzdcPpn1rT02Mp3MUlOeY7DKTXH+cOGW+IE1FZhBwhbeMftae85inGZzPbyGQAqSgLU25M9GrpkJvQ364isgAmjDTAnDcW1bAvWRVdBuf2M89f9AMynE1lHgl0QqnyB/ixdeel5Ow52P1cQqXUmZsmqP9aPle1DSxCcoEzN2fZc0idcPCqmVSTndXd3S2JDQI+Fhw9zfMkBh50BCAzUni4tTNN0Cq76hQeffOmJizOZudIuKG/xo7hPFfFMBg5V/lmfveTm9l7zcdPT5rtWwZI+U8/DWD9qs2q4PnLEdVX4imxLzGPmkKbFBSMZIMxk2srRLm0SDt9jGGANa9AvKsZDy3b1M3e3mc8uvXDdWZQcKC+11Dx2DMEaddUkPhl4Y3NzevGytT/ttdT/dmDUK3DSQIrGSuoHwwsnPThAwHV8zOCEwU8lGUCoymxO5jFejykD+CDRzwWHB2YJlmfMac/aT5xzwZqr6RMHA7iTwjik80xkNBJ/+SUbT972xLYtpqdfYZumq8CQAhLhxRsTRTqYqvIJDlqMsY7rSQbYWlcXScNIBggzm5Yzh+KPEFk4otVDiPKbwF/YBTQLdDVravecuWxtMyulOpUu00NoYSyLSgYFo5Jhz11xyer9HQeetV31VHpDFQRqOL/9cUAlGqLMypU6TZ0p2928OXK8iWQATMlk5pShTfHtv7FE3dC6wbqG6sF7aLsi72m3LD5/7Q+YC8YpXMiV5zQaNMVbse4b2bz7gO1qU7EogiGMbvFxE/sByKTH1hNwLs0KIknTIeonigGYSTJALm/ODoR/SMEBLY3BT/Kvp3F+7Fi2Y7rGFxYtXfOnDRs2TKV6rSQm8Mf7rRbXTM5YvvYe+Da+bTmUPjjkfeKPAX7KqBIUcyG9hp6qDnNzvbY4RDFAXx5VU+lEGMo2fTnG8gd64KtNFZYznOP60td2559cvX7jfDLBwoWZ1Fi2Xk7dlHzCcvmXbpx9292/eNy0tavp4IK+l+N9OXWMVR6f1Ao0gNNw1VVX1bKdW2+9dYggD2WAAVxiaGpqKM+MFchx9XJhw9OxnmTZqn7qjve7nl6++opF27e3mEI0jWKxJK6dEcTDO8a26a+4MPP5RW+8s+NZ09XOhisf4z3lZbR+/BHAUFZW6FHPSXd0dEhh2b59qDNoCAM0B1wCyzvluErteJh/pftCpuXHM1wby0meOqe9M/fE8tUbVsAqoPt4XKeJckrKCQvaPv+izJq9bd1PFhx1ngtTP1D5TJzwQD6k8GJRSG2YP38InUMAhySEXPLH55+vwqx8Sphx4r+lLsIMwbEwqaref7D34QvWXHk14OI0Ua5bjDWMnIVwSop2vKaLLruxrde733LdNKZ4MPYUSNnE68vBOAA82DTodWERLiYMYYAwn9GFjX7c4VNhgTMEGlgcads6C/f800WX/StAJAOMqa+A4z1nIUTHWZ+99LudOfd2jK98tAlT5RGfoNGa97SenmwsHWMZYP78aXAAUddVGldLiHT60vPYoApC/NdnVm34Djs7Vr4CGpwc78Fk6TMvyPyyx/K+BmPPkysunlzMYfOVFUA27tHFWo5y0CzE0jk2QdOmYxKg0pdQkQFgaYDOLWDo7ei1/q1p5fofEtDD7iuA5NPgvOaafzn6d09t+0vOUtbCuWNz1wVMwVj8TTTSQDcpvDg3oLZls/gdHWI7YFkmdoDQ2q3oABbFNkjsme4sOBs/fdGGX99xx8NVh8NXwN066LnO9Yj1V13zqZd37t2CZdzF3KeH8X5iZh8jJgVMQSjxKlduJI4sHc8AaWxQkVOayHIVFIktUJBEMKx9sNdatemRe/70n3feKXcZjdZXQLeuHP0wvq/dcM2aN/cc/EuvJY4RfZ69ClWLg6hC/h1efmMZIG1VoXRs8qCmJvoB5AAPKLprmVZ33jv7dw8/9fT1N33zBOkr8JeUywaxqalZD5Zxvc+t/+JN8Dvc32sr1SosfSBkfFbyyoa2jIwYCwpc0Y8JsRTOGwUUkh7hmKIVGI0NJg42m7V3507a9tq7T15307+f7i8pN+mBSi8J9KJrr4Wl3ywt/ZXrNn5/Z1v3//QW4IPkKRe5Zatk8YpMBNN6OKszcgYw8tiixn/Da5GK6jjATcE2tDAcHPXi9tcf33jDzSvptOGMBlZ8LMPLbVt3323d8ZOfTD1/7Ybf7T3Q88/5QkEuSAB7WMaNxWFF9T8EBipRAsxVCRX9COOLv2MRYjSkcAyJa8rFRSbDM/axY5ttZ85Ov/rm7ocyn7+OB1Jc+gqKl5QDptCoKa77+tc/+sufP7yl9YB9gWWatg4sIsTiaDJgAgLsqionhNEhtnO59/PYUoJJ5CTTAH43peoyuKbc3ZMX7+ztvHPFpdd8m2mcJi7M+AtJXMkjUyDauXzj9au3bdvx17Zu8yQbJxiwmwpHb9n7SYkAHw38i7EPh8djx/JYBujt3Uv6g3MmKwLABHDSYEHL6e3tFfs6st9Y+rkNLbfddlvt9hYuJAks5uA8AsKy1Vc2v7mr7QEMG3XUHEAKDjhDh8ozrLHCw6IVHNB/kg5HtmekZoycAbLZrH/uuIK7WA5o4GEsh3BF2XTau8119/1hy5aLMl88HWXt1Zd9/sQl56/7fVu3fUsPjD0NGgMiM/ks/RKIwDDu5vR8LBcP0QAtC3nXjxC5XA648+I9CCUarbQkdAgOI0WDE8fuscQp+9oObL74si8/sKu95+msrS4Hdzg6LEdqjEqD/VDhgR7zqtLp8jVAc9DiggULLEM3crGsc6iQjWt52gSyJzp0gZ13vOodrQdXFyy3UTgmF3M4TxoiDOMK4pg0hl6Br3UDM7qYENvplStXWrAe8zHlJm00MIGDqtxrhv168pqNI0/q+4kjXbme0dtbPgPccsstMvOll17qqKqQRlJ/hUfKL+7YkWN9rAAcKT3luEZ7Lq4/QxAwcNqHeSCupZmss4C4Ln8w4v0ZDGgnSX8gttNDGGDQtM/DLS9JmJQYoNjS7jEM3Z09++TyNcDA3mJnPndk+kw0MCH5PQkwQPvfE+kqo7DgvPPk+sbCYIY3EPgIDdCfzE0PCfn78TGpfkHmAw1gZs45RzJAFPwlGSBdnZYFw5EkqoIkrkIxEKhuXNVnnXLKKbFDeRwDSNmvrqqKnz9UaL8TsPoxQFcwtDjPJkoGCGd4/TminR80GCQDpDT9iPMDDOz8Ef0b1Oc/Bx6PUv0s6fpUVIWLJpMn0GjBh9xLA0iGcBrLw1r8VFoIxS2Ek/CFsB8irJB8zOLVUTGA1AA2brOgMqhAtA1GDaBVNBU3GKOv3eDZHmxnpb8PAxw8oUjDhYV1KeHWgN8riBFIIJxyoLWGpSlc9gzCcwVa9oWDMw6ZHgrTkoj4jIoBJIIdXlg4kDNlbAX9IeJAXLlqu7dXeNOrhHP6scI+brrwpuBcK1yZerYgRGunEK/gXsodXUKZhcOyBs5JcImcGJqogJMNuMcNsAE+C4Sv04VrIA5wqVnY3oBNmZkWXlUA62jgBH6mTil9uCtmCGhC85tFTx6HsLgvYjSNj3UZAgUpVwqOcLOWMC86SViLjhfOjFrhpIC0kHEhVQqWe/RP54Txyi5h/P5VoXUB6fVVPhOMNZxF9UsJJ+O93yssMKy3/ERhH98ovPpaoYIxcQQWWiwnUjvahfrcTqGBscVsMC0DNUL5TCvJBoUXuxLIKmMYgElCVGMZMddTUoP4GSfiL1U8iM8bbAvXLhHmR47FNYs0emzsEYfiAkx9jAu7oNBQI5xzThLO/Jkiff9zQtvZKZQ6SBiROl6BNgoY0t2fF+7SE0Xu7A8JZ2adcEB4wktKydM87NpHjxXpxSeACd4SVY+8LhQwCy7sLn9ICHbzVaUNcJAMoUgEj/4XR5r4ABVKwCousCuQajgqReHqM0ThY3OxagVWMC2hg185tlID+EYQfoPGZAouAObnNYjc5WcKdzYOPvfCXoDtMC6B8HCMb8sLa8NpIvu5TwprTj2ghoYC3AIfld84d8JnXLAscrOnivyKTwhz42Lh4EpRaQ+wb2UEOoFIPNww38cAQMgQbo/ufVPQguvFbyctA4gxy8Jjq605Ya86VeQ+gvMaBdO/kgHIkdugohoOGMJDXnPONJEHAVxcg+4jNarA4YsL5uPCBvELl31MFJYswGW+kHfT11Rk1uKP3MZJpgUj9CzGMLH+U0K0Y+gqj/4SeJ6YyWZ7UCg+RDPAZr9A/ZTa3mAOMIJm4xs7LCkkPix9+9RGYX58nrwancCxs3LhYphG8HoTgbN9wjxxlrDPmy+8/TlpdQ9T7JCSpVXfkRfOEhiopy8QJoiqcejhkFAiUIOpGMQ92OKF0+aLwtLjJePj/qwSpQYnwQ0QTuUjG4upyb8o0nScytIAVGBEWpsp3NOg9jGGK7jEnNLDpMgeDsaHn4lIheq3T53rDwF8LqtwcWVlPKNeBcTm2x7sJR8WVrUhVAxd8k7vMopLoxHlTRi2hTM+JJQaA0YtT20OAzAQQgbq7snFrgOw+WgGWORDZptWSfVRBvyHNwuhZeenGaJwLKZ6+Be/4z26aRpaMKWko0gaYPPrhAccwekVXeBQY8FoLmYdLjSWfdQ0XLpPhh1hpSQ2yrmzpgrrk0cJ72BB+jaGq4XNFOzAym1qimw1mgGCmrE3HtumKFsVEkgk04XlnBZuHaziUexbJhbYI3oK7eqU8GZinpz3tchh72WoljCv9+bNFE5ah0oHCw4nvcWAAGheO+WmwEzHzYDxCniHDTSChZhSU+1rgM3RBSIZIFAA2BmMl71IdEUXnohYEs6DOsR+xUNo3rfIpdSnqVJhiR9CbbFFg0oVYNGZVo0jOlC5oxQoeVEDKGqjHtoUUjBLAy1T8Xq5bglfUzSUJbFo4orT6GITFAuJkmfB4UHz8Bat0v0vBaMvmrKOPKZeNCwZNRYB9brwtuCEphxvyzFUo8CQ4OEPX8/X5z6OyjggjmVSKSMro4ILwAcky58xDODrgJyJeZJsefSoLm7wkJ6l9AOR7XmhZeFMGeW4zd7QCFMxDVPbeoRXHUjUIQEXUxiNccanYEpHaRo6E48pVxRNmCXzcHmGcik7UZSp6JGkc82CXNENtXpRlhgjMMhVVzvFpJqE2mVzEx+IQUyBlANw+Ozu6DMARwKc7AkwQwZItXYJ8R4+sMz7Vg8PZy+l8IBWWPBR0ZZWjvUe1z7Uv06t19YtmUg6u+LyMj6wM3ImbzSJDzEawC+QrqkOLpepEAaQYgCsNhhCf2EnFnpM4eh4VSE0Q7mqldJIlzGnYtrLO+X3mA0BhJdG+PSU0F5rBbzQWpzDB3ZAwB/x1EGKZG7mh+pX4Q3U38CiVj3WZ4YrzAz43z+MR+uAkgxg6Gqv35APRklIxyuRCJ1aJZSX20T6xR1wj/uLVRoIWxabYuagpAxR9Q4I8vg7QjRioQXMMGYB1js1jP7mQaG98T5OnWpyKCDDlkR+ABAhk7QEo6fe2S/0v7fJ/mPRozTIsGvpHcFyeK5UxkgY6upeB9QYP2yRD1hNckCpisY1DUhVG9PC+M02Uf3qHiHSmM5hcKV3TQIeBYzEItKRN/1+p6j59YtSNUvHUmyhqIpGHkcNpcyoEsajWInc3ynUFKxCvgp2mKqIdB5ecqt0kW7LCu3Pr0D6eSXhcCVZMcXBE9VTarEWzuC/vdf/3f83kgHC5M6ebpsnS4LhJIye+G8QWr6sGiuCVT9+RtT8HapcNbAMDOSg23LRJSA4kUV0eboOyU+J6h0dInXvM8Lbg7G0Fpd7jqX0B5iiJ9CuNYSBJeCaXz8vjM6C0PCSZ8kCEs4gY0BXOeVDvFwoqkqJGix3Gw+/KPS3QcspYHZqwdKBRJPuUQw+PaWyllwO9q9ALVV8gtLI20QCNkuomBJW/d/TQj//wyK/aL6wG2qxsQLExj/KAJGpYZ5vdOSE9uouof3xVYylWHmrh+q3oEaRZzyCdP/OSAv9pVZR4z0r8hd9QhSOqgdzglbUBmQE9ovwcBWWH9CwqjUrUo+8JPQtu4Q6qwbrAiOY/pKJ8MIK9q8u5o0hJRlg1syGrh37DljYWMiNS5UVCBAI62EtXa1Xhfbga0J99l3hLZwt7LnwllXDU0jVlTeFtg9q97X3ReqtLuFh6HBhQ4wn8fsQB+J5sDl07E5K73lc6E0fFvbJRwtzWo3ArZzyDQn0+CnYIaR39cBu2CuMx14X6j4IcUD8vrrK+EGmyhZwwV2JEMkAjY2Nvo4xlG6cDyxAmnhpAuLKMrNKNHeYk0BfqleO4+4xNUKHS9f7y3tC73mnz1lCL59IIyMsce8Y7AEAUpRxlPziHksJnpYWGn0QP35BpI76h6ha0OC7pGEb4EUpmOphvH8T09wd8OHMArPOhPcPMI8E+yQgnGbWrBkzAhugGBL/OZIBwqxtu/fiMmwVN2xxqjWM1RkWGu9vagJJVGoDmDSza6Qq7XO4MJ0BjELPnwxhnP80rn8luISDu4DmYR0Cu5qM5/YIr4A4gscMafSjDjJ3PNLJ4NQcI4PZtwGwEWpaqkZ6AvuEuqi3kUZgeIbsqHkfyuq63kVu8k2KotKV9sgpF406esr4Hf6mxDOtkgLhIVxU/ZBwcSxe6jEXn+Pw4TMYRKaPEm6OfrjjyWlvh9esRIhkgOZmv8Scjx/fA8nPSq8TBKhEPZWVNFBaBv6uLCh9aKC9JKOSGcIPGXeUhGelUlihMvDSr855px4rNUAo1MUoiGQAIZolse/46ldNWNHdwYyiuGzyXKkYgLDiQAh8CE53jW3LaWBz8y2RAhzDAH7PQHhcsaW1Smt61MsYlYqlIxsuCi2YoP173/tesKknWhWWYgBZAlZrB+fUSZg8GIAFiP+AV1HeD6COJWAcA0BdZGSaphu7J0/XE0hDDNAIxB3R8JPLQAYY2RCQyfglMZiAAVi2Ul6F5sOV/C2FAbmVFIuc6g6ZK5MZsQboe988vKqoBFapfN153+y6VOtJ2sRigL4uTf5R3fcIStP+/SNngNBxAN/KTliT4SHRSDUysf1NWi/CAMQf1juOTemK+m5R2pDHOBugL+OUOnU36muVixN0uSWhojEAUcf2Q6yNKG7rzJopcggIhTkK8FgGCO8MfvDee9vgk3oHW1FRPmGAKCRWUhwklBoA6wDaWy0tP9xP2EJaRsEZywBC3qPfLNPxfraXuRtX+piiakniKgkDHAHoYX7JBwo09N+JEAljPAMg+6JFD1Lsse1K2QLGoneS+ZNhgEipzEAVjaMH/Ks/TRBDGsaBW5IBwk0ENdXqMzh3n8dGBaqBhAHisDnx8VD/fNmn011bq0gGOOGEEzCFiw+x04OgSB/BP7l09VO2p53l4d4YpJVcRo5vLkkZYwzY2CSr64rzh+f+/MAFQVt9NIxqu6QGQAGvCW/MZsG0od6PtynCw5gogChEVkIcKCNPu1Wn9HsJT0C7kgQbjgFEOIWY01jzM7xRpR0WAS9ngRYYTnlUAko+UDCYqgZrTXhvzG+c1xIwQEn1zzzlURGvUsWruZ3zVlz69ayl3lYoFHAqy+MRVRqcsg4cHsL3oH0r5dVNKJJQDgYCSVaAaVh5/lM4L4d9jj3PuiGm1qiZzb/9+X2Ufr4hbbiKyyMSj4YF98ssWb72pzlHu4IuAW4Zx64TyQFy+zhbI2wIsBdx9kHC6QMblGcWv1Eyy+DmJfv0K6zBicwt0wZHs77ogGYHZB3wMzo7YsOmw7x8Brpj8/sJYamoJxnnIyioZagz3W+lqJawUXxjvyPQSQJIdCEjHT0yIEpOz+XBU0dU60rzX/9w361IC8sPrjaAYeBXmHFgXORvvF9PxUeqlMVNq67EsdGLDV0/Xte0mXnbqtdUvcpxXOxn4hzUP/3C6QhVRBjwiODzw6C4ID5cxeyHP8wVfA/bncH5B7WEsn5xn3FDavsTGx/GflD7Ye7/Nbju2KeIAmw3EAYAwQz48P+AvH6sHx/WTXyxHHEoD4jg1ldd10zs0uLVPXlo/A5cOHEA74V8D9fC/+yJh1oeCsqyOr+7YWUx3wNAiMkxMJqvXh3gVKCg33TTrVPf3P127cwZ02r27Gud4alGOl2TruvY39GAc+xVU2pq0gXTbOjJ5utShqFVpVNpjB1TC4V8vWXZmoHrrPEKeJzsUrB939Nc19Vws5Vu2w7vTsNuVDnj4Nu8Ob/lGetAoxCFEo8BhEFXAnkjclFzH/RyrEIcJSadxtUy/I3UXpy25cSGpZ0B27C4b5MMLNVOSEFZm4/XPuxSJAZqN1AHOppvW3NQHDfDKDivgutaFc+Gfe5g2zdfxePYoJyNBqtSKSuVSh2EVs9iaC2YBcuZUlfdZVSlDpi9Zi5fyFkzpk8/AEQd7GzvsObMOrqrulbrPNjdWjhx8eKu/7355oFHv9gNhj7w/Mf4v2GB+BxDUxSMLxrGF9oAZTc0tJr4GGobvMRZ7e5epHTOmqU2Hjyo4pCjYjU09ME7Z0Bx06xXROOACPzkIwxYkc/n/TIzZ4qZzNLQIE7Cb+y85JNoa3tNtLfDtpW/ce4uCPxV1d3tSV9qGMlvRKRSafTb32uxb2Aafhvt7R78J+6u6l1efWu9y3n4pk2bXDDCmOAqaH7UNOlDaFE/ynlUoA2UZuTcvn17Xz37i5YeN4c18ayBtE3lHyIj/IQ5PqjfxF3wyQjuw2gJcNhUhJFwRsbocJOnv4G3edS47CNcUVvj9VhO+0Ge5tIwRSRHRJWuoyg1tnxsQlhBX4ZypL6cPGHFyXeCgQQDCQYSDCQYSDCQYCDBQIKBBAMJBhIMJBhIMJBgIMFAgoEEAwkGEgwkGEgwkGAgwUCCgQQDCQYSDCQYKAcD/w8bQFWpi6zeCgAAAABJRU5ErkJggg==) no-repeat;' +
            'width: 32px; ' +
            'height: 32px; ' +
            'background-size: cover;' +
            'background-color: #ffffff;' +
            'position: fixed;' +
            'z-index: 99;' +
            'left: 10px;' +
            'bottom: 10px;'
        );
        popupBtn.addEventListener("click", togglePopup);
        document.addEventListener("click", function (e) {
            if (popupShown) {
                togglePopup(e);
            }
        });

        kuollTools = document.createElement("div");
        kuollTools.setAttribute("id", "kuollTools");
        kuollTools.appendChild(popupBtn);

        createIframe(kuollTools);

        if (typeof document.body != "undefined" && document.body != null) {
            document.body.appendChild(kuollTools);
        } else {
            window.addEventListener("DOMContentLoaded", function () {
                document.body.appendChild(kuollTools);
            })
        }
    }

    function destroyPopup() {
        kuollTools.style.display = "none";
        popupShown = false;
    }

    function recordIsStarted(recordCode) {
        if (popupIframe.contentWindow) {
            popupIframe.contentWindow.postMessage(JSON.stringify({
                action: "updateRecordCode",
                recordCode: recordCode
            }), "*")
        }
    }

    function recordIsFinished() {
        popupIframe.contentWindow.postMessage(JSON.stringify({
            action: "updateRecordCode",
            recordCode: null
        }), "*")
    }

    window.addEventListener("message", function (event) {
        if (event.origin + "/" !== server && event.origin + "/" !== server.replace("https://", "http://")) {
            return;
        }

        var data = JSON.parse(event.data);

        if (data.action === "updateIframeSize") {
            if (data.height) {
                popupIframe.style.height = data.height + parseInt(popupIframe.style.borderTopWidth) * 2
                + parseInt(popupIframe.style.borderBottomWidth) * 2 + "px";
            }
        } else if (data.action === "finishRecord" || data.action === "restartRecord") {
            kuoll.stopRecord({
                restart: (data.action === "restartRecord")
            });
        }
    });

    return {
        createPopup: createPopup,
        recordIsStarted: recordIsStarted,
        recordIsFinished: recordIsFinished,
        destroyPopup: destroyPopup
    }

})();
