chrome.runtime.onMessage.addListener(function (request, sender, sendResponse) {
    if (request.action === "getCookie") {
        chrome.cookies.get({url: config.server, name: request.name}, function (cookie) {
            sendResponse(cookie);
        });
        return true;
    }
});
