(function () {

    (function injectScript() {
        var s = document.createElement('script');
        s.src = chrome.runtime.getURL("injectScript.js");
        s.onload = function () {
            this.parentNode.removeChild(s);
        };
        (document.head || document.documentElement).appendChild(s);
    }());


    function createPopup(customerId, userId) {
        window[config.globalName].createPopup(customerId, userId);
    }

    chrome.runtime.sendMessage({action: "getCookie", name: "userToken"}, function (cookie) {
        if (cookie && cookie.value) {
            var xhr = new XMLHttpRequest();
            xhr.open("POST", config.server + "../getUserId");
            xhr.onload = function () {
                var response = JSON.parse(xhr.response);
                if (response.errorMsg) {
                    console.error(response);
                } else {
                    window.dispatchEvent(new CustomEvent("kuollDispatchCustomerId", {detail: {apiKey: response.apiKey}}));

                    //createPopup(response.customerId, response.userId);
                }
            };
            xhr.onerror = function () {
                createPopup("0");
            };
            xhr.send(JSON.stringify({
                userToken: cookie.value
            }));
        } else {
            console.warn("KUOLL-BRIDGE: Could not get userToken at " + config.server + ". Check if you're logged in");
            //createPopup("0"); // use default Kuoll Customer
        }
    });

}());
