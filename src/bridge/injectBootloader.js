(function injectBootloader() {
    var s = document.createElement('script');
    s.src = chrome.runtime.getURL("bootloader.js");
    s.onload = function () {
        this.parentNode.removeChild(s);
    };
    (document.head || document.documentElement).appendChild(s);
}());

function callAPIMethod(methodName, params) {
    var event = new CustomEvent("kuollAPICall", {"detail": {method: methodName, params: Array.prototype.slice.call(params)}});
    window.dispatchEvent(event);
}

if (typeof window[config.globalName] === "undefined") {
    (function (w, k) {
        w[k] = w[k] || function () {
                var a = arguments;
                return new Promise(function (y, n) {
                    (w[k].q = w[k].q || []).push({a: a, d: {y: y, n: n}});
                });
            };
    }(window, config.globalName));

    var kuoll = window[config.globalName];
    kuoll.startRecord = function () {
        callAPIMethod("startRecord", arguments);
    };
    kuoll.isRecordActive = function () {
        return window.sessionStorage.hasOwnProperty("kuollRecordingInfo");
    };
    kuoll.stopRecord = function () {
        callAPIMethod("stopRecord", arguments);
    };
    kuoll.restartRecord = function () {
        callAPIMethod("restartRecord", arguments);
    };
    kuoll.cancelRecord = function () {
        callAPIMethod("cancelRecord", arguments);
    };
    kuoll.createPopup = function () {
        callAPIMethod("createPopup", arguments);
    };
    kuoll.getRecordInfo = function () {
        callAPIMethod("getRecordInfo", arguments);
    };
    kuoll.createIssue = function () {
        callAPIMethod("createIssue", arguments);
    };
}