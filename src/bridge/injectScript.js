(function () {

    var apiKey;

    function proxyKuollApi() {
        var globalName = "kuoll";
        var api = window[globalName];
        if (api.isRecordActive && api.isRecordActive()) {
            api.stopRecord();
            console.log("Current record is stopped by Embed Bridge for debugging purposes");
        }

        // Replace any Kuoll API with Dev Kuoll API
        window[globalName] = window[config.globalName];

        var startRecordMethod = window[globalName].startRecord;
        window[globalName].startRecord = function (params) {
            var newParams;
            if (typeof params === "string") {
                newParams = apiKey;
            } else if (typeof params === "object") {
                newParams = params;
                newParams.API_KEY = newParams.customerId = apiKey;
            }

            console.log("API_KEY replaced with " + apiKey + " for debugging purposes");

            startRecordMethod(newParams);
        };
    }

    (function (w, k) {
        w[k] = w[k] || function () {
                var a = arguments;
                return new Promise(function (y, n) {
                    (w[k].q = w[k].q || []).push({a: a, d: {y: y, n: n}});
                });
            };
    }(window, 'kuoll'));

    window.addEventListener("kuollDispatchCustomerId", function (e) {
        var disableKuoll = false; // Use to disable kuoll for debugging purposes
        if (disableKuoll) {
            delete window.kuoll;
        } else {
            apiKey = e.detail.apiKey;
            proxyKuollApi();
        }
    });

}());