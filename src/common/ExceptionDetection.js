/**
* When asynchronous callbacks throw any exceptions, that's not caught explicitly, this exception can only be
* registered by window.onerror. However, this listener doesn't always provide sufficient info about exception.
*
* To work it around, this module proxies following asynchronous callbacks:
*  - callback properties of XmlHttpRequest object
*  - setTimeout and setInterval callbacks
*  - arguments of addEventListener
*
* Proxy calls original function, catching and rethrowing any exception. This allows us to react to any unhandled
* exception and always have access to the original Error object.
*
* Implementation is based on raven-js: https://github.com/getsentry/raven-js/blob/master/src/raven.js
* @type {{install, uninstall, subscribe, onExceptionCaptured}}
*/

var objectPrototype = Object.prototype;

var wrappedBuiltIns = [];

var installed = false;

var subscribers = [];

/**
 * Instrument browser built-ins w/ breadcrumb capturing
 *  - XMLHttpRequests
 */
function instrumentBreadcrumbs() {
    function wrapProp(prop, xhr) {
        if (prop in xhr && isFunction(xhr[prop])) {
            fill(xhr, prop, function (orig) {
                return wrap(orig);
            }); // intentionally don't track filled methods on XHR instances
        }
    }

    if ('XMLHttpRequest' in window) {
        var xhrproto = XMLHttpRequest.prototype;
        fill(xhrproto, 'open', function(origOpen) {
            return function (method, url) { // preserve arity
                return origOpen.apply(this, arguments);
            };
        }, wrappedBuiltIns);

        fill(xhrproto, 'send', function(origSend) {
            return function (data) { // preserve arity
                var xhr = this;

                var props = ['onload', 'onerror', 'onprogress'];
                for (var j = 0; j < props.length; j++) {
                    wrapProp(props[j], xhr);
                }

                if ('onreadystatechange' in xhr && isFunction(xhr.onreadystatechange)) {
                    fill(xhr, 'onreadystatechange', function (orig) {
                        return wrap(orig);
                    });
                }

                return origSend.apply(this, arguments);
            };
        }, wrappedBuiltIns);
    }
}

function instrumentTryCatch() {
    function wrapTimeFn(orig) {
        return function (fn, t) { // preserve arity
            // Make a copy of the arguments to prevent deoptimization
            // https://github.com/petkaantonov/bluebird/wiki/Optimization-killers#32-leaking-arguments
            var args = new Array(arguments.length);
            for(var i = 0; i < args.length; ++i) {
                args[i] = arguments[i];
            }
            var originalCallback = args[0];
            if (isFunction(originalCallback)) {
                args[0] = wrap(originalCallback);
            }

            // IE < 9 doesn't support .call/.apply on setInterval/setTimeout, but it
            // also supports only two arguments and doesn't care what this is, so we
            // can just call the original function directly.
            if (orig.apply) {
                return orig.apply(this, args);
            } else {
                return orig(args[0], args[1]);
            }
        };
    }

    function wrapEventTarget(global) {
        var proto = window[global] && window[global].prototype;
        if (proto && proto.hasOwnProperty && proto.hasOwnProperty('addEventListener')) {
            fill(proto, 'addEventListener', function(orig) {
                return function (evtName, fn, capture, secure) { // preserve arity
                    try {
                        if (fn && fn.handleEvent) {
                            fn.handleEvent = wrap(fn.handleEvent);
                        }
                    } catch (err) {
                        // can sometimes get 'Permission denied to access property "handle Event'
                    }

                    return orig.call(this, evtName, wrap(fn), capture, secure);
                };
            }, wrappedBuiltIns);
            fill(proto, 'removeEventListener', function (orig) {
                return function (evt, fn, capture, secure) {
                    try {
                        fn = fn && (fn.__kuoll_wrapper__ ? fn.__kuoll_wrapper__  : fn);
                    } catch (e) {
                        // ignore, accessing __kuoll_wrapper__ will throw in some Selenium environments
                    }
                    return orig.call(this, evt, fn, capture, secure);
                };
            }, wrappedBuiltIns);
        }
    }

    // fill(window, 'setTimeout', wrapTimeFn, wrappedBuiltIns);
    // fill(window, 'setInterval', wrapTimeFn, wrappedBuiltIns);
    if (window.requestAnimationFrame) {
        fill(window, 'requestAnimationFrame', function (orig) {
            return function (cb) {
                return orig(wrap(cb));
            };
        }, wrappedBuiltIns);
    }

    // event targets borrowed from bugsnag-js:
    // https://github.com/bugsnag/bugsnag-js/blob/master/src/bugsnag.js#L666
    var eventTargets = ['EventTarget', 'Window', 'Node', 'ApplicationCache', 'AudioTrackList', 'ChannelMergerNode', 'CryptoOperation', 'EventSource', 'FileReader', 'HTMLUnknownElement', 'IDBDatabase', 'IDBRequest', 'IDBTransaction', 'KeyOperation', 'MediaController', 'MessagePort', 'ModalWindow', 'Notification', 'SVGElementInstance', 'Screen', 'TextTrack', 'TextTrackCue', 'TextTrackList', 'WebSocket', 'WebSocketWorker', 'Worker', 'XMLHttpRequest', 'XMLHttpRequestEventTarget', 'XMLHttpRequestUpload'];
    for (var i = 0; i < eventTargets.length; i++) {
        wrapEventTarget(eventTargets[i]);
    }
}

function instrumentUncaughtError() {
    window.onerror = function (msg, url, lineNo, columnNo, error) {
        if (error) {
            notifyExceptionCaptured(error);
        }
    };
}

/**
 * Browsers may hide details of an error if it originates in a file from another domain.
 * Kuoll snippet may catch an error and dispatch it as a CustomEvent. This way error details are preserved since
 * the snippet is located within the page.
 * This function listens for such redispatched errors.
 */
function observeInPageErrors() {
    window.addEventListener("kuollInPageError", function (customEvent) {
        if (customEvent && customEvent.detail && customEvent.detail.errorEvent && customEvent.detail.errorEvent.error) {
            notifyExceptionCaptured(customEvent.detail.errorEvent.error);
        }
    })
}

function restoreBuiltIns() {
    // restore any wrapped builtins
    var builtin;
    while (wrappedBuiltIns.length) {
        builtin = wrappedBuiltIns.shift();

        var obj = builtin[0],
            name = builtin[1],
            orig = builtin[2];

        obj[name] = orig;
    }
}

function fill(obj, name, replacement, track) {
    var orig = obj[name];
    obj[name] = replacement(orig);

    if (track) {
        track.push([obj, name, orig]);
    }
}

function wrap(options, func, _before) {
    // 1 argument has been passed, and it's not a function
    // so just return it
    if (isUndefined(func) && !isFunction(options)) {
        return options;
    }

    // options is optional
    if (isFunction(options)) {
        func = options;
        options = undefined;
    }

    // At this point, we've passed along 2 arguments, and the second one
    // is not a function either, so we'll just return the second argument.
    if (!isFunction(func)) {
        return func;
    }

    // We don't wanna wrap it twice!
    try {
        if (func.__kuoll__) {
            return func;
        }

        // If this has already been wrapped in the past, return that
        if (func.__kuoll_wrapper__){
            return func.__kuoll_wrapper__;
        }
    } catch (e) {
        // Just accessing custom props in some Selenium environments
        // can cause a "Permission denied" exception (see raven-js#495).
        // Bail on wrapping and return the function as-is (defers to window.onerror).
        return func;
    }

    function wrapped() {
        var args = [], i = arguments.length,
            deep = !options || options && options.deep !== false;

        if (_before && isFunction(_before)) {
            _before.apply(this, arguments);
        }

        // Recursively wrap all of a function's arguments that are
        // functions themselves.
        while(i--) args[i] = deep ? wrap(options, arguments[i]) : arguments[i];

        try {
            // Attempt to invoke user-land function
            // NOTE: If you are a Sentry user, and you are seeing this stack frame, it
            //       means Raven caught an error invoking your application code. This is
            //       expected behavior and NOT indicative of a bug with Raven.js.
            return func.apply(this, args);
        } catch(e) {
            notifyExceptionCaptured(e);
            throw e;
        }
    }

    // copy over properties of the old function
    for (var property in func) {
        if (hasKey(func, property)) {
            wrapped[property] = func[property];
        }
    }
    wrapped.prototype = func.prototype;

    func.__kuoll_wrapper__ = wrapped;
    // Signal that this function has been wrapped already
    // for both debugging and to prevent it from being wrapped twice
    wrapped.__kuoll__ = true;
    wrapped.__inner__ = func;

    return wrapped;
}

function isUndefined(what) {
    return what === void 0;
}

function isFunction(what) {
    return typeof what === 'function';
}

function hasKey(object, key) {
    return objectPrototype.hasOwnProperty.call(object, key);
}

export function install() {
    if (installed) return;

    instrumentBreadcrumbs();
    instrumentTryCatch();
    instrumentUncaughtError();

    observeInPageErrors();
    installed = true;
}

export function uninstall() {
    if (!installed) return;
    
    restoreBuiltIns();
    installed = false;
    subscribers = [];
}

export function subscribe(callback) {
    subscribers.push(callback);
}

export function unsubscribe(callback) {
    subscribers = subscribers.filter(c => c !== callback);
}

function notifyExceptionCaptured(exception) {
    for (var i = 0; i < subscribers.length; ++i) {
        subscribers[i](exception);
    }
}