// Fixed size queue
var queue = (function () {
    var array = [];
    // TODO vlad: incrementing index of first element or using Array.shift? Test performance
    var firstElemIndex = 0;
    var MAX_SIZE = 50;
    var SLICE_SIZE = 10;

    return {
        add: function (data) {
            array.push(data);
            if (array.length - firstElemIndex > MAX_SIZE) {
                delete array[firstElemIndex++];
            }
        },

        getAsArray: function () {
            var result;
            var length = array.length;
            for (var i = length - 1; i >= length - SLICE_SIZE * 2 && i >= 0; --i) {
                if (result && result.length >= SLICE_SIZE) {
                    break;
                }
                if (array[i].type) {
                    result = array.slice(i);
                }
            }

            if (!result || result.length < SLICE_SIZE) {
                result = array.slice(Math.max(0, length - SLICE_SIZE));
            }
            return result;
        },

        getLast: function () {
            return array.length > 0 ? array[array.length - 1] : null;
        }
    };
}());

function getCoordinates(event) {
    var eventDoc, doc, body;
    var mouseX, mouseY;

    event = event || window.event; // IE-ism

    // If pageX/Y aren't available and clientX/Y are,
    // calculate pageX/Y - logic taken from jQuery.
    // (This is to support old IE)
    if (event.pageX === null && event.clientX !== null) {
        eventDoc = (event.target && event.target.ownerDocument) || document;
        doc = eventDoc.documentElement;
        body = eventDoc.body;

        mouseX = event.clientX +
        (doc && doc.scrollLeft || body && body.scrollLeft || 0) -
        (doc && doc.clientLeft || body && body.clientLeft || 0);
        mouseY = event.clientY +
        (doc && doc.scrollTop || body && body.scrollTop || 0) -
        (doc && doc.clientTop || body && body.clientTop || 0 );
    } else {
        mouseX = event.pageX;
        mouseY = event.pageY;
    }
    return {
        x: mouseX,
        y: mouseY
    };
}

var mouseMoveListener = function handleMouseMove(event) {
    var coords = getCoordinates(event);

    var last = queue.getLast();
    if (event.type === "click") {
        coords.type = "click";
        queue.add(coords);
    } else if (!last || Math.abs(last.x - coords.x) > 4 || Math.abs(last.y - coords.y) > 4) {
        queue.add(coords);
    }
};

export function init() {
    document.addEventListener("mousemove", mouseMoveListener);
    document.addEventListener("drag", mouseMoveListener);
    document.addEventListener("click", mouseMoveListener);
}

export function shutdown() {
    document.removeEventListener("mousemove", mouseMoveListener);
    document.removeEventListener("drag", mouseMoveListener);
    document.removeEventListener("click", mouseMoveListener);
}

export function getMousePositions() {
    return queue.getAsArray();
}

