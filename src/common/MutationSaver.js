import reportError from "./reportError";
import * as utils from "./utils";
import * as saving from "./saving";
import * as domTools from "./domTools";
import * as capturing from "./capturing";
import * as ignoring from "./ignoring";
import TreeMirrorClient from "./mutation-summary/tree-client";

let tmc;

let lastMutationTime;

const HTML_EVENT_ATTRIBUTES = ["onload", "onerror", "onmouseover", "onabort", "onbeforeonload", "onbeforeunload",
    "onblur", "onchange", "onclick", "oncontextmenu", "ondblclick", "ondrag", "ondragend", "ondragenter",
    "ondragleave", "ondragover", "ondragstart", "ondrop", "onfocus", "onkeydown", "onkeypress",
    "onkeyup", "onmessage", "onmousedown", "onmousemove", "onmouseout", "onmouseup",
    "onmousewheel", "onreset", "onresize", "onscroll", "onselect", "onsubmit", "onunload"];

export function init() {
    lastMutationTime = utils.currentTimestamp();

    var kuollTools = document.getElementById("kuollTools");
    if (kuollTools === null) {
        var listener = function (e) {
            var detail = e.detail;
            var parsed = ((typeof detail) === "string") ? JSON.parse(detail) : detail;
            if (parsed.action === "setKuollToolsNode") {
                kuollTools = document.getElementById("kuollTools");
            }
            window.removeEventListener("kuollInterdom", listener);
        };
        window.addEventListener("kuollInterdom", listener);
    }


    if (tmc) {
        tmc.disconnect();
    }

    tmc = new TreeMirrorClient({
        target: document,

        mirror: {
            initialize: function (rootId, children) {
                saving.saveBaseSnapshot(JSON.stringify({
                    rootId: rootId,
                    children: children
                }));
            },

            applyChanged: function (removed, addedOrMoved, attributes, text, propertiesChanged) {
                if (removed.length > 0 || addedOrMoved.length > 0 || attributes.length > 0 || text.length > 0 || propertiesChanged.length > 0) {
                    var newTime = utils.currentTimestamp();
                    var timeDelta = newTime - lastMutationTime;
                    lastMutationTime = newTime;

                    addedOrMoved.forEach(function (node) {
                        var attributes = node.attributes;
                        if (attributes) {
                            if (node.tagName === "IMG" && attributes.src) {
                                domTools.loadAndSaveImage(attributes.src);
                            }
                            if (attributes.style) {
                                domTools.loadAndSaveResourcesFromStyle(attributes.style);
                            }
                        }
                    });
                    attributes.forEach(function (node) {
                        var attributes = node.attributes;
                        if (attributes && attributes.style) {
                            domTools.loadAndSaveResourcesFromStyle(attributes.style);
                        }
                    });

                    var sequent = window.event ? capturing.recordBrowserEvent(null, window.event) : capturing.getCurrentSequent();
                    var mutationData = {
                        removed: removed,
                        addedOrMoved: addedOrMoved,
                        attributes: attributes,
                        text: text,
                        propertiesChanged: propertiesChanged,
                        timeDelta: timeDelta
                    };
                    try {
                        capturing.recordDomMutation(sequent || capturing.recordBrowserEvent(window.event), mutationData);
                    } catch (e) {
                        reportError("Can not record DOM mutation", e);
                    }
                }
            }
        },

        filterNodes: function (node) {
            function doFilter(node) {
                if (node.tagName === "SCRIPT") {
                    return false;
                }
                if ((node.tagName === "FRAME" || node.tagName === "IFRAME") && !node.getAttribute("src")) {
                    return false;
                }
                if (node.nodeType === Node.COMMENT_NODE) {
                    return false;
                }
                if (node.nodeType === Node.ELEMENT_NODE && node.getAttribute("kuoll-ignore") === "true") {
                    return false;
                }
                if (ignoring.isNodeIgnored(node)) {
                    return false;
                }
                if (kuollTools && (kuollTools.contains(node) || kuollTools === node)) {
                    return false;
                }
                return true;
            }

            function overrideAttributes(node) {
                if (node.tagName === "INPUT" && node.getAttribute("type") === "password") {
                    return {
                        attributes: {
                            value: node.value ? "<hidden by kuoll>" : ""
                        }
                    };
                }
                if ((node.tagName === "FRAME" || node.tagName === "IFRAME") && node.getAttribute("src")) {
                    return {
                        attributes: {
                            sandbox: ""
                        }
                    };
                }
                return true; // keep the node without changes
            }

            if (typeof node.__mutation_summary_filtered !== "undefined") {
                return node.__mutation_summary_filtered;
            } else {
                var result = doFilter(node);
                if (result) {
                    var attributesResult = overrideAttributes(node);
                    if (attributesResult === true) {
                        node.__mutation_summary_filtered = true;
                    }
                    return attributesResult;
                } else {
                    node.__mutation_summary_filtered = result;
                }
                return result;
            }
        },

        filterAttributes: function (node, attribute) {
            if (HTML_EVENT_ATTRIBUTES.indexOf(attribute.name) !== -1) {
                var attr = document.createAttribute("_" + attribute.name);
                attr.value = attribute.value;
                return attr;
            } else if (node.tagName === "HTML" && attribute.name === "manifest") {
                return false;
            } else {
                return attribute;
            }
        }

    });
}

export function forceCheckValueChanges() {
    tmc.applyChanged([{
        removed: [],
        characterDataChanged: [],
        added: [],
        reparented: [],
        reordered: []
    }]);
}

export function shutdown() {
    if (tmc) {
        tmc.disconnect();
        tmc = null;
    }
}
