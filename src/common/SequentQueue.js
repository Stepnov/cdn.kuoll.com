import * as network from "./network";
import * as recording from "../embedScript/embedFake";
import reportError from "./reportError";

let queue = [];

// Number of sequents to send in one batch
let BATCH_SIZE = 100;

// If queue contains more than 50 sequents, send batch even if postQueue is not empty
let CRITICAL_QUEUE_SIZE = 100;

let sendSequentsWhenNetworkQueueIsEmpty = true;

let lastFrameNum = 0;

/**
 * @returns {Array.<Sequent>} Array of sequents from the first one to the BATCH_SIZE or to the end of frame
 */
function getNextBatch() {
    for (var i = 0; i < BATCH_SIZE && i < queue.length; ++i) {
        if (queue[i + 1] && queue[i].frameNum != queue[i + 1].frameNum) break;
    }

    return queue.splice(0, i + 1);
}

function sendBatch() {
    var batch = getNextBatch();
    if (batch.length > 0) {
        network.doPost("saveSequentsBatch", {
            sequents: batch
        });
        lastFrameNum = batch[batch.length - 1].frameNum;
        return true;
    }
    return false;
}

export function init(batchSize, criticalQueueSize) {
    BATCH_SIZE = batchSize || BATCH_SIZE;
    CRITICAL_QUEUE_SIZE = criticalQueueSize || CRITICAL_QUEUE_SIZE;

    queue = [];
    lastFrameNum = 0;
    sendSequentsWhenNetworkQueueIsEmpty = true;

    network.addQueueEmptyListener(function () {
        if (!recording.getRecordingInfo().localRecording && queue.length > 0) {
            sendBatch();
        }
    });

    window.addEventListener("beforeunload", function () {
        try {
            window.sessionStorage["kuoll-sequentQueue"] = JSON.stringify(queue);
            queue = [];
        } catch (e) {
            reportError("Error while persisting sequent queue on page unload. Some sequents will be lost", e);
        }
    });

    var savedQueue = window.sessionStorage["kuoll-sequentQueue"];
    if (savedQueue) {
        queue = JSON.parse(savedQueue);
        delete window.sessionStorage["kuoll-sequentQueue"];
    }
}

export function add(sequent) {
    if (queue.length >= CRITICAL_QUEUE_SIZE || queue.length >= BATCH_SIZE && network.isQueueEmpty()) {
        queue.push(sequent);
        return sendBatch();
    } else if (lastFrameNum && sequent.frameNum !== lastFrameNum) {
        if (sequent.frameNum > lastFrameNum) {
            queue.push(sequent);
            return sendBatch();
        } else {
            reportError("Wrong order of saving sequents; Attempting to save sequent with lower frameNum than previous");
            return false;
        }
    } else {
        queue.push(sequent);
    }
    return false;
}

export function flush(callback) {
    lastFrameNum = 0;
    while (queue.length) {
        var sequents = getNextBatch();
        network.doPost("saveSequentsBatch", {
            sequents: sequents
        });
    }
    queue = [];
    if (callback) {
        callback();
    }
}

export function setSendingSequentsWhenNetworkQueueIsEmpty(value) {
    sendSequentsWhenNetworkQueueIsEmpty = value
}