import reportError from "./reportError";
import * as utils from "./utils";
import * as capturing from "./capturing";
import * as SequentQueue from "./SequentQueue";
import * as saving from "./saving";
import * as cycles from "./sequentCycles";
import * as recording from "../embedScript/embedFake";
import * as ignoring from "./ignoring";
import * as MouseCapturer from "./MouseCapturer";
import config from "config";

const IMPORTANT_SEQUENT_TYPES = ["mutation"];
const IMPORTANT_SEQUENT_CLASSES = ["MessageEvent", "XMLHttpRequestProgressEvent", "XMLHttpRequest", "History", "Console"];
const IMPORTANT_SEQUENT_SUBTYPES = ["error", "[initialPageLoad]"];

var isEmbedScript = config.artifact === "embed";

function getPreviousSequent(allSequents, sequent) {
    for (var i = 0; i < allSequents.length; ++i) {
        var prevSequent = allSequents[i];
        if (prevSequent && sequent.previousSequentNum === prevSequent.sequentNum && sequent.frameNum === prevSequent.frameNum) {
            return prevSequent;
        }
    }
    return null;
}

function deleteSequentBySequentNum(allSequents, sequentNum) {
    for (var i = 0; i < allSequents.length; ++i) {
        var prevSequent = allSequents[i];
        if (prevSequent && prevSequent.sequentNum === sequentNum) {
            delete allSequents[i];
            return true;
        }
    }
    return false;
}

function callNextChain(sequent) {
    if (this.nextChain) {
        this.nextChain.doChain(sequent);
    }
}

function buildChain(links) {
    for (var i = 0; i < links.length; ++i) {
        var link = links[i];

        link.callNextChain = callNextChain;

        if (i < links.length - 1) {
            link.nextChain = links[i + 1];
        }

        if (link.init) {
            link.init();
        }
    }
}

var StyleMutationFilterChain = {

    init: function () {
        this.firstSequent = null;
        this.lastSequent = null;
        this.timeout = null;
    },

    doChain: function (sequent) {
        var self = this;
        function saveLastSequent() {
            if (self.firstSequent !== self.lastSequent) {
                self.callNextChain(self.lastSequent);
            }
            self.timeout = null;
            self.firstSequent = null;
            self.lastSequent = null;
        }
        if (sequent.isStyleMutation) {
            if (this.timeout === null) {
                this.timeout = setTimeout(saveLastSequent, 400);
                this.firstSequent = this.lastSequent = sequent;
                this.callNextChain(sequent);
            } else {
                if (sequent.time - this.firstSequent.time > 400) {
                    clearTimeout(self.timeout);
                    saveLastSequent();
                } else {
                    this.lastSequent = sequent;
                }
            }
        } else {
            this.callNextChain(sequent);
        }
    }
};

var IgnoredSequentsChain = {
    doChain: function (sequent) {
        if (ignoring.isCssIgnoringActive() && sequent.browserEvent && sequent.browserEvent.target && sequent.browserEvent.target.matches) {
            var isIgnored = ignoring.isNodeIgnored(sequent.browserEvent.target);
            if (isIgnored === true) {
                return;
            } else if (isIgnored !== false) {
                reportError("Error while matching ignore rule. " + isIgnored.rule, null, isIgnored.target);
            }
            this.callNextChain(sequent);
        } else {
            this.callNextChain(sequent);
        }
    }
};

var ReadyStateChangeSequentChain = {

    init: function () {
        this.sequentsOnHold = [];
        this.firstIndex = 0;
        this.timeout = null;
    },

    doChain: function (sequent) {

        if (IMPORTANT_SEQUENT_CLASSES   .indexOf(sequent.sequentClass)      !== -1 ||
            IMPORTANT_SEQUENT_TYPES     .indexOf(sequent.sequentType)       !== -1 ||
            IMPORTANT_SEQUENT_SUBTYPES  .indexOf(sequent.sequentSubtype)    !== -1) {
            var prevSequent = getPreviousSequent(this.sequentsOnHold, sequent);
            if (prevSequent) {
                this.callNextChain(prevSequent);
                deleteSequentBySequentNum(this.sequentsOnHold, prevSequent);
            }
            this.callNextChain(sequent);
        } else if (sequent.sequentSubtype === "readystatechange" && !sequent.valuable) {
            this.sequentsOnHold.push(sequent);
            if (this.sequentsOnHold.length > 10) {
                delete this.sequentsOnHold[this.firstIndex++];
            }
        } else {
            this.callNextChain(sequent);
        }
    },
    clear: function () {
        this.sequentsOnHold = [];
    }
};

var PostMessageChain = {

    init: function () {
        this.constrainedSequents = [];
        this.firstIndex = 0;
    },

    prohibitedOrigins: ["http://localhost:8080", "https://www.kuoll.com", "http://www.kuoll.com"],
    origin: window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port: ''),

    isConstrained: function (frameNum, sequentNum) {
        for (var i = 0; i < this.constrainedSequents.length; ++i) {
            var data = this.constrainedSequents[i];
            if (data.frameNum === frameNum && data.sequentNum === sequentNum) {
                return true;
            }
        }
        return false;
    },

    doChain: function (sequent) {
        if (sequent.sequentClass === "MessageEvent" && sequent.sequentSubtype === "message") {
            fillRawData(sequent, false);
            var rawData = sequent.rawData;
            if (rawData !== null) {
                if (this.prohibitedOrigins.indexOf(rawData.origin) !== -1 && this.prohibitedOrigins.indexOf(this.origin) === -1) {
                    this.constrainedSequents.push({
                        frameNum: sequent.frameNum,
                        sequentNum: sequent.sequentNum
                    });
                    // TODO vlad: Functionality duplication: fixed-sized queue here and in MouseCapturer.queue
                    if (this.constrainedSequents.length > 10) {
                        delete this.constrainedSequents[this.firstIndex++];
                    }
                    return;
                }
                if (rawData.data) {
                    rawData.data = JSON.stringify(rawData.data);
                }
            }
        } else if (sequent.sequentType === "mutation" && this.isConstrained(sequent.frameNum, sequent.previousSequentNum)) {
            return;
        }
        this.callNextChain(sequent);
    },
    clear: function () {
        this.constrainedSequents = [];
    }
};

var WindowResizeFilterChain = {

    init: function () {
        this.isPreviousSequentResize = false;
        this.lastWindowResizeTime = 0;
    },

    doChain: function (sequent) {
        if (sequent.sequentType === "event" && sequent.sequentSubtype === "resize") {
            if (!this.isPreviousSequentResize) {
                this.isPreviousSequentResize = true;
            } else if (utils.currentTimestamp() - this.lastWindowResizeTime < 1000) {
                return;
            }
            this.lastWindowResizeTime = utils.currentTimestamp();
            this.callNextChain(sequent);
        } else {
            this.isPreviousSequentResize = false;
            this.callNextChain(sequent);
        }
    }
};

var UnvaluableFilterChain = {

    init: function () {
        this.sequentsOnHold = [];
        this.firstIndex = 0;
    },

    doChain: function (sequent) {
        // TODO vlad: extract checking sequent's importance
        if (IMPORTANT_SEQUENT_CLASSES   .indexOf(sequent.sequentClass)      !== -1 ||
            IMPORTANT_SEQUENT_TYPES     .indexOf(sequent.sequentType)       !== -1 ||
            IMPORTANT_SEQUENT_SUBTYPES  .indexOf(sequent.sequentSubtype)    !== -1) {
            if (sequent.previousSequentNum) {
                var prevSequent = getPreviousSequent(this.sequentsOnHold, sequent);

                if (prevSequent !== null) {
                    this.callNextChain(prevSequent);
                    deleteSequentBySequentNum(this.sequentsOnHold, prevSequent);
                }
            }
        }
        if (sequent.valuable === false) {
            this.sequentsOnHold.push(sequent);
            if (this.sequentsOnHold.length > 20) {
                delete this.sequentsOnHold[this.firstIndex++];
            }
        } else {
            this.callNextChain(sequent);
        }
    },
    clear: function () {
        this.sequentsOnHold = [];
    }
};

var FlushOnPageHideChain = {
    doChain: function (sequent) {
        this.callNextChain(sequent);
        if (isEmbedScript && sequent.sequentSubtype === "visibilitychange" && sequent.hidden === true) {
            SequentQueue.flush();
        }
    }
};


function extractTargetButtonInfo(event) {
    // Button is something that people see as a clickable button

    var activeElts = {
        "A": 1,
        "BUTTON": 1,
        "INPUT": 1,
        "SELECT": 1,
        "OPTION": 1,
        "LABEL": 1
    };

    const target = event.target || event.srcElement;
    let currElt = target;

    var path = [];
    var index = 0;

    while (currElt && !(currElt.tagName in activeElts)) {
        currElt = currElt.parentElement;
    }

    var eltInfo = {};
    if (currElt) {
        eltInfo.tagName = currElt.tagName;
        eltInfo.id = currElt.id;
        eltInfo.type = currElt.type;
        eltInfo.title = currElt.title;
        eltInfo.value = currElt.value;
        eltInfo.text = currElt.outerText;            
        eltInfo.html = currElt.outerHTML;            
        eltInfo.href = currElt.href;            
        eltInfo.className = currElt.className;
        eltInfo.selectedOptionText = currElt.selectedOptions && currElt.selectedOptions[0] && currElt.selectedOptions[0].text;
    } else if (target) {
        eltInfo.tagName = target.tagName;   
        eltInfo.id = target.id;   
        eltInfo.className = target.className;
        eltInfo.title = target.title;
        eltInfo.href = target.href;            
        eltInfo.selectedOptionText = target.selectedOptions && target.selectedOptions[0] && target.selectedOptions[0].text;
    } else {
        return null;
    }

    return eltInfo;
}

var fillRawData = function (sequent, stringify) {
    if (sequent.rawData === undefined && sequent.browserEvent !== undefined) {
        try {
            sequent.rawData = utils.cutAndCopy(sequent.browserEvent);

            var target, targetSequent, targetLoadSequent;
            if (!sequent.rawData.target) {
                target = capturing.getSequentTarget(sequent.sequentNum);
                if (target) {
                    targetSequent = target.sequent;
                    targetLoadSequent = target.loadSequent;
                    delete target.sequent;
                    delete target.loadSequent;

                    sequent.rawData.target = target;
                }
            }

            try {
                sequent.rawData.eventClass = utils.getConstructorName(sequent.browserEvent);
            } catch (e) {
                reportError(sequent.rawData + "; " + typeof sequent.browserEvent, e);
            }

            if (sequent.browserEvent) {
                sequent.rawData.eventTargetInfo = extractTargetButtonInfo(sequent.browserEvent);
            }

            if (stringify) {
                sequent.rawData = JSON.stringify(sequent.rawData);
            }

            if (target) {
                target.sequent = targetSequent;
                target.loadSequent = targetLoadSequent;
                capturing.resetSequentTarget(sequent.sequentNum);
            }
        } catch (e) {
            reportError("Error while copying browser event", e);
            sequent.rawData = null;
        }
    } else if (typeof sequent.rawData === "object" && !!stringify) {
        sequent.rawData = JSON.stringify(sequent.rawData);
    }
};

var FillRawDataChain = {
    doChain: function (sequent) {
        fillRawData(sequent, true);
        delete sequent.browserEvent;
        this.callNextChain(sequent);
    }
};

var FillBrowserStateInfoChain = {
    doChain: function (sequent) {
        sequent.scrollX = utils.getPageXOffset();
        sequent.scrollY = utils.getPageYOffset();
        sequent.windowWidth = window.innerWidth;
        sequent.windowHeight = window.innerHeight;
        sequent.mouseTrail = JSON.stringify(MouseCapturer.getMousePositions());
        this.callNextChain(sequent);
    }
};

var SaveChain = {
    doChain: function (sequent) {
        saving.saveSequent(sequent);
        this.callNextChain(sequent);
    }
};

var links = [StyleMutationFilterChain, IgnoredSequentsChain, ReadyStateChangeSequentChain, PostMessageChain,
    WindowResizeFilterChain, UnvaluableFilterChain, FlushOnPageHideChain, FillRawDataChain,
    FillBrowserStateInfoChain, SaveChain];
buildChain(links);

export function save(sequent) {
    if (recording.isActive()) {
        if (!capturing.isInitialSequentRecorded()) {
            capturing.recordInitialSequent();
        }
        sequent.frameTime = utils.highResolutionTimestamp();

        cycles.uncycleSequent(sequent, function () {
            try {
                StyleMutationFilterChain.doChain(sequent);
            } catch (e) {
                reportError("Sequent saving failed", e);
            }
        });
    }
}

export function clearBuffer() {
    UnvaluableFilterChain.clear();
    ReadyStateChangeSequentChain.clear();
    PostMessageChain.clear();
}
