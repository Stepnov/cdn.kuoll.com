import {calculateStacktraceHash} from "./utils";
import reportError from "./reportError";
import * as network from "./network";
import {Promise, XMLHttpRequest} from "./origObjects";
import StackTraceGPS from "./stacktrace-gps"

let gps;

export function init() {
    gps = new StackTraceGPS({
        // Prevent logging requests made by StackTraceGPS
        ajax: (url) =>
            new Promise(function (resolve, reject) {
                let req = new XMLHttpRequest();
                req.open('get', url);
                req.onerror = reject;
                req.onreadystatechange = function onreadystatechange() {
                    if (req.readyState === 4) {
                        if ((req.status >= 200 && req.status < 300) ||
                            (url.substr(0, 7) === 'file://' && req.responseText)) {
                            resolve(req.responseText);
                        } else {
                            reject("");
                        }
                    }
                };
                req.send();
            })
    });
}

export function restoreStacktrace(stacktrace, hash) {
    Promise.all(stacktrace.filter(sf => !!sf.fileName).map(function (stackFrame) {
        // stackframe from eval contains only column number. We set missing coordinate to 1
        stackFrame.columnNumber = stackFrame.columnNumber || 1;
        stackFrame.lineNumber = stackFrame.lineNumber || 1;
        return gps.pinpoint(stackFrame).catch(function (error) {
            return stackFrame;
        })
    })).then(function (restoredStacktrace) {
        const restoredStacktraceHash = calculateStacktraceHash(restoredStacktrace, document.location.href);
        if (restoredStacktraceHash !== hash) {
            network.doPost("put_restored_stacktrace", {
                hash: hash,
                restoredStacktraceString: JSON.stringify(restoredStacktrace),
                restoredStacktraceHash: restoredStacktraceHash
            });
        }
    }, function (error) {
        reportError("Could not restore StackTrace", null, {
            error: error
        });
    });
}
