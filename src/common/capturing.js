import config from "config";
import * as utils from "./utils";
import * as logging from "./logging";
import reportError from "./reportError";
import * as saving from "./saving";
import * as MutationSaver from "./MutationSaver";
import * as ignoring from "./ignoring";
import * as SequentSaver from "./SequentSaver";
import * as MouseCapturer from "./MouseCapturer";
import * as recording from "../embedScript/embedFake";
import * as LocalRequestsStorage from "../embedScript/LocalRequestsStorage";
import * as ConversionMonitoring from "../embedScript/ConversionMonitoring";

const ExceptionDetection = window.kuoll.ExceptionDetection;

var initialSequent = null;
var currentSequent = null;

// Used for preserving causality for drag sequents
var dragSequent = null;

var seriesNextNum = 1;
var sequentNextNum = 1; // index

var recordId;

var captured = false; // FIXME @Vlad `captured` is a bad name, please rename

var listeners = {};
var cookieSetterOrig, cookieGetterOrig;

/**
 * IE<10 don't provide target property for some elements.
 * We can define target when event is just captured, but we can't assign it to event.target
 * since it's read-only. If event.target is empty, one should use this property to get the target.
 */
var sequentTarget = {};

function getSeriesSequent(previousSequent) {
    var thisSequentNum;
    var thisSequentSeriesNum;
    var previousSequentNum;

    if (previousSequent) {
        thisSequentSeriesNum = previousSequent.seriesNum;
        previousSequentNum = previousSequent.sequentNum;
    } else {
        thisSequentSeriesNum = seriesNextNum++;
        previousSequentNum = 0;
    }
    thisSequentNum = sequentNextNum++;

    return {
        seriesNum: thisSequentSeriesNum,
        sequentNum: thisSequentNum,
        previousSequentNum: previousSequentNum
    };
}

let lastError = null;
function onUncaughtError(error) {
    if (error && lastError === error) {
        return;
    }

    var errorMessage;
    if (!error) {
        reportError("Kuoll could not retrieve error details");
        errorMessage = null;
    } else {
        errorMessage = error.message;
    }

    if (error && error.__kuoll_processed__) {
        return;
    }

    var errorEvent = new ErrorEvent("error", {
        error: error,
        message: errorMessage
    });

    errorEvent.stack = utils.parseStacktrace(error);
    errorEvent.stacktraceHash = utils.calculateStacktraceHash(errorEvent.stack, document.location.href);

    recordBrowserEvent(null, errorEvent);
    lastError = error;

    if (!error.__kuoll_issue_created) {
        if (config.artifact === "embed" && recording && recording.getRecordingInfo().createIssueOn.error && !LocalRequestsStorage.ignoreErrors() && !errorEvent.ignoredError) {
            createIssue("JavaScript error", errorEvent.message, errorEvent.stack);
        }
    }

    if (error) {
        error.__kuoll_processed__ = true;
    }
}

function createIssue(type, description, stacktrace) {
    if (window[config.globalName] instanceof Function) {
        window[config.globalName]("createIssue", type, description, stacktrace || utils.getStacktrace());
    } else {
        logging.log("Tried to automatically create issue, but window[config.globalName] is not a function");
    }
}

function recordMethodInterception(previousSequent, interception, sequentClass, valuable) {
    if (!interception.sequent) {
        var sequent = getSeriesSequent(previousSequent && previousSequent.recordId === recordId ? previousSequent : null);
        if (sequent.sequentNum === sequent.previousSequentNum) {
            reportError("sequentNum == previousSequentNum", null, JSON.stringify([previousSequent, sequent], null, 2));
        }
        if ("XMLHttpRequest" === sequentClass && "send" === interception.methodName) {
            var inputs = document.getElementsByTagName("input");
            for (var i = 0; i < inputs.length; ++i) {
                if ("password" === inputs[i].getAttribute("type")) {
                    var password = inputs[i].value;
                    if (password) {
                        interception.args[0] = interception.args[0] && interception.args[0].split(password).join("<hidden by Kuoll>");
                    }
                }
            }
        }

        var stacktrace = utils.getStacktrace();
        var fullSequent = {
            seriesNum: sequent.seriesNum,
            sequentNum: sequent.sequentNum,
            previousSequentNum: sequent.previousSequentNum,
            sequentType: "interception",
            sequentClass: sequentClass,
            sequentSubtype: interception.methodName,

            stack: stacktrace,
            stacktraceHash: utils.calculateStacktraceHash(stacktrace, document.location.href),

            rawData: JSON.stringify(interception),

            recordId: recordId
        };
        if (typeof valuable !== "undefined") {
            fullSequent.valuable = valuable;
        }
        interception.sequent = fullSequent;
        SequentSaver.save(fullSequent);
        return fullSequent;
    }
    return interception.sequent;
}

/**
 * @returns {boolean} true if nothing but style attributes was mutated
 */
function isStyleMutation(mutations) {
    if (mutations.removed.length !== 0 || mutations.addedOrMoved.length !== 0 ||
        mutations.propertiesChanged.length !== 0 || mutations.text.length !== 0) {
        return false;
    }
    for (var i = 0; i < mutations.attributes.length; ++i) {
        var attr = mutations.attributes[i].attributes;
        var attrNames = Object.keys(attr);
        if (attrNames.length !== 1 || !("style" in attr)) {
            return false;
        }
    }
    return true;
}

function stringifyMutations(mutations) {
    return JSON.stringify(mutations);
}

function createFunctionProxy(object, fn, methodName, loggable, objectLevel, callback) {

    var proxy = function () {
        try {
            if (loggable && (typeof callback === "undefined" || callback(object, fn, methodName, arguments))) {
                var prevSequent;
                if (objectLevel) {
                    prevSequent = window.event ? recordBrowserEvent(null, window.event) : object.sequent || currentSequent;
                } else {
                    prevSequent = currentSequent ? currentSequent : (window.event ? recordBrowserEvent(null, window.event) : null);
                }
                var sequent = recordMethodInterception(prevSequent, {
                    methodName: methodName,
                    args: utils.cutAndCopy(arguments)
                }, utils.getConstructorName(object));
                proxy.sequent = sequent;
                object.sequent = sequent;
            }
        } catch (e) {
            reportError("Error in function proxy. function: " + methodName, e);
        }

        return fn.apply(object, arguments);
    };
    proxy.fn = fn;
    proxy.__kuoll_orig = fn;
    object[methodName + "_orig"] = fn;
    return proxy;
}

function callFnWithCausingSequent(fn, object, args, sequent) {
    try {
        currentSequent = sequent;
        return fn.apply(object, args);
    } finally {
        currentSequent = null;
    }
}

function createScheduledCallbackProxy(fn, callback, causingSequent, fnName) {
    var proxy;

    if (typeof callback === "function") {
        proxy = function () {
            var sequent = recordMethodInterception(causingSequent, {methodName: fnName}, "[" + fnName + "-callback]", false);
            proxy.sequent = sequent;
            callFnWithCausingSequent(callback, null, arguments, sequent);
        };
        proxy.fn = fn;
    } else if (typeof callback === "string") {
        proxy = function () {
            var sequent = recordMethodInterception(causingSequent, {methodName: fnName}, "[" + fnName + "-callback]", false);
            proxy.sequent = sequent;
            callback = new Function(callback);
            callFnWithCausingSequent(callback, null, arguments, sequent);
        };
        proxy.fn = fn;
    } else {
        // just ignore, we can do -- show potential source of problems
    }

    return proxy;
}

function createEventListenerProxy(object, fn, callback) {
    if (!fn) {
        return fn;
    }
    if (typeof fn !== "function") {
        // reportError("Attempt to proxy a non-function", null, fn);
        return fn;
    }

    return function (event) {
        if (typeof callback === "undefined" || callback(object, fn, fn.name)) {
            var prevSequent = object.sequent || null;
            var sequent = recordBrowserEvent(prevSequent, event, false);
            callFnWithCausingSequent(fn, object, arguments, sequent);
        } else {
            fn.apply(object, arguments);
        }
    };
}

function createAddEventListenerProxy(object, fn) {
    return function (eventName, eventListener) {
        var event = window.event;
        var prevSequent = object.sequent || currentSequent;
        if (event) {
            recordBrowserEvent(prevSequent, event, false);
        }

        var proxy = createEventListenerProxy(object, eventListener);
        var args = Array.prototype.slice.call(arguments);
        args[1] = proxy;
        return fn.apply(object, args);
    };
}

function wrapObject(wrapper, obj, fns, listenerNames, callback) {

    function wrapProperty(propertyName) {
        if (propertyName === "addEventListener") {
            wrapper.addEventListener = createAddEventListenerProxy(obj, obj.addEventListener);
        } else if (typeof obj[propertyName] === "function") {
            wrapper[propertyName] = createFunctionProxy(obj, obj[propertyName], propertyName, propertyName in fns, true, callback);
        } else {
            var descriptor = {
                set: function (value) {
                    obj[propertyName] = (listenerNames.indexOf(propertyName) >= 0) ? createEventListenerProxy(obj, value, callback) : value;
                },
                get: function () {
                    return obj[propertyName];
                }
            };

            Object.defineProperty(wrapper, propertyName, descriptor);
        }
    }

    for (var propertyName in obj) {
        wrapProperty(propertyName);
    }

    return wrapper;
}

function getCookie(name) {
    var result = null;
    document.cookie.split(";").forEach(function (cookie) {
        var parts = cookie.split("=");
        if (parts[0].trim() == name) {
            result = parts[1];
        }
    });
    return result;
}

function registerListeners(eventNames) {
    eventNames.forEach(function (name) {
        var listener = function (e) {
            recordBrowserEvent(null, e);
        };
        listeners[name] = {obj: window, fn: listener};
        window.addEventListener(name, listener, true);
    });
}

function registerListener(obj, eventName, callback) {
    listeners[eventName] = {obj: obj, fn: callback};
    window.addEventListener(eventName, callback, true);
}

function startCapturing() {
    if (captured) {
        return;
    }

    window.XMLHttpRequest_orig = window.XMLHttpRequest;
    window.XMLHttpRequest = function XMLHttpRequest(parameters) {
        var xhr = new window.XMLHttpRequest_orig(parameters);
        try {
            xhr.__kuoll_stacktrace = xhr.__kuoll_stacktrace || utils.getStacktrace();
            var responseCallback = function (event) {
                try {
                    if (xhr.readyState === 4) { // 4 means that the request is complete
                        xhr.responseHeaders = xhr.getAllResponseHeaders();
                        var prevSequent = xhr.sequent || this.sequent;
                        if (event.target === undefined) {
                            sequentTarget[sequentNextNum + 1] = xhr;
                        }
                        recordBrowserEvent(prevSequent, event, true);

                        if (xhr.status >= 400) {
                            if (recording.getRecordingInfo().createIssueOn.serverError) {
                                const url = xhr.responseURL.indexOf('?') === -1 ? xhr.responseURL : xhr.responseURL.substring(0, xhr.responseURL.indexOf('?'));
                                createIssue("Server error", xhr.status + " " + url, xhr.__kuoll_stacktrace);
                            }
                            recordBrowserEvent(null, {
                                type: "xhrError",
                                target: xhr
                            }, true);
                            ConversionMonitoring.xhrError(xhr);
                        }
                    }
                } catch (e) {
                    reportError("Error while processing XHR sequent", e);
                }
            };
            xhr.addEventListener("readystatechange", responseCallback);
            xhr = wrapObject(this, xhr,
                {

                    "send": true, // this method is special. It is event-causing.
                    //"setRequestHeader": false,
                    "open": false,
                    //"getAllResponseHeaders": false,
                    //"getResponseHeader": false,
                    //"overrideMimeType": false
                    "abort": true // this is special too, event causing "onabort"
                },
                ["onprogress", "onabort", "onerror", "onload", "ontimeout", "onloadend", "onreadystatechange", "onloadstart"],
                function shouldRecord(obj, func, fnName, args) {
                    if (obj instanceof XMLHttpRequest) {
                        if (fnName === "open") {
                            // convert URL to absolute
                            var a = document.createElement("a");
                            a.setAttribute("href", args[1]);
                            obj.url = a.href;
                        }

                        return !ignoring.isUrlIgnored(obj.url);
                    } else {
                        return true;
                    }
                }
            );
        } catch (e) {
            reportError("Error in XHR constructor proxy", e);
        }
        return xhr;
    };
    window.XMLHttpRequest.__kuoll_orig = window.XMLHttpRequest_orig;
    try {
        Object.getOwnPropertyNames(window.XMLHttpRequest_orig)
            .filter((name) => Object.getOwnPropertyDescriptor(window.XMLHttpRequest_orig, name).enumerable)
            .forEach((name) => window.XMLHttpRequest[name] = window.XMLHttpRequest_orig[name]);
    } catch (e) {
        logging.error("Could not proxy XMLHttpRequest due to error", e);
        window.XMLHttpRequest = window.XMLHttpRequest_orig;
    }

    window.Promise_orig = window.Promise;
    window.Promise = function Promise() {
        var promise;
        var executor = arguments[0];
        try {
            var prevSequent = window.event ? recordBrowserEvent(null, window.event) : currentSequent;
            promise = new window.Promise_orig(executor);
            var sequent = currentSequent = recordMethodInterception(prevSequent, {
                methodName: "constructor",
                args: executor ? [executor.toString()] : [null]
            }, "Promise", false);
            promise.then(function onFulfilled() {
                currentSequent = recordMethodInterception(sequent, {
                    methodName: "onFulfilled",
                    args: utils.cutAndCopy(arguments)
                }, "Promise", false);
            }, function onRejected(error) {
                currentSequent = recordMethodInterception(sequent, {
                    methodName: "onRejected",
                    args: utils.cutAndCopy(arguments)
                }, "Promise", false);
                throw error;
            });
        } catch (e) {
            reportError("Error while proxying Promise", e);
            promise = new window.Promise_orig(executor);
        }
        return promise;
    };
    try {
        Object.getOwnPropertyNames(window.Promise_orig)
            .filter((name) => Object.getOwnPropertyDescriptor(window.Promise_orig, name).writable)
            .forEach((name) => window.Promise[name] = window.Promise_orig[name]);
    } catch (e) {
        logging.error("Could not proxy Promise due to error", e);
        window.Promise = window.Promise_orig;
    }

    window.history.pushState = createFunctionProxy(window.history, window.history.pushState, "pushState", true);
    window.console.log = createFunctionProxy(window.console, window.console.log, "log", true);
    window.console.error = createFunctionProxy(window.console, window.console.error, "error", true, true,
        function (obj, origFn, fnName, args) {
            if (recording && recording.getRecordingInfo().createIssueOn.consoleError && !LocalRequestsStorage.ignoreErrors()) {
                let description;
                let issueCreated = false;
                if (args) {
                    if (args.length === 1) {
                        if (args[0] instanceof Error) {
                            const error = args[0];
                            createIssue("consoleError", error.message, utils.parseStacktrace(error));
                            issueCreated = true;
                        } else {
                            description = args[0];
                        }
                    } else if (args.length > 1)  {
                        description = Array.prototype.join.call(args, "; ");
                    }
                }

                if (!issueCreated) {
                    createIssue("consoleError", description);
                }
            }
            return false;
        });

    window.postMessage = createFunctionProxy(window, window.postMessage, "postMessage", true);

    registerListeners(["beforeunload", "drop", "message"]);

    registerListener(window, "click", function (e) {
        recordBrowserEvent(null, e);
        MutationSaver.forceCheckValueChanges();
    });
    registerListener(window, "keypress", function (e) {
        recordBrowserEvent(null, e);
        MutationSaver.forceCheckValueChanges();
    });
    registerListener(window, "keyup", function (e) {
        if ((e.keyCode || e.which || e.charCode) === 8) { // backspace key
            recordBrowserEvent(null, e);
            MutationSaver.forceCheckValueChanges();
        }
    });

    registerListener(window, "dragstart", function (e) {
        dragSequent = recordBrowserEvent(null, e);
    });

    registerListener(document, "visibilitychange", function (event) {
        event.hidden = document.hidden;
        recordBrowserEvent(null, event);
    });
    registerListener(window, "resize", function (event) {
        event.screen = window.screen;
        event.width = window.innerWidth;
        event.height = window.innerHeight;
        recordBrowserEvent(null, event);
    });
    //document.addEventListener("scroll", function (event) {
    //    event.screen = window.screen;
    //    event.pageXOffset = window.pageXOffset;
    //    event.pageYOffset = window.pageYOffset;
    //    recordBrowserEvent(null, event);
    //});

    registerListener(window, "kuollEcommerceEvent", function (event) {
        const ecomEvent = event.detail;
        recordBrowserEvent(null, {
            type: "ecommerce",
            hashes: ecomEvent.hashes,
            convertedToStep: ecomEvent.convertedToStep,
            
            // TODO vlad: traffic source should be a part of RecordInfo, but that would require integrating ConvMon with regular recording which may be complicated
            trafficSourceName: ecomEvent.trafficSourceName,
        }, true);
    });

    ExceptionDetection.install();
    ExceptionDetection.subscribe(onUncaughtError);

    window.setTimeout_orig = window.setTimeout;
    window.setTimeout = function () {
        var prevSequent = window.event ? recordBrowserEvent(null, window.event) : currentSequent;
        var sequent = recordMethodInterception(prevSequent, {methodName: "setTimeout"}, "Window", false);
        var proxy = createScheduledCallbackProxy(setTimeout_orig, arguments[0], sequent, "setTimeout");

        var args = Array.prototype.slice.call(arguments);
        args[0] = proxy;

        return window.setTimeout_orig.apply(window, args);
    };

    window.setInterval_orig = window.setInterval;
    window.setInterval = function () {
        var prevSequent = window.event ? recordBrowserEvent(null, window.event) : currentSequent;
        var sequent = recordMethodInterception(prevSequent, {methodName: "setInterval"}, "Window", false);
        var proxy = createScheduledCallbackProxy(setInterval_orig, arguments[0], sequent, "setInterval");

        var args = Array.prototype.slice.call(arguments);
        args[0] = proxy;

        return window.setInterval_orig.apply(window, args);
    };

    window.clearTimeout_orig = window.clearTimeout;
    window.clearTimeout = function () {
        var prevSequent = window.event ? recordBrowserEvent(null, window.event) : currentSequent;
        recordMethodInterception(prevSequent, {methodName: "clearTimeout"}, "Window", false);
        return window.clearTimeout_orig.apply(window, arguments);
    };

    window.clearInterval_orig = window.clearInterval;
    window.clearInterval = function () {
        var prevSequent = window.event ? recordBrowserEvent(null, window.event) : currentSequent;
        recordMethodInterception(prevSequent, {methodName: "clearInterval"}, "Window", false);
        return window.clearInterval_orig.apply(window, arguments);
    };

    if (document.__lookupGetter__ && document.__lookupSetter__) {
        try {
            cookieSetterOrig = document.__lookupSetter__("cookie");
            cookieGetterOrig = document.__lookupGetter__("cookie");
            if (cookieGetterOrig && cookieSetterOrig) {
                Object.defineProperty(document, "cookie", {
                    get: function () {
                        return cookieGetterOrig.apply(document);
                    },
                    set: function () {
                        var prevSequent = window.event ? recordBrowserEvent(null, window.event) : currentSequent;
                        var cookie = arguments[0];
                        var cookieName = cookie.slice(0, cookie.indexOf("="));
                        if (cookieName && cookieName.indexOf("kuoll") !== 0) {
                            recordMethodInterception(prevSequent, {
                                methodName: "document.cookie_set",
                                oldValue: getCookie(cookieName),
                                value: cookie
                            }, "[cookie_set]", true);
                        }
                        return cookieSetterOrig.apply(document, arguments);
                    },
                    configurable: true
                });
            }
        } catch (e) {
            reportError("User's browsers doesn't support cookie proxying", e);
        }
    }

    //window.eval_orig = window["eval"];
    //window.eval = function () {
    //    var prevSequent = window.event ? recordBrowserEvent(null, window.event) : currentSequent;
    //    recordMethodInterception(prevSequent, {methodName: "eval", expression: arguments[0]}, "Window", true);
    //    return window.eval_orig.apply(window, arguments);
    //};

    captured = true;
}

function stopCapturing() {
    window.XMLHttpRequest = window.XMLHttpRequest_orig;
    window.Promise = window.Promise_orig;
    window.history.pushState = window.history.pushState_orig;
    window.console.log = window.console.log_orig;
    window.console.error = window.console.error_orig;
    window.postMessage = window.postMessage_orig;
    window.setTimeout = window.setTimeout_orig;
    window.clearTimeout = window.clearTimeout_orig;
    window.setInterval = window.setInterval_orig;
    window.clearInterval = window.clearInterval_orig;

    for (var eventName in listeners) {
        var listenerInfo = listeners[eventName];
        listenerInfo.obj.removeEventListener(eventName, listenerInfo.fn, true);
    }
    listeners = [];

    if (ExceptionDetection) {
        ExceptionDetection.unsubscribe(onUncaughtError);
    }

    if (cookieGetterOrig && cookieSetterOrig) {
        Object.defineProperty(document, "cookie", {
            get: cookieGetterOrig,
            set: cookieSetterOrig,
            configurable: true
        });
    }

    captured = false;
}

function gatherRecordInfo() {
    var cutAndCopy = utils.cutAndCopy;
    var navi = {
        doNotTrack: cutAndCopy(navigator.doNotTrack),
        onLine: cutAndCopy(navigator.onLine),
        languages: cutAndCopy(navigator.languages),
        language: cutAndCopy(navigator.language),
        appCodeName: cutAndCopy(navigator.appCodeName),
        product: cutAndCopy(navigator.product),
        platform: cutAndCopy(navigator.platform),
        appVersion: cutAndCopy(navigator.appVersion),
        appName: cutAndCopy(navigator.appName),
        hardwareConcurrency: cutAndCopy(navigator.hardwareConcurrency),
        maxTouchPoints: cutAndCopy(navigator.maxTouchPoints),
        vendorSub: cutAndCopy(navigator.vendorSub),
        vendor: cutAndCopy(navigator.vendor),
        productSub: cutAndCopy(navigator.productSub),
        cookieEnabled: cutAndCopy(navigator.cookieEnabled),
        mimeTypes: "<skipped>",
        plugins: "<skipped>"
    };
    var isEmbedScript = !!window.kuollRecording;
    var firstUrl = document.location.href;
    var screen = cutAndCopy(window.screen);

    var startParams;
    if (isEmbedScript) {
        var startParamsOrig = window.kuollRecording.getRecordingInfo().startParams;
        try {
            startParams = JSON.parse(startParamsOrig);
        } catch (e) {
            startParams = startParamsOrig;
        }
        if (startParams.startCallback) {
            startParams = startParams.startCallback.toString();
        }
        startParams = JSON.stringify(startParams);
    }

    saving.putRecordInfo({
        navigator: JSON.stringify(navi),
        userAgent: navigator.userAgent,
        recordingType: isEmbedScript ? "embedScript" : "extension",
        screen: JSON.stringify(screen),
        startParams: startParams,
        firstUrl: firstUrl,
        trafficSourceName: ConversionMonitoring.getTrafficSourceName(),
        initialPageVisibility: !document.hidden
    });
}

function onDomReady() {
    startCapturing();

    gatherRecordInfo();

    if (initialSequent === currentSequent || currentSequent === null) {
        currentSequent = null;
    } else {
        reportError("UNEXPECTED: kuoll.sequent has been changed before window.onload. currentSequent=" + JSON.stringify(currentSequent));
    }

    if (window.event && window.event.type !== "kuollInterdom") {
        recordBrowserEvent(null, window.event);
    }

    if (typeof window.MutationObserver !== "undefined") {
        MutationSaver.init();
    } else {
        reportError("User's browser doesn't support MutationObserver");
        logging.warn("Kuoll: Your browser doesn't support MutationObserver. Kuoll will not record page changes, but will record everything else.");
    }
}

export function getCurrentSequent() {
    return currentSequent;
}

export function getNextSequentNum() {
    return sequentNextNum;
}

export function recordBrowserEvent(previousSequent, browserEvent, valuable) {
    if (!browserEvent) {
        return;
    }

    if (browserEvent.sequent) {
        return browserEvent.sequent;
    }

    if (typeof browserEvent === "string") {
        return;
    }

    if (browserEvent.type === "kuollInterdom") {
        return;
    }

    var target = browserEvent.target || sequentTarget;
    if (target instanceof Element && (ignoring.isNodeIgnored(target) || ignoring.isNodeEventIgnored(target, browserEvent))) {
        return;
    }

    var eventConstructorName = utils.getConstructorName(browserEvent);
    var isXhrResponseEvent = (
        (eventConstructorName === "XMLHttpRequestProgressEvent" || eventConstructorName === "ProgressEvent")
        && browserEvent.type === "load" || browserEvent.type === "loadend"
        || (browserEvent.type === "readystatechange" && browserEvent.target.readyState === 4)
    );
    if (isXhrResponseEvent) {
        if (ignoring.isUrlIgnored(target.responseURL)) {    
            return;
        }
        if (target.loadSequent) {
            return target.loadSequent;
        }
    }

    if (eventConstructorName === "MessageEvent" && utils.getConstructorName(browserEvent.target) === "MessagePort") {
        // MessageEvent caused by MessagePort.postMessage rather than Window.postMessage. Such sequents don't
        // contain any interesting information, so just ignore it.
        return;
    }

    if (eventConstructorName === "DragEvent") {
        if (browserEvent.type === "drop") {
            previousSequent = dragSequent;
        } else if (browserEvent.type === "dragover" || browserEvent.type === "dragenter" || browserEvent.type === "dragleave") {
            previousSequent = dragSequent;
            valuable = false;
        } else if (browserEvent.type === "dragend") {
            previousSequent = dragSequent;
            dragSequent = null;
            valuable = false;
        }
    }

    var sequent = getSeriesSequent(previousSequent);

    var activeElement = "";
    try {
        activeElement = document.activeElement && utils.getPath(document.activeElement);
    } catch (e) {
        reportError("Error while building CSS path of active DOM element", e);
    }

    var fullSequent = {
        seriesNum: sequent.seriesNum,
        sequentNum: sequent.sequentNum,
        previousSequentNum: sequent.previousSequentNum,
        sequentType: "event",
        sequentSubtype: browserEvent.type,
        sequentClass: eventConstructorName,

        activeElement: activeElement,

        hidden: browserEvent.hidden,

        browserEvent: browserEvent,

        valuable: valuable,

        recordId: recordId
    };
    try {
        browserEvent.sequent = fullSequent;
        if (isXhrResponseEvent) {
            target.loadSequent = fullSequent;
            target.responseHeaders = target.getAllResponseHeaders();
        }
    } catch (e) {
        logging.error(e);
    }

    if (fullSequent.sequentClass === "MouseEvent" && fullSequent.sequentSubtype !== "click") {
        fullSequent.valuable = false;
    }

    if (fullSequent.sequentClass === "ErrorEvent" && fullSequent.sequentType === "event" && !fullSequent.stack) {
        fullSequent.stack = utils.parseStacktrace(browserEvent.error);
    }
    SequentSaver.save(fullSequent);

    return fullSequent;
}

export function recordInitialSequent() {
    if (initialSequent) {
        return;
    }

    initialSequent = {};

    var initialPageLoad = recordBrowserEvent(null, {
        type: "[initialPageLoad]",
        clientTime: utils.currentTimestamp(),
        startParams: recording.getRecordingInfo().startParams
    }, true);
    currentSequent = initialSequent = initialPageLoad;
}

export function recordDomMutation(previousSequent, mutations) {
    var sequent;
    var fullSequent;

    var styleMutation = isStyleMutation(mutations);
    var mutationsStr = stringifyMutations(mutations);
    sequent = getSeriesSequent(previousSequent);
    fullSequent = {
        seriesNum: sequent.seriesNum,
        sequentNum: sequent.sequentNum,
        previousSequentNum: sequent.previousSequentNum,
        sequentType: "mutation",

        mutations: mutationsStr,

        isStyleMutation: styleMutation
    };
    SequentSaver.save(fullSequent);
    return fullSequent;
}

export function isInitialSequentRecorded() {
    return !!initialSequent;
}

export function getSequentTarget(sequentNum) {
    return sequentTarget[sequentNum];
}

export function resetSequentTarget(sequentNum) {
    delete sequentTarget[sequentNum];
}

export function init() {
    initialSequent = null;
    currentSequent = null;
    dragSequent = null;
    seriesNextNum = 1;
    sequentNextNum = 1;
    sequentTarget = {};
    MouseCapturer.init();

    recordId = recording.getRecordingInfo().recordId;

    if (document.readyState === "complete" || document.readyState === "loaded" || document.readyState === "interactive") {
        onDomReady();
    } else {
        document.addEventListener("DOMContentLoaded", onDomReady, true);
    }
}

export function shutdown() {
    MouseCapturer.shutdown();
    MutationSaver.shutdown();
    stopCapturing();
}