import * as ResourceLoader from "./resourceLoader";
import * as saving from "./saving";
import reportError from "./reportError";
import {currentTimestamp} from "./utils";
const origin = document.location.protocol + "//" + document.location.host;

const resourceTypes = {
    STYLE: "style",
    OTHER: "other"
};

const URL_RESOURCE_REGEXP = /url\(['"]{0,2}(.*?)['"]{0,2}\)/g;

export function saveResources() {
    if (document.body) {
        saveStyleSheets();
        loadAndSaveAllImages();
        loadAndSaveAllResourcesFromStyleAttributes();
    }
}

function loadAndSaveResourcesFromCss(css, href) {
    css.replace(URL_RESOURCE_REGEXP, function (match, p1) {
        if (p1.indexOf("data:") !== 0) {
            var path = href.substring(0, href.lastIndexOf("/"));
            ResourceLoader.loadResource(p1, origin, path, resourceTypes.OTHER);
        }
        return match;
    });
}

export function collectPerformanceData() {
    try {
        return window.performance ? JSON.stringify({
            timing: window.performance.timing,
            navigation: window.performance.navigation,
            memory: window.performance.memory
        }) : null;
    } catch (e) {
        reportError("Error while collecting performance data", e);
        return null;
    }
}

export function saveFrame() {
    const charset = (document.charset === "iso-8859-1") ? "utf-8" : document.charset;
    const performance = collectPerformanceData();
    const frame = {
        url: document.location.href,
        base: document.baseURI,
        charset: charset,
        referrer: document.referrer,
        performance: performance,
        clientStartTime: currentTimestamp()
    };
    saving.saveFrame(frame);
}

function loadAndSaveAllImages() {
    var images = document.images;
    for (var i = 0; i < images.length; ++i) {
        var image = images[i];
        var src = image.getAttribute("src");
        loadAndSaveImage(src);
    }
}

export function loadAndSaveImage(src) {
    if (src && src.indexOf("data:") !== 0) {
        ResourceLoader.loadResource(src, origin, "", resourceTypes.OTHER);
    }
}

function saveCssResources(styleSheet, path) {
    if (!styleSheet || !styleSheet.cssRules) return;

    for (var j = 0; j < styleSheet.cssRules.length; ++j) {
        var cssRule = styleSheet.cssRules[j];
        if (cssRule instanceof CSSStyleRule || cssRule instanceof CSSFontFaceRule) {
            var style = cssRule.style;
            if (!style) continue;

            for (var k = 0; k < style.length; ++k) {
                var css = style[style[k]];
                if (typeof(css) === "string") {
                    loadAndSaveResourcesFromCss(css, path);
                }
            }
        }
    }
}

function saveCssResourcesFromHref(styleSheet, path) {
    var xhr = new XMLHttpRequest();
    xhr.open("GET", styleSheet.href, true);
    xhr.onerror = function () {
        reportError("Could not load resource. url: " + styleSheet.href);
    };
    xhr.onload = function (/*XMLHttpRequestProgressEvent*/event) {
        loadAndSaveResourcesFromCss(xhr.responseText, path);
    };
    xhr.send();
}

function loadAndSaveAllResourcesFromStyleAttributes() {
    var iter = document.createNodeIterator(document.body, NodeFilter.SHOW_ALL, function (node) {
        return node.hasAttribute && node.hasAttribute("style")
    });
    var node;
    while ((node = iter.nextNode()) != null) {
        var style = node.getAttribute("style");
        loadAndSaveResourcesFromStyle(style);
    }
}

export function loadAndSaveResourcesFromStyle(style) {
    if (style.indexOf("url(") !== -1) {
        loadAndSaveResourcesFromCss(style, "");
    }
}

function saveStyleSheets() {
    var resourcesLoadingCount = 0;

    try {
        var styleSheets = document.styleSheets;
        for (var i = styleSheets.length - 1; i >= 0; i--) {
            var styleSheet = styleSheets.item(i);

            if (styleSheet.kuollSaved) {
                continue;
            }

            var href = styleSheet.href;
            if (!href) {
                // assert styleSheet.ownerElement == style and cssRules are not null
                ResourceLoader.loadInlineResource(styleSheet.ownerNode.innerText);
            } else {
                if (href.substr(0, 2) === "//") {
                    href = document.location.protocol + href;
                } else if (href.substr(0, 1) === "/") {
                    href = document.location.protocol + "//" + document.location.host + href;
                }
                var path = href.substring(0, href.lastIndexOf("/"));
                ResourceLoader.loadResource(href, origin, path, resourceTypes.STYLE);
            }
            if (styleSheet.cssRules) {
                saveCssResources(styleSheet, href || origin);
            } else {
                resourcesLoadingCount++;
                saveCssResourcesFromHref(styleSheet, href || origin);
            }
            styleSheet.kuollSaved = true;

        }
    } catch (e) {
        reportError("Error while parsing stylesheets", e);
    }
}

export function clearCache() {
    var styleSheets = document.styleSheets;
    for (var i = styleSheets.length - 1; i >= 0; i--) {
        var styleSheet = styleSheets.item(i);
        delete styleSheet.kuollSaved;
    }
}