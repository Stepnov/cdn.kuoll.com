var expectly = (function () {
    var expectations = [];
    var enabled = false;
    var ignoreList = [];

    function finishTesting() {
        if (expectations.length !== 0) {
            console.log("Not all expectations happened until testing finished");
        }
    }

    function ignore(fnName, json) {
        ignoreList.push({
            fnName: fnName,
            json: json
        });
    }

    function unignore(fnName, json) {
        for (var i in ignoreList) {
            var item = ignoreList[i];
            if (item.fnName === fnName && 0 === validateJso(json, item.json).length) {
                ignoreList.splice(i, 1);
            }
        }
    }

    function expect(fnName, json) {
        if (!fnName) {
            throw new Error("At least fnName should be defined");
        }
        expectations.push({
            fnName: fnName,
            json: json
        })
    }

    var NO_ERROR = [];

    function happenedImpl(fnName, json) {
        if (!enabled) {
            //console.log("Skipping expectly " + fnName);
            return NO_ERROR;
        }

        for (var i in ignoreList) {
            var ignoreItem = ignoreList[i];
            if (ignoreItem.fnName === fnName) {
                if (0 == validateJso(ignoreItem.json, json).length) {
//                    console.log("In ignore list " + fnName);
                    return NO_ERROR;
                }
            }
        }

        if (expectations.length == 0) {
            return [ErrorEntry("No expecations but smth happened: " + fnName + ", json: " + JSON.stringify(json))];
        }

        var expectation = expectations[0];
        expectations = expectations.slice(1, expectations.length);
        if (fnName !== expectation.fnName) {
            return [ErrorEntry("fnNames are different. expected: " + JSON.stringify(expectation.fnName) + ", actual: " + fnName)];
        }

        return validateJso(expectation.json, json);
    }

    function happened(fnName, json) {
        var errors = happenedImpl(fnName, json);
        if(!errors || 0 === errors.length) {
//            console.log("No errors for: " + fnName);
        }
        return errors;
    }

    function ErrorEntry(msg, path) {
        return {
            msg: msg,
            path: path || ""
        };
    }

    function superReport(errorEntries, name) {
        if (!errorEntries || !name) {
            return;
        }
        for (var i = errorEntries.length - 1; i >= 0; i--) {
            var entry = errorEntries[i];
            entry.path = name + "." + entry.path;
        }
    }

    function validateJso(expected, actual) {
        var errorReport = [];
        if (!expected) {
            return NO_ERROR; //anything is acceptable, even absence of parameters
        }

        if (typeof actual !== "object") {
            return [ErrorEntry("actual is not js object")]; // at least js object is expected
        }

        if (expected == {}) {
            return NO_ERROR;
        }

        for (var key in expected) {
            if (!expected.hasOwnProperty(key)) {
                errorReport.push(ErrorEntry("Error in expected json. property is not own", key));
                continue;
            }

            var expectedValue = expected[key];
            if (actual.hasOwnProperty(key)) {
                var actualValue = actual[key];

                if (typeof expectedValue === "object") {
                    var subReport = validateJso(expectedValue, actualValue);
                    superReport(subReport, key);
                    errorReport = errorReport.concat(subReport);
                } else {
                    if (expectedValue != actualValue) {
                        errorReport.push(ErrorEntry("Expected and actual value are different. Expected: " + expectedValue + ", actual: " + actualValue, key));
                    }
                }
            } else {
                errorReport.push(ErrorEntry("Actual should contain own property: ", key));
            }
        }
        return errorReport;
    }

    return {
        turn: function (on) {
            enabled = !!on;
        },
        enable: function () {
            this.turn(true);
        },
        expect: expect,
        happened: happened,
        ignore: ignore,
        unignore: unignore,
        finishTesting: finishTesting
    }

}());


