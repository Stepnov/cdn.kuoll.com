//var ignoreCss = [".kuoll-ignore"];
import reportError from "./reportError";
import * as utils from "./utils";
import * as recording from "../embedScript/embedFake";

//var ignoreCss = [".kuoll-ignore"];
var ignoreCss = []; // @dk #quick_fix_for_gantt_pro
var ignoreUrls = [];

var keyboardEventTypes = ["keypress", "keydown", "keyup"];

export function init() {
    if (typeof recording !== "undefined") {
        var recordingInfo = recording.getRecordingInfo();
        if (recordingInfo.ignoreRules) {
            var origin = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port : '');
            recordingInfo.ignoreRules.forEach(function (rule) {
                try {
                    if (!rule.domain || new RegExp(rule.domain).test(origin)) {
                        ignoreCss.push(rule.selector);
                    }
                } catch (e) {
                    reportError("Error while matching ignore rule domain: " + rule.domain, e);
                }
            });
        }
        if (recordingInfo.ignoreUrls) {
            var urlsString = recordingInfo.ignoreUrls || [];
            for (var i = 0; i < urlsString.length; ++i) {
                ignoreUrls[i] = new RegExp(urlsString[i]);
            }
        }
        
        const kuollUrlRegExp = 'https?://' + (recordingInfo.envSuffix === 'Dev' ? 'localhost:8080' : 'app.kuoll.com') + '/.*';
        ignoreUrls.push(new RegExp(kuollUrlRegExp));
    }
}

export function isNodeIgnored(node, withoutParents) {
    try {
        for (var i = 0; i < ignoreCss.length; ++i) {
            while (node) {
                if (typeof node.matches === "function" && node.matches(ignoreCss[i])) {
                    return true;
                }
                node = withoutParents ? null : node.parentElement;
            }
        }
        return false;
    } catch (e) {
        reportError("Error in isNodeIgnored ", e);
        return {rule: ignoreCss[i], target: utils.getPath(node)};
    }
}

export function isNodeEventIgnored(node, event) {
    // Returns true for keyboard events on "input[type=password]" nodes
    return node && node.tagName === "INPUT" &&
        typeof node.getAttribute === "function" && node.getAttribute("type") === "password" &&
            event && keyboardEventTypes.indexOf(event.type) !== -1
}

export function isUrlIgnored(url) {
    for (var i = 0; i < ignoreUrls.length; ++i) {
        if (ignoreUrls[i].test(url)) {
            return true;
        }
    }
    return false;
}

export function isCssIgnoringActive() {
    return ignoreCss.length > 0;
}
