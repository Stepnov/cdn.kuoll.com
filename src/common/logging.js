import { console } from "./origObjects";

let silentMode = false;

function logInternal(loggerFn, loggerObj, args) {
    if (silentMode) return;

    loggerFn.apply(loggerObj, args);
}

export function init(_silentMode) {
    silentMode = !!_silentMode;
}

export function error() {
    logInternal(console.error, console, arguments);
}
export function warn() {
    logInternal(console.warn, console, arguments);
}
export function info() {
    logInternal(console.info, console, arguments);
}
export function debug() {
    logInternal(console.debug, console, arguments);
}
export function log() {
    logInternal(console.log, console, arguments);
}