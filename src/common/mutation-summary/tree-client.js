import config from "config";
import { setTimeout } from "../origObjects";
import * as utils from "../utils";
import MutationSummary from "./ms";

export default function TreeMirrorClient(params) {
    var self = this;
    this.target = params.target;
    this.mirror = params.mirror;
    this.filterNodes = params.filterNodes;
    this.filterAttributes = params.filterAttributes;
    this.nextId = 1;
    this.knownNodes = new MutationSummary.NodeMap();
    this.nodesProperties = {};
    this.initialized = false;
    this.initialSerialize(params.target);
    var queries = [{ all: true }];
    if (params.testingQueries)
        queries = queries.concat(params.testingQueries);

    this.mutationSummary = new MutationSummary({
        rootNode: params.target,
        callback: function (summaries) {
            // TODO vlad: check if we can pend summaries until all tree is serialized
            self.applyChanged(summaries);
        },
        queries: queries
    });
}
TreeMirrorClient.prototype.initialSerialize = function (target) {
    function serializeNextNode(array, node, putIntoArray) {
        var serializedNode = self.serializeNode(node);
        if (serializedNode) {
            if (putIntoArray) {
                array.push(serializedNode);
            }

            var childNodes = node.childNodes;
            if (childNodes && childNodes.length) {
                var childArray;
                if (putIntoArray) {
                    serializedNode.childNodes = [];
                    childArray = serializedNode.childNodes;
                } else {
                    childArray = array;
                }
                nodesSerializingCounter += childNodes.length;
                for (var j = 0; j < childNodes.length; ++j) {
                    if (i++ >= 100) {
                        i = 0;
                        setTimeout(serializeNextNode, 100, childArray, childNodes[j], true);
                    } else {
                        serializeNextNode(childArray, childNodes[j], true);
                    }
                }
            }
        }

        --nodesSerializingCounter;
        if (nodesSerializingCounter === 0) {
            // all DOM is serialized
            self.initialized = true;
            var pendingSummary = self.pendingSummary;
            if (pendingSummary) {
                self.applyChanged([pendingSummary]);
            }
            self.mirror.initialize(rootId, children);
        }
    }
    var rootId = this.serializeNode(target).id;
    var children = [];
    var self = this;

    if (config.artifact === "extension") {
        for (var child = target.firstChild; child; child = child.nextSibling) {
            var serializeNode = this.serializeNode(child, true);
            if (serializeNode) {
                children.push(serializeNode);
            }
        }
        this.mirror.initialize(rootId, children);
        this.initialized = true;
    } else {
        var nodesSerializingCounter = 1;
        var i = 0;
        setTimeout(function () {
            if (self.mutationSummary) {
                serializeNextNode(children, target, false);
            }
        }, 500);
    }
};
TreeMirrorClient.prototype.disconnect = function () {
    if (this.mutationSummary) {
        this.mutationSummary.disconnect();
        this.mutationSummary = undefined;
    }
};
TreeMirrorClient.prototype.rememberNode = function (node) {
    var id = this.nextId++;
    this.knownNodes.set(node, id);
    return id;
};
TreeMirrorClient.prototype.forgetNode = function (node) {
    this.knownNodes.delete(node);
};
TreeMirrorClient.prototype.hasFilteredParent = function (node) {
    while (node.parentNode) {
        node = node.parentNode;
        if (node.__mutation_summary_filtered === false) {
            return true;
        }
    }
    return false;
};
TreeMirrorClient.prototype.serializeNode = function (node, recursive) {
    if (node === null)
        return null;
    var id = this.knownNodes.get(node);
    if (id !== undefined) {
        return { id: id };
    }
    var data = {};
    switch (node.nodeType) {
        case Node.DOCUMENT_TYPE_NODE:
            var docType = node;
            data.name = docType.name;
            data.publicId = docType.publicId;
            data.systemId = docType.systemId;
            break;
        case Node.COMMENT_NODE:
        case Node.TEXT_NODE:
            data.textContent = node.textContent;
            break;
        case Node.ELEMENT_NODE:
            var elm = node;
            data.attributes = {};

            if (this.filterNodes) {
                var filtering = this.filterNodes(elm);
                if (filtering === false) {
                    return null;
                } else if (typeof filtering === "object") {
                    data.attributes = filtering.attributes;
                } else if (filtering === true) {
                    // do nothing, keep node
                }
            }

            data.tagName = elm.tagName;
            for (var i = 0; i < elm.attributes.length; i++) {
                var attr = elm.attributes[i];
                if (this.filterAttributes) {
                    attr = this.filterAttributes(elm, attr);
                    if (!attr) continue;
                }
                if (typeof data.attributes[attr.name] == "undefined") {
                    data.attributes[attr.name] = attr.value;
                }
            }

            if (recursive) {
                data.childNodes = [];
                if (elm.childNodes.length) {
                    for (var child = elm.firstChild; child; child = child.nextSibling) {
                        var serializedNode = this.serializeNode(child, true);
                        if (serializedNode) {
                            data.childNodes.push(serializedNode);
                        }
                    }
                } else if (elm.content instanceof DocumentFragment) {
                    var serializedContent = this.serializeNode(elm.content, true);
                    if (serializedContent) {
                        data.childNodes.push(serializedContent);
                    }
                }
            }
            break;
        case Node.DOCUMENT_FRAGMENT_NODE:
            var elm = node;
            data.attributes = {};

            if (this.filterNodes) {
                var filtering = this.filterNodes(elm);
                if (filtering === false) {
                    return null;
                } else if (typeof filtering === "object") {
                    data.attributes = filtering.attributes;
                } else if (filtering === true) {
                    // do nothing, keep node
                }
            }

            data.tagName = elm.tagName;
            if (recursive && elm.childNodes.length) {
                data.childNodes = [];
                for (var child = elm.firstChild; child; child = child.nextSibling) {
                    var serializedNode = this.serializeNode(child, true);
                    if (serializedNode) {
                        data.childNodes.push(serializedNode);
                    }
                }
            }
            break;
    }

    data.nodeType = node.nodeType;
    data.id = this.rememberNode(node);

    return data;
};
TreeMirrorClient.prototype.serializeAddedAndMoved = function (added, reparented, reordered) {
    var _this = this;
    var all = added.concat(reparented).concat(reordered);
    var parentMap = new MutationSummary.NodeMap();
    all.forEach(function (node) {
        var parent = node.parentNode;
        if (parent) {
            var children = parentMap.get(parent);
            if (!children) {
                children = new MutationSummary.NodeMap();
                parentMap.set(parent, children);
            }
            children.set(node, true);
        }
    });
    var moved = [];
    parentMap.keys().forEach(function (parent) {
        var children = parentMap.get(parent);
        var keys = children.keys();
        while (keys.length) {
            var node = keys[0];

            while (node.previousSibling && children.has(node.previousSibling))
                node = node.previousSibling;
            while (node && children.has(node)) {
                if (node.parentNode && !_this.knownNodes.has(node.parentNode) && _this.hasFilteredParent(node)) {
                    children.delete(node);
                    node = node.nextSibling;
                    continue;
                }
                if (_this.filterNodes) {
                    var filtering = _this.filterNodes(node);
                    if (filtering === false) {
                        children.delete(node);
                        node = node.nextSibling;
                        continue;
                    }
                }
                var data = _this.serializeNode(node, true);
                if (data) {
                    var prevSibling = node, prevSiblingSerialized;
                    do {
                        prevSibling = prevSibling.previousSibling;
                        prevSiblingSerialized = _this.serializeNode(prevSibling);
                        data.previousSibling = prevSiblingSerialized;
                    } while (!prevSiblingSerialized && prevSibling != null && prevSibling.previousSibling != null);
                    data.parentNode = _this.serializeNode(node.parentNode);
                    data.parentPath = utils.getPath(node.parentNode);
                    moved.push(data);
                }
                children.delete(node);
                node = node.nextSibling;
            }
            keys = children.keys();
        }
    });
    return moved;
};
TreeMirrorClient.prototype.serializeAttributeChanges = function (attributeChanged) {
    var _this = this;
    var map = new MutationSummary.NodeMap();
    Object.keys(attributeChanged).forEach(function (attrName) {
        attributeChanged[attrName].forEach(function (element) {
            if (!_this.knownNodes.has(element)) return;

            var attr = element.getAttributeNode(attrName);
            if (!attr) return;

            if (_this.filterAttributes) {
                attr = _this.filterAttributes(element, attr);
                if (!attr) return;
            }

            var record = map.get(element);
            if (!record) {
                record = _this.serializeNode(element);
                if (record) {
                    record.attributes = {};
                    record.path = utils.getPath(element);
                    map.set(element, record);
                }
            }
            if (record) {
                record.attributes[attrName] = attr.value;
            }
        });
    });
    return map.keys().map(function (node) {
        return map.get(node);
    });
};
TreeMirrorClient.prototype.postponeSummaryApplying = function (summary) {
    var pendingSummary = this.pendingSummary;
    if (!pendingSummary) {
        this.pendingSummary = summary;
    } else {
        // merge current summary with all previous ones
        pendingSummary.added = pendingSummary.added.concat(summary.added);
        pendingSummary.reparented = pendingSummary.reparented.concat(summary.reparented);
        pendingSummary.reordered = pendingSummary.reordered.concat(summary.reordered);
        pendingSummary.removed = pendingSummary.removed.concat(summary.removed);
        pendingSummary.characterDataChanged = pendingSummary.characterDataChanged.concat(summary.characterDataChanged);

        // merge attribute changes
        var attributeChanged = summary.attributeChanged;
        if (pendingSummary.attributeChanged) {
            for (var attrName in attributeChanged) {
                if (attributeChanged.hasOwnProperty(attrName)) {
                    var attribute = attributeChanged[attrName];
                    var pendingAttributes = pendingSummary.attributeChanged[attrName];
                    if (pendingAttributes) {
                        for (var i = 0; i < attribute.length; ++i) {
                            if (pendingAttributes.indexOf(attribute[i]) === -1) {
                                pendingAttributes.push(attribute[i]);
                            }
                        }
                    } else {
                        pendingSummary.attributeChanged[attrName] = attribute;
                    }
                }
            }
        }
    }
};
TreeMirrorClient.prototype.applyChanged = function (summaries) {
    var summary = summaries[0];
    if (!this.initialized) {
        this.postponeSummaryApplying(summary);
        return;
    }

    var _this = this;
    var removed = [];
    for (var i = 0; i < summary.removed.length; ++i) {
        var data = _this.serializeNode(summary.removed[i]);
        if (data) {
            removed.push(data);
        }
    }
    var moved = this.serializeAddedAndMoved(summary.added, summary.reparented, summary.reordered);
    var attributes = this.serializeAttributeChanges(summary.attributeChanged || {});
    var text = summary.characterDataChanged.map(function (node) {
        var data = _this.serializeNode(node);
        data.textContent = node.textContent;
        return data;
    });

    var propertiesChanged = [];
    var textareas = document.getElementsByTagName("textarea");
    var inputs = document.querySelectorAll("input:not([type=password])");

    var recordPropertyChanges = function (node, data, propertyName) {
        if (_this.nodesProperties[data.id][propertyName] != node[propertyName]) {
            if (propertyName !== "value" || _this.nodesProperties[data.id][propertyName] !== undefined || node[propertyName] !== "") {
                data.properties[propertyName] = node[propertyName];
            }
            _this.nodesProperties[data.id][propertyName] = node[propertyName];
        }
    };
    var recordValueChanges = function (node) {
        var data = _this.serializeNode(node);
        if (data) {
            if (_this.nodesProperties[data.id] === undefined) {
                _this.nodesProperties[data.id] = {};
            }
            data.properties = {};
            recordPropertyChanges(node, data, "value");
            if (node.tagName === "INPUT") {
                recordPropertyChanges(node, data, "checked");
            }
            if (Object.keys(data.properties).length) {
                propertiesChanged.push(data);
            }
        }
    };
    Array.prototype.forEach.call(textareas, recordValueChanges);
    Array.prototype.forEach.call(inputs, recordValueChanges);

    this.mirror.applyChanged(removed, moved, attributes, text, propertiesChanged);
    summary.removed.forEach(function (node) {
        _this.forgetNode(node);
    });
};