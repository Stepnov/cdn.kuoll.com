import reportError from "./reportError";
import {cutAndCopy} from "./utils";
import config from "config";
import * as logging from "./logging";

import { XMLHttpRequest } from "../common/origObjects";

let xhr;
let getXhrs = [];

let queueEmptyListeners = [];

const postCommandQueue = (function () {
    var queue = [];
    var running = 0;

    function startNextTask() {
        if (queue.length > 0 && 0 === running) {
            running++;
            var command = queue.shift();
            doPostPrivate(command.url, command.postBody, function (response) {
                try {
                    if (typeof command.successCallback === "function") {
                        command.successCallback(response);
                    } else if (command.successCallback) {
                        reportError("Unknown type of successCallback " + typeof command.successCallback, e);
                    }
                } catch (e) {
                    reportError("Error calling successCallback" + command, e);
                } finally {
                    if (running !== 0) {
                        running--;
                        startNextTask();
                    } else {
                        fireQueueEmptyEvent();
                    }
                }
            });
        } else {
            fireQueueEmptyEvent();
        }
    }

    function run(url, postBody, successCallback) {
        queue.push({url: url, postBody: postBody, successCallback: successCallback});
        startNextTask();
    }

    function clear() {
        queue = [];
        running = 0;
    }

    function getAll() {
        return queue;
    }

    function isEmpty() {
        return running === 0;
    }

    return {
        run: run,
        clear: clear,
        getAll: getAll,
        isEmpty: isEmpty
    };
}());

function doPostPrivate(url, postBody, callback) {
    var fullUrl = config.server + url;

    xhr = new XMLHttpRequest(); // FIXME @vlad add comment why it is not method-local. Or fix it, if bug.
    xhr.open("POST", fullUrl, true);

    xhr.onabort = function () {

    };
    xhr.onerror = function () {
        reportError("XHR request failed", null, cutAndCopy(xhr));
        xhr = null;
    };
    xhr.addEventListener("loadend", function () {
        xhr = null;
        if (callback) {
            try {
                callback(this.response);
            } catch (e) {
                reportError("Error in API request callback", e);
            }
        } else {
            if (typeof this.response === "string") {
                try {
                    var resp = JSON.parse(this.response);
                    if (resp.errorMsg) {
                        reportError(resp.errorMsg);
                    } else {
                        if (config.envSuffix === "Dev") {
                            logging.debug("Ignored network response from " + url, postBody);
                        }
                    }
                } catch (e) {
                    if (config.envSuffix === "Dev") {
                        logging.debug("Unexpected server response from " + url, postBody, this.response);
                    } else {
                        reportError("Error with network", e)
                    }
                }
            }
        }
    });
    postBody = (typeof postBody === "string" ? postBody : JSON.stringify(postBody));
    xhr.setRequestHeader("Content-Type", "text/plain; charset=UTF-8");
    xhr.setRequestHeader("X-Kuoll-Build-Number", config.buildNumber);
    xhr.send(postBody);
}


export function doPost(url, postBody, successCallback) {
    postCommandQueue.run(url, postBody, successCallback);
    if (typeof expectly !== "undefined") {
        var errors = expectly.happened(url, postBody);
        if (errors && errors.length > 0) {
            throw new Error("doPost failed");
        }
    }
}

export function doGet(url, responseType, headers, onLoad, onError) {
    var xhr = new XMLHttpRequest();
    xhr.open("GET", url);
    getXhrs.push(xhr);

    if (typeof responseType !== "undefined" && responseType) {
        xhr.responseType = responseType;
    }

    if (typeof headers === "array" && headers.length !== 0) {
        for (var i = 0; i < headers.length; ++i) {
            var header = headers[i];
            xhr.setRequestHeader(header.name, header.value);
        }
    }

    xhr.onload = onLoad;
    xhr.onerror = onError;
    xhr.send();
}

function fireQueueEmptyEvent() {
    // Consider changing an API in such a way that only one listener is invoked at once.
    for (var i = 0; i < queueEmptyListeners.length; ++i) {
        if (queueEmptyListeners[i]) {
            (function (index) {
                queueEmptyListeners[index](function () {
                    delete queueEmptyListeners[index];
                });
            }(i));
        }
    }
}

export function addQueueEmptyListener(callback) {
    queueEmptyListeners.push(callback);
}

export function init() {
    xhr = null;
    getXhrs = [];
    queueEmptyListeners = [];
}

export function shutdown() {
    postCommandQueue.clear();
    if (xhr) {
        xhr.abort();
    }
    getXhrs.forEach(function (xhr) {
        xhr.abort();
    });
}

export function clearCommandQueue() {
    postCommandQueue.clear();
}

export function isQueueEmpty() {
    postCommandQueue.isEmpty();
}