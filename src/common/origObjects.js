function unwrap(obj) {
    let origFn = obj;
    while (origFn.__kuoll_orig != null) {
        origFn = origFn.__kuoll_orig;
    }
    return origFn;
}

export const console = {
    debug: unwrap(window.console.debug).bind(window.console),
    log: unwrap(window.console.log).bind(window.console),
    info: unwrap(window.console.info).bind(window.console),
    warn: unwrap(window.console.warn).bind(window.console),
    error: unwrap(window.console.error).bind(window.console)
};

export const XMLHttpRequest = unwrap(window.XMLHttpRequest);
export const setTimeout = unwrap(window.setTimeout);
export const clearTimeout = unwrap(window.clearTimeout);
export const setInterval = unwrap(window.setInterval);
export const clearInterval = unwrap(window.clearInterval);

export const Promise = unwrap(window.Promise);