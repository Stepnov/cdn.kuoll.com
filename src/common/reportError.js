import config from "config";
import {console, XMLHttpRequest} from "./origObjects";

let getRecordingInfo;

export default function reportError(message, cause, diagnostics) {
    try {

        var stack;

        if (cause && cause.stack) {
            stack = cause.stack;
        } else {
            stack = new Error().stack;
        }

        var userInfo = null;
        if (getRecordingInfo) {
            const recordingInfo = getRecordingInfo();
            if (recordingInfo) {
                userInfo = {
                    orgId: recordingInfo.kuollCustomerId,
                    userId: recordingInfo.userId,
                    frameNum: recordingInfo.frameNum,
                    recordCode: recordingInfo.recordCode,

                    startParams: recordingInfo.startParams,

                    localRecording: recordingInfo.localRecording,
                    autonomousMode: recordingInfo.autonomousMode
                };
            }
        }

        if ("Dev" === config.envSuffix) {
            console.warn(message, stack, diagnostics, userInfo);
        } else {
            var xhr = new XMLHttpRequest();
            var fullUrl = config.server + "reportKError";
            xhr.open("POST", fullUrl, true);

            xhr.onerror = function (e) {
                console.error("Can not report error to the server due to another error while reporting.", e, stack);
            };

            xhr.send(JSON.stringify({
                message: message,
                stack: stack,
                diagnostics: diagnostics,
                userInfo: userInfo,
                page: document.location.href,
                userAgent: window.navigator.userAgent,
                build: config.buildNumber
            }));
        }

    } catch (e) {
        try {
            if (console && console.error instanceof Function) {
                console.error.apply(console, ["Can not report error to the server due to another error while reporting.", e].concat(arguments));
            }
        } catch (e) {
            // Something is very broken, can't even report an error
        }
    }
}

export function init(getRecordingInfoCallback) {
    getRecordingInfo = getRecordingInfoCallback;
}