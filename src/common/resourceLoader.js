import * as network from "./network";
import * as recording from "../embedScript/embedFake";
import * as LocalRequestsStorage from "../embedScript/LocalRequestsStorage";
import * as saving from "./saving";
import reportError from "./reportError";
import SHA256 from "crypto-js/sha256";
import encUTF8 from "crypto-js/enc-utf8";
import config from "config";

// TODO vlad: such config should be extracted
let localRecording = false;

let loadedResources = {};

let currentFrameNum;

let resourceHashesToCheck = [];
const resourceHashesToSave = [];

function string2Bin(str) {
    var result = new Array(str.length);
    for (var i = 0; i < str.length; i++) {
        result[i] = str.charCodeAt(i);
    }
    return result;
}

function addResourceHash(hash, frameNum) {
    var recordingInfo = recording.getRecordingInfo();

    if (recordingInfo.active) {
        resourceHashesToSave.push({
            hash: hash,
            frameNum: frameNum
        });
        if (resourceHashesToSave.length > 100) {
            putResourceHashesIntoFrame();
        }
    }
}

function putResourceHashesIntoFrame() {
    if (!resourceHashesToSave.length) return;

    var recordingInfo = recording.getRecordingInfo();
    var hashes;
    var frameNum = 0;
    for (var i = 0; i < resourceHashesToSave.length; ++i) {
        if (!frameNum) {
            frameNum = resourceHashesToSave[i].frameNum;
        } else if (frameNum !== resourceHashesToSave[i].frameNum) {
            break;
        }
    }
    hashes = resourceHashesToSave.splice(0, i).map(function (entry) { return entry.hash; });

    network.doPost("putResourcesIntoFrame", {
        orgId: recordingInfo.orgId,
        userId: recordingInfo.userId,
        recordId: recordingInfo.recordId,
        frameNum: frameNum,
        hashes: hashes
    });
}

function makeFullUrl(url, origin, filePath) {
    if (!origin) return url;

    var fullUrl = url;
    if ("http://" === url.substring(0, 7) || "https://" === url.substring(0, 8) || "file:///" === url.substring(0, 8)) {
        return fullUrl;
    } else if ("//" === url.substring(0, 2)) {
        var a = document.createElement('a');
        a.href = origin;
        var protocol = a.protocol;
        return protocol + url;
    } else {
        if (filePath) {
            var a = document.createElement('a');
            a.href = filePath;
            var _origin = a.origin;
            if ("/" === url.substring(0, 1)) {
                if ("/" !== url.substring(0, 1) && "/" !== _origin.substring(_origin.length - 1)) {
                    fullUrl = _origin + "/" + url;
                } else {
                    fullUrl = _origin + url;
                }
            } else {
                return filePath.substring(0, filePath.lastIndexOf("/") + 1) + url;
            }
        } else {
            if ("/" !== url.substring(0, 1) && "/" !== origin.substring(origin.length - 1)) {
                fullUrl = origin + "/" + url;
            } else {
                fullUrl = origin + url;
            }
        }
    }
    return fullUrl;
}

export function loadResource(url, origin, path, resourceType, isInline, resourceContent) {
    var frameNum = recording.getRecordingInfo().frameNum;
    if (localRecording) {
        LocalRequestsStorage.loadResource(url, origin, path, resourceType, isInline, resourceContent, frameNum);
    } else {
        loadResourceNow(url, origin, path, resourceType, isInline, resourceContent, frameNum);
    }
}

export function loadInlineResource(content) {
    loadResource("", "", "", "style", true, content);
}

export function loadResourceNow(url, origin, path, resourceType, isInline, resourceContent, frameNum) {
    function loadCallback(hash, headers, response) {
        saving.saveResource({
            hash: hash,
            url: fullUrl,
            path: path,
            type: resourceType,
            isInline: !!isInline,
            headers: headers,
            content: response
        });
    }

    var fullUrl = makeFullUrl(url, origin, path);

    var recordingInfo = recording.getRecordingInfo();
    if (!currentFrameNum || currentFrameNum !== recordingInfo.frameNum) {
        loadedResources = {};
        currentFrameNum = recordingInfo.frameNum;
    }

    if (!fullUrl) {
        if (!resourceContent) {
            return;
        }
        var contentBytes = string2Bin(resourceContent);
        isResourceExists("", contentBytes, function (hash, exists) {
            if (!exists) {
                loadCallback(hash, "", contentBytes);
            }
            addResourceHash(hash, frameNum);
        });
        return;
    }

    if (loadedResources[fullUrl]) {
        return;
    }
    loadedResources[fullUrl] = true;

    var resourcesCachingInfo = recordingInfo.resourcesCachingInfo;
    var cachingInfo = resourcesCachingInfo[fullUrl];
    var isAlreadySaved = false;

    var headers = [];
    if (cachingInfo) {
        if (cachingInfo.expires && new Date(cachingInfo.expires) > new Date()) {
            addResourceHash(cachingInfo.hash, frameNum);
            return;
        }
        isAlreadySaved = true;
        if (cachingInfo.lastModified) {
            headers.push({
                name: "If-Modified-Since",
                value: cachingInfo.lastModified
            });
        }
        if (cachingInfo.eTag) {
            headers.push({
                name: "If-None-Match",
                value: cachingInfo.eTag
            });
        }
    }

    var onErrorCallback = function () {
        reportError("Could not load resource", null, fullUrl);
    };

    var onLoadCallback = function () {
        if (this.status !== 304 || !isAlreadySaved) {
            var responseArray = Array.prototype.slice.call(new Int8Array(this.response), 0);
            isResourceExists(fullUrl, responseArray, function (hash, exists) {
                if (!exists) {
                    loadCallback(hash, this.getAllResponseHeaders(), responseArray);
                }
                resourcesCachingInfo[fullUrl] = {
                    hash: hash,
                    lastModified: this.getResponseHeader("Last-Modified"),
                    expires: this.getResponseHeader("Expires")
                };

                if (config.artifact !== "embed") {
                    resourcesCachingInfo[fullUrl].eTag = this.getResponseHeader("ETag");
                }

                addResourceHash(hash, frameNum);
            }.bind(this));
        } else {
            var hash = cachingInfo.hash;
            addResourceHash(hash, frameNum);
        }
    };

    network.doGet(fullUrl, "arraybuffer", headers, onLoadCallback, onErrorCallback);
}

function isResourceExists(url, response, callback) {
    var hash = getHash(url, response);
    resourceHashesToCheck.push({
        hash: hash,
        callback: callback
    });
    if (resourceHashesToCheck.length > 100) {
        checkResourceExistence();
    }
}

function getHash(url, message) {
    var _message = url;
    if (typeof(message) === "string") {
        _message += message;
    } else {
        _message += encUTF8.parse(message);
    }
    return SHA256(_message).toString();
}

function checkResourceExistence() {
    var hashes = resourceHashesToCheck.map(function (entry) { return entry.hash });
    var callbacks = resourceHashesToCheck.map(function (entry) { return entry.callback });
    resourceHashesToCheck = [];

    network.doPost("areResourcesExist", {
        hashes: hashes
    }, function (data) {
        var response = JSON.parse(data);
        if (response.errorMsg) {
            reportError(response.errorMsg);
        } else {
            var existArray = response.existArray;
            for (var i = 0; i < existArray.length; ++i) {
                callbacks[i](hashes[i], existArray[i]);
            }
        }
    });
}

export function init() {
    loadedResources = {};
    resourceHashesToCheck = [];
    currentFrameNum = null;
    network.addQueueEmptyListener(function () {
        if (resourceHashesToCheck.length) {
            checkResourceExistence();
        }
        if (resourceHashesToSave.length) {
            putResourceHashesIntoFrame();
        }
    });
}

export function enableLocalRecording() {
    localRecording = true;
}
