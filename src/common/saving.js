import * as StacktraceRestoreService from "./StacktraceRestoreService";
import * as recording from "../embedScript/embedFake";
import * as SequentQueue from "./SequentQueue";
import * as network from "./network";
import * as logging from "./logging";
import config from "config";
import * as utils from "./utils";
import * as LocalRequestsStorage from "../embedScript/LocalRequestsStorage";
import * as domTools from "./domTools";

let localRecording = false;

function prepareApiRequest(reqBody) {
    var recordingInfo = recording.getRecordingInfo();

    if (recordingInfo.active) {
        reqBody.frameNum = recordingInfo.frameNum;
        reqBody.recordId = recordingInfo.recordId;
        reqBody.userId = recordingInfo.userId;
        reqBody.orgId = recordingInfo.orgId;
        return reqBody;
    } else {
        return false;
    }
}

export function saveSequent(sequent) {
    if (prepareApiRequest(sequent)) {
        sequent.stack = JSON.stringify(sequent.stack);
        sequent.clientTime = utils.currentTimestamp();
        sequent.frameTime = utils.highResolutionTimestamp();
        if (localRecording) {
            LocalRequestsStorage.saveSequent(sequent);
        } else {
            SequentQueue.add(sequent);
        }
    }
}

export function saveBaseSnapshot(baseSnapshot) {
    var postBody = prepareApiRequest({baseSnapshot: baseSnapshot});
    if (postBody) {
        if (localRecording) {
            LocalRequestsStorage.putBaseSnapshot(postBody);
        } else {
            network.doPost("putBaseSnapshot", postBody);
        }
    }
}

export function putRecordInfo(recordInfo) {
    var postBody = {recordInfo: recordInfo};
    if (prepareApiRequest(postBody)) {
        network.doPost("putRecordInfo", postBody);
    }
}

export function saveFrame(postBody) {
    if (prepareApiRequest(postBody)) {
        if (localRecording) {
            LocalRequestsStorage.saveFrame(postBody);
        } else {
            network.doPost("saveFrame", postBody);
        }
    }
}

export function saveResource(postBody, successCallback) {
    if (prepareApiRequest(postBody)) {
        network.doPost("saveResource", postBody, successCallback);
    }
}

function createOnIssueSavedCallback(stacktrace, hash) {
    return function onIssueSavedCallback(response) {
        try {
            var resp = JSON.parse(response);
        } catch (e) {
            console.error(response);
            return;
        }

        if (resp.errorCode === "reports_quota_exceeded") {
            logging.error("Your month quota of reports is exceeded. Recording is stopped. Please visit app.kuoll.com/billing.html for more details");
            window[config.globalName].stopRecord();
        }

        if (config.envSuffix === "Dev" || recording.getRecordingInfo().debugMode) {
            logging.debug("Issue created", "http://localhost:9000/play.html?recordCode=" +
                recording.getRecordingInfo().recordCode + "&issueId=" + resp.issueId);
        }

        StacktraceRestoreService.restoreStacktrace(stacktrace, hash);
    };
}

export function createIssue(postBody) {
    const recordingInfo = recording.getRecordingInfo();

    postBody.url = document.location.href;
    var stacktrace = postBody.stacktrace;
    postBody.stacktraceHash = utils.calculateStacktraceHash(stacktrace, document.location.href);
    postBody.stacktrace = JSON.stringify(stacktrace);
    postBody.clientTime = utils.currentTimestamp();
    postBody.frameTime = utils.highResolutionTimestamp();
    postBody.performance = domTools.collectPerformanceData();
    if (recordingInfo.active) {
        if (localRecording) {
            LocalRequestsStorage.persistRequestsToServer(postBody, createOnIssueSavedCallback(stacktrace, postBody.stacktraceHash));
        } else {
            network.doPost("createIssue", postBody, createOnIssueSavedCallback(stacktrace, postBody.stacktraceHash));
        }
    } else {
        network.doPost("createIssue", postBody, (response) => {
            let resp;
            try {
                resp = JSON.parse(response);
            } catch (e) {
                console.error(response);
                return;
            }

            if (resp.errorCode === "reports_quota_exceeded") {
                logging.error("Your month quota of reports is exceeded. Recording is stopped. Please visit app.kuoll.com/billing.html for more details");
            }

            if (config.envSuffix === "Dev" || recording.getRecordingInfo().debugMode) {
                logging.debug("Issue created", "http://localhost:9000/play.html?recordCode=" +
                    recording.getRecordingInfo().recordCode + "&issueId=" + resp.issueId);
            }
        });
    }
}

export function createIssueWithoutRecord(postBody) {
    postBody.url = document.location.href;
    const stacktrace = postBody.stacktrace;
    postBody.stacktraceHash = utils.calculateStacktraceHash(stacktrace, document.location.href);
    postBody.stacktrace = JSON.stringify(stacktrace);
    postBody.clientTime = utils.currentTimestamp();
    postBody.frameTime = utils.highResolutionTimestamp();
    postBody.performance = domTools.collectPerformanceData();
    network.doPost("createIssueWithoutRecord", postBody, (response) => {
        let resp;
        try {
            resp = JSON.parse(response);
        } catch (e) {
            console.error(response);
            return;
        }

        if (resp.errorCode === "reports_quota_exceeded") {
            logging.error("Your month quota of reports is exceeded. Recording is stopped. Please visit app.kuoll.com/billing.html for more details");
        }

        if (config.envSuffix === "Dev" || recording.getRecordingInfo().debugMode) {
            logging.debug("Issue created while recording is off", "http://localhost:9000/play.html?recordCode=" +
                recording.getRecordingInfo().recordCode + "&issueId=" + resp.issueId);
        }
    });
}

export function saveMetaData(metadata) {
    var postBody = {
        metaData: metadata
    };
    if (prepareApiRequest(postBody)) {
        network.doPost("saveRecordMetaData", postBody);
    }
}

export function enableLocalRecording() {
    localRecording = true;
}