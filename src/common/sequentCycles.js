import reportError from "./reportError";
var num2sequent = {};

function equals(o1, o2) {
    if(o1 && o2) {
        return o1.sequentType === o2.sequentType &&
            o1.sequentSubtype === o2.sequentSubtype &&
            o1.stack === o2.stack;
    } else {
        //should not happen
        return false;
    }
}

export function uncycleSequent(sequent, successCallback) {
    var orig = {
        sequentType: sequent.sequentType,
        sequentSubtype: sequent.sequentSubtype,
        previousSequentNum: sequent.previousSequentNum,
        stack: sequent.stack,
        cycle: false
    };

    var sequentsWithCycle = [orig]; // Temporary for better diagnostics
    var currentSequentNum = sequent.previousSequentNum;
    num2sequent[sequent.sequentNum] = orig;
    while (currentSequentNum) {
        var another = num2sequent[currentSequentNum];
        if (!another) {
            reportError("No sequent found by previousSequentNum", null, JSON.stringify(sequentsWithCycle));
            return;
        }

        sequentsWithCycle.push(another);

        if (equals(orig, another)) {
            orig.cycle = true;
            return;
        }
        if (currentSequentNum === another.previousSequentNum) {
            reportError("currentSequentNum == sequent.currentSequentNum", null, JSON.stringify(sequentsWithCycle));
            return;
        }

        currentSequentNum = another.previousSequentNum;
    }

    successCallback(sequent);
}

export function init() {
    num2sequent = {};
}