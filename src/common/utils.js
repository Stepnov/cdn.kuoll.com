import StackTrace from "stacktrace-js";
import BigNumber from "bignumber.js";
import SHA256 from "crypto-js/sha256";
import encUTF8 from "crypto-js/enc-utf8";
import * as ErrorStackParser from "error-stack-parser";
import normalizeErrorStack from "error-stack-normalizer";
import reportError from "./reportError";

let is_chrome = navigator.userAgent.indexOf('Chrome') > -1;
const is_explorer = navigator.userAgent.indexOf('MSIE') > -1;
const is_firefox = navigator.userAgent.indexOf('Firefox') > -1;
let is_safari = navigator.userAgent.indexOf("Safari") > -1;
const is_opera = navigator.userAgent.toLowerCase().indexOf("op") > -1;
if ((is_chrome) && (is_safari)) {                                                                                   
    is_safari = false;
}                                                                       
if ((is_chrome) && (is_opera)) {
    is_chrome = false;                                                                                                 
}

const PROHIBITED_PROPERTIES = ["caller", "callee", "arguments", "currentTarget", "fromElement",
    "toElement", "sourceCapabilities", "sequent"];

const NOT_DEFINED_DESCRIPTION_PATTERNS = [
    /'?([\w.\\$]*)'? is not defined/,
    /'?([\w.\\$]*)'? is undefined/,
    /Can't find variable: '?([\w.\\$]*)'?/
];
const CANNOT_READ_PROPERTY_DESCRIPTION_PATTERNS = [
    /Unable to get property '?([\w.\\$]*)'? of undefined or null reference/,
    /Cannot read property '?([\w.\\$]*)'? of undefined/,
    /Cannot read property '?([\w.\\$]*)'? of null/
];

function getIndexInParent(elt) {
    var index = 1;
    while (elt = elt.previousElementSibling) {
        index++;
    }
    return index;
}

function getPathInternal(elt) {
    // It's been shown during optimisation for GanttPro that building CSS path of elements is expensive,
    // so we made CSS paths cacheable. However, CSS path of the same element may change over time by
    // certain manipulations with element object. While it's rarely used, this still can break recording.
    // Since we don't have serious performance issues now, caching is turned off to improve reliability.

    // if (elt && elt.__kuoll_element_path) {
    //     return elt.__kuoll_element_path;
    // }

    if (!elt) {
        return "<path-is-undefined-for-" + elt + ">";
    } else if ("HTML" === elt.tagName) {
        return "html";
    } else if ("BODY" === elt.tagName) {
        return "body";
    } else if ("HEAD" === elt.tagName) {
        return "head";
    } else if (window === elt) {
        return "window";
    } else if (document === elt) {
        return "document";
    }

    var tagName = elt.tagName;

    if (typeof tagName === "undefined" || tagName === "") {
        if ("#text" === elt.nodeName) {
            tagName = "";
        } else if ("#document-fragment" === elt.nodeName) {
            tagName = "";
        } else {
            reportError("Unknown tag name: " + elt.nodeName);
            return "<path-is-undefined>";
        }
    }

    var path;
    if (elt.id) {
        var firstChar = elt.id.charAt(0);
        if (firstChar >= 0 && firstChar <= 9) {
            // ID selector works incorrectly if it starts with a number; need to escape first char in this case
            path = "#\\\\3" + firstChar + " " + elt.id.substring(1);
            elt.__kuoll_element_path = path;
            return path;
        } else {
            path = "#" + elt.id;
            elt.__kuoll_element_path = path;
            return path;
        }
    } else {
        if (elt.parentNode === null) {
            //shadow dom?
            //path = elt.host ? getPathInternal(elt.host) + "::shadow>" + tagName : elt.nodeName + [""].concat(Array.prototype.slice.call(elt.classList)).join(".");
            //reportError("elt.parentNode == null. elt.nodeName=" + elt.nodeName);
            elt.__kuoll_element_path = null;
            return null;
        } else {
            var index = getIndexInParent(elt);
            path = index ? (getPathInternal(elt.parentNode) + ">" + tagName + ":nth-child(" + index + ")")
                : getPathInternal(elt.parentNode) + ">" + tagName;
        }

        //// TODO DEV_MODE: check if path was built correctly. For debug purposes only
        //if (document.querySelector(path) !== elt) {
        //    window.console_orig.error("getPathInternal works incorrectly");
        //}

        elt.__kuoll_element_path = path;
        return path;
    }
}

function filterKuollOwnStackFrames(frame) {
    if (!frame.fileName) {
        return true;
    }

    const kuollCdnDomains = ['cdn.kuoll.com', 'localhost:9001'];
    for (let i = 0; i < kuollCdnDomains.length; ++i) {
        const suffix = kuollCdnDomains[i];
        if (frame.fileName.indexOf(suffix) === -1) {
            return true;
        }
    }
    return false;
}

function pad(str, size) {
    while (str.length < size) str = "0" + str;
    return str;
}

/**
     * Some sites override Date.now() method, so it returns Date object instead of timestamp.
     * It may break sequents saving. So we need an independent way to get current timestamp.
     */
export function currentTimestamp() {
    var date = Date.now();
    if (typeof date === "number") {
        return date;
    } else if (date instanceof Date) {
        return date.getTime();
    } else {
        reportError("Date.now() returns unexpected result", null, date);
    }
}

export function highResolutionTimestamp() {
    if (typeof performance.now === "function") {
        return performance.now();
    } else {
        return -1;
    }
}

/*
 * http://stackoverflow.com/questions/728360/most-elegant-way-to-clone-a-javascript-object?page=1&tab=votes
 * @param obj
 * @returns {*}
 */
export function cutAndCopy(obj, /*int?*/level) {
    if (null === obj || "object" !== typeof obj) {
        return obj;
    }
    level = (level || 0) + 1;
    if (level >= 5) {
        return "<depthLimitReached>";
    }

    if (obj instanceof Date) {
        var copy = new Date();
        copy.setTime(obj.getTime());
        return copy;
    }

    if (obj instanceof Array) {
        copy = [];
        for (var i = 0, len = obj.length; i < len; i++) {
            copy[i] = cutAndCopy(obj[i], level);
        }
        return copy;
    }

    if (obj instanceof Object) {
        copy = {};
        if (obj instanceof HTMLElement || obj instanceof SVGSVGElement) {
            var attrArray = [];
            var targetAttributes = obj.attributes;
            for (var j = 0; j < targetAttributes.length; ++j) {
                var attribute = targetAttributes[j];
                attrArray.push({
                    "name": attribute.name,
                    "value": attribute.value                           
                });
            }
            return {
                HTMLElementPath: getPath(obj),
                attributes: attrArray,
                tagName: obj.tagName,                           
                innerText: obj.innerText,
                currentValue: obj.value
            };
        } else if (obj instanceof Document) {
            return {HTMLElementPath: "document"};
        } else if (obj === window) {
            return {HTMLElementPath: "window"};
        } else if (obj instanceof CSSStyleDeclaration) {
            return {CSSStyleDeclaration: "CSSStyleDeclaration"};
        } else if (obj instanceof CSSStyleRule) {
            return {CSSStyleRule: "CSSStyleRule"};
        } else if (obj instanceof CSSStyleSheet) {
            return {CSSStyleSheet: "CSSStyleSheet"};
        } else if (obj instanceof HTMLCollection) {
            return {HTMLCollection: "HTMLCollection"};
        } else if (obj instanceof Attr) {
            return {name: obj.name, value: obj.value};
        } else if (obj.nodeName === "#text") {
            var isAttrValue = (getConstructorName(obj) === "Text" && obj.parentNode && getConstructorName(obj.parentNode) === "Attr");
            if (isAttrValue) {
                return {
                    HTMLElementPath: getPath(obj.parentNode.ownerNode)
                }
            } else {
                return obj.textContent
            }
        } else {
            var attr, value;

            if (is_safari) {
                // walk through the properties differently in Safari
                var currentProps = Object.getOwnPropertyNames(obj);
                for (j = currentProps.length - 1; j >= 0; j--) {
                    attr = currentProps[j];
                    if (PROHIBITED_PROPERTIES.indexOf(attr) !== -1) continue;

                    value = obj[attr];
                    if ("undefined" !== typeof value && "function" !== typeof value) {
                        copy[attr] = cutAndCopy(value, level);
                    }
                }
            } else {
                var prot = obj;
                while (prot !== Object.prototype && prot !== undefined) {
                    currentProps = Object.getOwnPropertyNames(prot);
                    for (j = currentProps.length - 1; j >= 0; j--) {
                        attr = currentProps[j];
                        if (PROHIBITED_PROPERTIES.indexOf(attr) !== -1) continue;

                        try {
                            value = obj[attr];
                        } catch (e) {
                            // denied access to property in IE. Just skip this property for now
                        }
                        if ("undefined" !== typeof value && "function" !== typeof value) {
                            copy[attr] = cutAndCopy(value, level);
                        }
                    }
                    prot = Object.getPrototypeOf(prot);
                }
            }
            return copy;
        }
    }

    if ((typeof obj == "object") && !(obj instanceof Object)) {
        return {"path": "Another_frame_window"};
    }

    return "<can-not-copy-object>";
}

export function getPath(elt) {
    try {
        return getPathInternal(elt);
    } catch (e) {
        return null;
    }
}

export function getConstructor(object) {
    try {
        var proto = Object.getPrototypeOf(object);
        if (proto !== undefined && proto.constructor !== undefined) {
            return proto.constructor;
        } else if (object.constructor !== undefined) {
            return object.constructor;
        }
    } catch (e) {
        reportError("Was not able to get constructor name for object", e);
    }
    return null;
}

export function getConstructorName(object) {
    var constructor = getConstructor(object);
    if (constructor) {
        if (constructor.name !== undefined) {
            return constructor.name;
        } else {
            return object.toString().split("object ")[1].split("]")[0];
        }
    } else {
        return "<undefined-constructor>";
    }
}

var pageXOffset, pageYOffset;

export function getPageXOffset() {
    return pageXOffset;
}

export function getPageYOffset() {
    return pageYOffset;
}

export function getStacktrace() {
    const stacktrace = StackTrace.getSync({
        filter: filterKuollOwnStackFrames
    });
    return normalizeErrorStack(stacktrace);
}

export function parseStacktrace(error) {
    if (!error) {
        return null;
    }

    const stackFrames = ErrorStackParser.parse(error).filter(filterKuollOwnStackFrames);
    return normalizeErrorStack(stackFrames);
}

export function normalizeErrorDescription(description) {
    if (!description) return description;
    
    description = description.trim();

    for (let i = 0; i < NOT_DEFINED_DESCRIPTION_PATTERNS.length; ++i) {
        const pattern = NOT_DEFINED_DESCRIPTION_PATTERNS[i];
        const groups = pattern.exec(description);
        if (groups) {
            return `'${groups[1]}' is not defined`;
        }
    }

    for (let i = 0; i < CANNOT_READ_PROPERTY_DESCRIPTION_PATTERNS.length; ++i) {
        const pattern = CANNOT_READ_PROPERTY_DESCRIPTION_PATTERNS[i];
        const groups = pattern.exec(description);
        if (groups) {
            return `Unable to get property '${groups[1]}' of undefined or null reference`;
        }
    }
    
    return description;
}

export function calculateStacktraceHash(stacktrace, url) {
    if (!stacktrace) return null;
    
    var modulo = new BigNumber(2).pow(256);
    var hash = new BigNumber(0);
    for (var i = 0; i < stacktrace.length; ++i) {
        var stackFrame = stacktrace[i];
        var fileName = stackFrame.fileName, uriHash = new BigNumber(0);
        if (fileName) {
            fileName = fileName.split("[?#]")[0];

            if (url) {
                var urlPageName = url.substr(url.lastIndexOf("/") + 1);
                fileName = fileName.replace(urlPageName, "");
            }

            uriHash = new BigNumber(SHA256(encUTF8.parse(fileName)).toString(), 16);
        }

        var fnName = stackFrame.functionName, fnNameHash = 0;
        if (fnName) {
            var dotPosition = fnName.lastIndexOf(".");
            if (dotPosition !== -1) {
                fnName = fnName.substr(dotPosition + 1);
            }
            if (fnName === "<anonymous>") {
                fnName = "anonymous";
            }

            fnNameHash = new BigNumber(SHA256(encUTF8.parse(fnName)).toString(), 16);
        }

        // 37 is an arbitrate prime required for better hash uniformity
        hash = hash.times(37).plus(uriHash)
                .times(37).plus(fnNameHash)
                .mod(modulo);
    }


    return pad(hash.toString(16), 64);
}

export function init() {
    var recalculatePageOffset = function () {
        pageXOffset = window.pageXOffset;
        pageYOffset = window.pageYOffset;
    };
    window.addEventListener("scroll", recalculatePageOffset);
    recalculatePageOffset();
}