export default {
    server: "http://localhost:8080/api/",
    resourcesServer: "http://localhost:9001/",
    envSuffix: "Dev",
    globalName: "kuoll",
    artifact: "embed",
    buildNumber: "[AIV]{version}[/AIV]"
};