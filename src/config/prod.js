export default {
    server: "https://api.kuoll.com/api/",
    resourcesServer: "https://cdn.kuoll.com/",
    envSuffix: "Prod",
    globalName: "kuoll",
    artifact: "embed",
    buildNumber: "[AIV]{version}[/AIV]"
};