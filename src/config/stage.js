export default {
    server: "https://stage.kuoll.com/api/",
    resourcesServer: "https://cdn.stage.kuoll.com/",
    envSuffix: "Prod",
    globalName: "kuoll",
    artifact: "embed",
    buildNumber: "[AIV]{version}[/AIV]"
};