import config from "config";
import * as utils from "../common/utils";
import {XMLHttpRequest, setTimeout, console} from "../common/origObjects";
import reportError from "../common/reportError";
import * as logging from "../common/logging";
import * as ExceptionDetection from "../common/ExceptionDetection";
import {getCookie, CookieCrossOriginStorage} from "./CookieCrossOriginStorage";
import * as StacktraceRestoreService from "../common/StacktraceRestoreService";

function gaActionToKuollStep(action) {
    return action === "detail" ? "Visit" :
        action === "add" ? "AddToCart" :
        action === "checkout" ? "Checkout" :
            action === "purchase" ? "Purchase" : null;
}

const cookieStorage = new CookieCrossOriginStorage(document.location.hostname);

let presetTrafficSourceName = null;
let presetLoggedInFlag = null;

const xhrHooks = [];
const pageHooks = [];
const clickHooks = [];
const formHooks = [];

const tracker = (function () {

    function cleanSession() {
        cookieStorage.del("kuollRecordId");
        cookieStorage.del("alreadyPurchased");
        cookieStorage.del("firstVisit");
        cookieStorage.del("prevEcomAction");
        cookieStorage.del("prevErrors");
        cookieStorage.del("kuollTrafficSourceName");
        cookieStorage.del("kuollLoggedIn");
        cookieStorage.del("kuollSessionDataSent");
        cookieStorage.put("kuollReportedErrorHashes", "{}");
        cookieStorage.put("kuollReportedEcomSteps", INITIAL_ECOM_STEPS_STR);
        cookieStorage.del("kuollReportedXhrErrorUrls");
        cookieStorage.del("prevXhrErrors");
    }

    let started = false;

    let startDateTime = Date.now();
    const INITIAL_ECOM_STEPS_STR = JSON.stringify({
        Visit: 0,
        AddToCart: 0,
        Checkout: 0,
        Purchase: 0        
    });

    let ecomSteps = [
        "Visit",
        "AddToCart",
        "Checkout",
        "Purchase"
    ]; 

    if (!cookieStorage.has("kuollReportedEcomSteps")) {
        cookieStorage.put("kuollReportedEcomSteps", INITIAL_ECOM_STEPS_STR);
    }
    if (!cookieStorage.has("kuollReportedErrorHashes")) {
        cookieStorage.put("kuollReportedErrorHashes", JSON.stringify({}));
    }

    function getPreviousSteps(step) {
        return ecomSteps.slice(0, ecomSteps.indexOf(step));
    }

    function markRecordedStep(step) {
        let reportedSteps = JSON.parse(cookieStorage.get("kuollReportedEcomSteps"));
        if (reportedSteps[step]) {
            return true;
        } else {
            reportedSteps[step] = 1;
            cookieStorage.put("kuollReportedEcomSteps", JSON.stringify(reportedSteps));
            return false;
        }
    }

    function markRecordedError(errorHash) {
        let reportedErrors = JSON.parse(cookieStorage.get("kuollReportedErrorHashes"));
        let key = (cookieStorage.has("prevEcomAction") ? cookieStorage.get("prevEcomAction") : "Visit") + "." + errorHash;
        if (reportedErrors[key]) {
            return true;
        } else {
            reportedErrors[key] = 1;
            cookieStorage.put("kuollReportedErrorHashes", JSON.stringify(reportedErrors));
            return false;
        }
    }

    function markRecordedXhrError(url, status, hash) {
        let reportedXhrErrorUrls = JSON.parse(cookieStorage.get("kuollReportedXhrErrorUrls", "{}"));
        let key = (cookieStorage.has("prevEcomAction") ? cookieStorage.get("prevEcomAction") : "Visit") + "." + url + "." + status + "." + hash;
        if (reportedXhrErrorUrls[key]) {
            return true;
        } else {
            reportedXhrErrorUrls[key] = 1;
            cookieStorage.put("kuollReportedXhrErrorUrls", JSON.stringify(reportedXhrErrorUrls));
            return false;
        }
    }

    function getEcomStep() {
        return cookieStorage.has("prevEcomAction") ? cookieStorage.get("prevEcomAction"): ecomSteps[0];
    }

    function getNextEcomStep() {
        if(cookieStorage.has("prevEcomAction")) {
            return ecomSteps[1 + ecomSteps.indexOf(cookieStorage.get("prevEcomAction"))];
        } else {
            return ecomSteps[1];
        }
    }


    // @dk: Is there a reason for using UUID instead of plain integer?
    function uuidv4() {
        return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, function (c) {
            return (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16);
        })
    }

    const timeMks = (performance && performance.now) ? performance.now.bind(performance) : function () {
        return Date.now() - startDateTime;
    };

    return {
        start: function () {
            started = true;
            if (cookieStorage) {
                if (!cookieStorage.has("kuollRecordId")) {
                    cookieStorage.put("kuollRecordId", uuidv4());
                }
            }
            startDateTime = Date.now();

            if (presetTrafficSourceName) {
                setTrafficSourceNameInternally(presetTrafficSourceName);
                presetTrafficSourceName = null;
            }

            let loggedIn;
            if (!presetLoggedInFlag) {
                const sessionIdCookie = getCookie("modnakasta_sessionid");
                loggedIn = !!sessionIdCookie;
            } else {
                loggedIn = presetLoggedInFlag;
            }
            cookieStorage.put("kuollLoggedIn", loggedIn);
    
            return this;
        },
        send: function (e, url) {
            if ("kuollRecordingInfo" in sessionStorage) {
                const recording = JSON.parse(sessionStorage.kuollRecordingInfo);
                if (recording.active) {
                    e.recordCode = recording.recordCode;
                }
            }

            if (!cookieStorage.has("kuollTrafficSourceName")) {
                const defaultTrafficSourceName = document.location.hostname;
                cookieStorage.put("kuollTrafficSourceName", defaultTrafficSourceName);
                e.trafficSourceName = defaultTrafficSourceName;
            }

            e.loggedIn = cookieStorage.get("kuollLoggedIn");

            const data = JSON.stringify(e);

            let sent = false;
            try {
                sent = typeof navigator.sendBeacon === "function" && navigator.sendBeacon(url, data);
            } catch (e) {
                // sendBeacon doesn't work, fallback to using regular XHR
            }

            if (!sent) {
                let xhr = new XMLHttpRequest();
                xhr.open("POST", url, true);

                xhr.onabort = function () {
                };
                xhr.setRequestHeader("Content-Type", "application/json; charset=UTF-8");
                xhr.send(data);
            }

            cookieStorage.put("kuollSessionDataSent", "1");
        },

        error: function (errorEvent, error) {
            if (error.__kuoll_convmon_processed) return;
            error.__kuoll_convmon_processed = true;

            if (cookieStorage.has("alreadyPurchased")) {
                return;
            }

            if (markRecordedError(errorEvent.hash)) {
                return;
            }

            cookieStorage.append("prevErrors", ",", "\"" + errorEvent.hash + "\"");

            errorEvent.message = utils.normalizeErrorDescription(errorEvent.message);

            errorEvent.apiKey = window.kuollApiKey;
            errorEvent.domain = Shopify.shop;
            errorEvent.trafficSourceName = cookieStorage.get("kuollTrafficSourceName");

            const nextEcomStep = getNextEcomStep();
            if (!nextEcomStep) {
                return; // errors that occur after last ecom event aren't important
            }

            errorEvent.beforeEcomEventType = nextEcomStep;
            this.send(errorEvent, config.server + "../convmon/error");
        },
            
        ecommerce: function (ecomEvent) {
            if (!window.kuollApiKey && !Shopify.shop) return;

            cookieStorage.put("prevEcomAction", ecomEvent.convertedToStep);

            if (cookieStorage.has("alreadyPurchased")) {
                return;
            }
            
            ecomEvent.trafficSourceName = cookieStorage.get("kuollTrafficSourceName");

            const prevSteps = getPreviousSteps(ecomEvent.convertedToStep);
            prevSteps.filter(step => !markRecordedStep(step)).forEach(step => {
                this.send({
                    apiKey: window.kuollApiKey,
                    domain: Shopify.shop,
                    hashes: [],
                    xhrErrors: [],
                    trafficSourceName: ecomEvent.trafficSourceName,
                    convertedToStep: step
                }, config.server + "../convmon/ecommerce");
            });
            
            if (markRecordedStep(ecomEvent.convertedToStep)) {
                return;
            }
            ecomEvent.apiKey = window.kuollApiKey;
            ecomEvent.domain = Shopify.shop;
            ecomEvent.hashes = cookieStorage.has("prevErrors") ? JSON.parse("[" + cookieStorage.get("prevErrors") + "]"): [];
            ecomEvent.xhrErrors = JSON.parse(cookieStorage.get("prevXhrErrors", "[]"));

            this.send(ecomEvent, config.server + "../convmon/ecommerce");
            cookieStorage.del("prevErrors");
            cookieStorage.del("prevXhrErrors");
            cookieStorage.put("kuollReportedErrorHashes", "{}");

            window.dispatchEvent(new CustomEvent("kuollEcommerceEvent", {
                "detail": ecomEvent
            }));
            if ("Purchase" === ecomEvent.convertedToStep) {
                cookieStorage.put("alreadyPurchased", 1);
            }
        },

        xhrError: function (xhr) {
            if (xhr.responseURL.indexOf('https://api.kuoll.com') !== -1) {
                return;
            }

            if (cookieStorage.has("alreadyPurchased")) {
                return;
            }

            const url = xhr.responseURL.indexOf('?') === -1 ? xhr.responseURL : xhr.responseURL.substring(0, xhr.responseURL.indexOf('?'));

            let stacktrace = xhr.__kuoll_stacktrace;
            if (!stacktrace) {
                stacktrace = utils.getStacktrace();
            }
            const hash = utils.calculateStacktraceHash(stacktrace, document.location.href);

            if (markRecordedXhrError(url, xhr.status, hash)) {
                return;
            }

            const prevXhrErrors = JSON.parse(cookieStorage.get("prevXhrErrors", "[]"));
            prevXhrErrors.push({
                url: url,
                status: xhr.status,
                hash: hash
            });
            cookieStorage.put("prevXhrErrors", JSON.stringify(prevXhrErrors));

            const errorEvent = {};
            errorEvent.apiKey = window.kuollApiKey;
            errorEvent.domain = Shopify.shop;
            errorEvent.trafficSourceName = cookieStorage.get("kuollTrafficSourceName");
            errorEvent.url = url;
            errorEvent.status = xhr.status;
            errorEvent.hash = hash;

            const nextEcomStep = getNextEcomStep();
            if (!nextEcomStep) {
                return; // errors that occur after last ecom event aren't important
            }

            errorEvent.beforeEcomEventType = nextEcomStep;
            this.send(errorEvent, config.server + "../convmon/xhr");
        },

        checkFirstVisit: function() {
            tracker.ecommerce({convertedToStep: "Visit"});
            if(cookieStorage.has("prevEcomAction")) {
                cookieStorage.put("firstVisit", 1);
                return;
            }
            if (!cookieStorage.has("firstVisit") && (window.kuollApiKey || Shopify.shop)) {
                cookieStorage.put("firstVisit", 1);
            }
        },

        cleanSession: cleanSession,
        isStarted: function () {
            return started;
        }
    }
}());
// end of 'x'


function serializeDataAttribute(data) {
    try {
        if (typeof data === "string" || data instanceof String) {
            return data;
        } else if (data === null || typeof data === "undefined") {
            return null;
        } else {
            return JSON.stringify(data);
        }
    } catch (e) {
        return null;
    }
}

function proxy(objName, fname, cb) {
    let obj;
    if (objName) {
        obj = window[objName];
    } else {
        obj = window;
    }
    const oldImpl = obj[fname];

    if (oldImpl.kuollProxied) {
        throw new Error('Function already proxied: ' + fname);
    }

    const proxy = function () {
        const args = arguments;
        let retVal;

        try {
            retVal = oldImpl.apply(obj, arguments);
        } catch (e) {
            const errorStack = utils.parseStacktrace(e);
            const errorStacktraceHash = utils.calculateStacktraceHash(errorStack, document.location.href);
            if (errorStacktraceHash) {
                tracker.error({
                    hash: errorStacktraceHash,
                    message: e ? e.message: null
                }, e);
            }

            throw e;
        }

        cb(args, retVal);

        return retVal;
    };

    proxy.kuollProxied = oldImpl;

    obj[fname] = proxy;
}

function proxyWhenReady(objName, fname, readyPropName, initCb, cb) {
    let obj;
    if (objName) {
        obj = window[objName];
    } else {
        obj = window;
    }

    if (obj && typeof(obj[fname]) === "function" && (!readyPropName || obj[fname][readyPropName])) {
        if (typeof initCb === "function") {
            initCb();
        }

        proxy(objName, fname, cb);
    } else {
        setTimeout(function () {
            proxyWhenReady(objName, fname, readyPropName, initCb, cb);
        }, 50);
    }
}

function proxyGtmDataLayer() {
    proxyWhenReady("dataLayer", "push", null, null, function (args, retVal) {
        const data = args[0];
        if (data && data.ecommerce && !data.eventNonInteraction) {
            const ecommerce = data.ecommerce;
            const hint =
                data.eventAction ||
                (!!ecommerce.purchase ? "purchase" : false) ||
                (!!ecommerce.refund ? "refund" : false) ||
                (!!ecommerce.add ? "add" : false) ||
                (!!ecommerce.remove ? "remove" : false) ||
                (!!ecommerce.detail ? "detail" : false) ||
                (!!ecommerce.checkout ? "checkout" : false) ||
                (!!ecommerce.checkout_option ? "checkout_option" : false) ||
                (!!ecommerce.promo_click ? "promo_click" : false) ||
                (!!ecommerce.click ? "click" : false);

            const convertedToStep = gaActionToKuollStep(hint);
            if (convertedToStep) {
                tracker.ecommerce({
                    convertedToStep: convertedToStep
                });
            }
        }
    });
}

function queryEcommerceHooks() {
    return new Promise((resolve, reject) => {
        const apiKey = window.kuollApiKey;
        const domain = Shopify.shop;
        const xhr = new XMLHttpRequest();
        xhr.open("GET", config.server + "../convmon/hooks?apiKey=" + apiKey + "&domain=" + domain);
        xhr.addEventListener("loadend", function () {
            try {
                const resp = JSON.parse(xhr.response);
                resolve(resp);
            } catch (e) {
                try {
                    const resp = JSON.parse(xhr.response.replace(/\\'/g, "'"));
                    resolve(resp);    
                } catch(e) {
                    reportError("Error while parsing FIXED response from GET /convmon/hooks", e, {apiKey: apiKey});
                    reject();
                }
                reportError("Error while parsing response from GET /convmon/hooks", e, {apiKey: apiKey});
                reject();
            }
        });
        xhr.send();
    })
}

function normalizeUrl(url) {
    var link = document.createElement("a");
    link.href = url;
    return link.href;
}

function pathsEqual(path1, path2) {
    return normalizeUrl(path1) === normalizeUrl(path2);
}

function setupHooksOnPageVisit(hooks) {
    function onNavigation() {
        const absUrl = normalizeUrl(document.location.href);
        hooks.forEach((rule) => {
            if (new RegExp(rule.filter).test(absUrl)) {
                tracker.ecommerce({
                    convertedToStep: rule.userStep
                });
            }
        });
    }

    const pushState_orig = history.pushState;
    history.pushState = function () {
        pushState_orig.apply(history, arguments);
        onNavigation();
    };
    const replaceState_orig = history.pushState;
    history.replaceState = function () {
        replaceState_orig.apply(history, arguments);
        onNavigation();
    };

    onNavigation();
}

function setupHookOnClick(path, step) {
    try {
        const nodes = document.querySelectorAll(path);
        nodes.forEach(node => {
            node.addEventListener("click", function () {
                tracker.ecommerce({convertedToStep: step});
            });
            node.__kuoll_hook_proxy = true;
        });
    } catch (e) {
        reportError("Error setting up a click hook", e, {path: path, step: step, apiKey: window.kuollApiKey});
    }
}

function setupHookOnFormSubmit(path, step) {
    try {
        const nodes = document.querySelectorAll(path);
        nodes.forEach(form => {
            const origSubmit = form.submit;
            if (origSubmit) {
                form.submit = function () {
                    try {
                        tracker.ecommerce({convertedToStep: step});
                    } catch (e) {
                        reportError("Error while recording ecommerce Purchase step on purchase form submit", e, {path: path});
                    } finally {
                        origSubmit.apply(form);
                    }
                };
                form.submit.__kuoll_hook_proxy = true;
            }
        });
    } catch (e) {
        reportError("Error setting up a click hook", e, {path: path, step: step, apiKey: window.kuollApiKey});
    }
}

export function init(configuration) {
    window.kuoll.ExceptionDetection = ExceptionDetection;
    setTimeout(init1, 0, configuration);
}
    
export function init1(configuration) {
    if (!window.kuollApiKey && !Shopify.shop) {
        console.warn("window.kuollApiKey or Shopify.shop must be provided for Kuoll ConvMon to work");
        return;
    }

    tracker.start();
    tracker.checkFirstVisit();

    queryEcommerceHooks()
        .then(hooks => {
            hooks.forEach(hook => {

                if (hook.ruleType === 'page') {
                    pageHooks.push(hook);

                } else if (hook.ruleType === 'click') {
                    clickHooks.push(hook);
                    setupHookOnClick(hook.filter, hook.userStep);
                    window.addEventListener("load", () => setupHookOnClick(hook.filter, hook.userStep));

                } else if (hook.ruleType === 'xhr') {
                    xhrHooks.push(hook);

                } else if (hook.ruleType === 'form') {
                    formHooks.push(hook);
                    setupHookOnFormSubmit(hook.filter, hook.userStep);
                    window.addEventListener("load", () => setupHookOnFormSubmit(hook.filter, hook.userStep));

                } else {
                    reportError("Unknown ecommerceHook.ruleType encountered", null, {hook: hook, apiKey: window.kuollApiKey});
                }
            });

            setupHooksOnPageVisit(pageHooks);
        });

    if (configuration.googleAnalyticsAutowiring) {
        proxyGtmDataLayer();
        proxyWhenReady(null, "ga", "loaded", null, function (args, result) {
            if (args[0] === "ec:setAction") {
                const action = args[1];

                const convertedToStep = gaActionToKuollStep(action);
                if (convertedToStep) {
                    tracker.ecommerce({
                        convertedToStep: convertedToStep
                    });
                }
            }
        });
    }

    const xhrConstructorOrig = window.XMLHttpRequest;
    window.XMLHttpRequest = function (parameters) {
        const xhr = new xhrConstructorOrig(parameters);

        try {
            if (configuration.magentoAutowiring) {
                const xhrOpenOrig = xhr.open;
                xhr.open = function () {
                    try {
                        const url = arguments[1];
                        if (url && typeof url === "string") {
                            xhrHooks.forEach(hook => {
                                if (pathsEqual(url, hook.filter)) {
                                    tracker.ecommerce({
                                        convertedToStep: hook.userStep
                                    });
                                }
                            });
                        }
                    } catch (e) {
                        reportError("Error in ConvMon proxy for XHR.open", e);
                    }

                    xhrOpenOrig.apply(xhr, arguments);
                };
            }

            xhr.__kuoll_stacktrace = xhr.__kuoll_stacktrace || utils.getStacktrace();

            xhr.addEventListener("readystatechange", function (event) {
                try {
                    if (xhr.readyState === 4 && xhr.status >= 400) {
                        tracker.xhrError(xhr);
                    }
                } catch (e) {
                    reportError("Error in ConvMon listener for XHR.readystatechange", e);
                }
            });

        } catch (e) {
            reportError("Error in ConvMon proxy for XHR constructor", e);
        }

        return xhr;
    };
    window.XMLHttpRequest.__kuoll_orig = xhrConstructorOrig;
    try {
        Object.getOwnPropertyNames(xhrConstructorOrig)
            .filter((name) => Object.getOwnPropertyDescriptor(xhrConstructorOrig, name).enumerable)
            .forEach((name) => window.XMLHttpRequest[name] = xhrConstructorOrig[name]);
    } catch (e) {
        logging.error("ConvMon could not proxy XMLHttpRequest due to error", e);
        window.XMLHttpRequest = xhrConstructorOrig;
    }

    ExceptionDetection.install();
    ExceptionDetection.subscribe((error) => {
        if (!error) return;
        if (!window.kuollApiKey && !Shopify.shop) return;

        const errorStack = utils.parseStacktrace(error);
        const errorStacktraceHash = utils.calculateStacktraceHash(errorStack, document.location.href);
        if (errorStacktraceHash) {
            tracker.error({
                hash: errorStacktraceHash,
                message: error ? error.message: null
            }, error);
        }

        if (!error.__kuoll_issue_created) {
            if (window[config.globalName] instanceof Function) {
                window[config.globalName]("createIssue", "JavaScript error", error.message, errorStack || utils.getStacktrace());
            } else {
                logging.log("Tried to automatically create issue, but window[config.globalName] is not a function");
            }
            error.__kuoll_issue_created = true;
        }
    });

}

export function ecommerce(event) {
    const isValidStepName = event && ("Visit" === event || "AddToCart" === event || "Checkout" === event || "Purchase" === event);
    if (!isValidStepName) {
        return false;
    }
    tracker.ecommerce({convertedToStep: event});
    return true;
}

export function xhrError(xhr) {
    tracker.xhrError(xhr);
}

export function resetRecordId() {
    tracker.cleanSession();
    tracker.start();
}

function setTrafficSourceNameInternally(trafficSourceName) {
    if (cookieStorage.get("kuollTrafficSourceName") === trafficSourceName) {
        return;
    }

    if (cookieStorage.has("kuollSessionDataSent")) {
        throw new Error("Traffic source may not be changed after any data has been sent to server. Consider setting traffic source immediately after loading bootloader.js");
    }

    cookieStorage.put("kuollTrafficSourceName", trafficSourceName);
}

function setLoggedInInternally(loggedIn) {
    if (cookieStorage.get("kuollLoggedIn") === loggedIn) {
        return;
    }

    if (cookieStorage.has("kuollSessionDataSent")) {
        throw new Error("LoggedIn flag may not be changed after any data has been sent to server. Consider setting the flag immediately after loading bootloader.js");
    }

    cookieStorage.put("kuollLoggedIn", loggedIn);
}

export function setTrafficSourceName(trafficSourceName) {
    if (tracker.isStarted()) {
        setTrafficSourceNameInternally(trafficSourceName);
    } else {
        presetTrafficSourceName = trafficSourceName;
    }
}

export function setLoggedIn(loggedIn) {
    if (tracker.isStarted()) {
        setLoggedInInternally(loggedIn);
    } else {
        presetLoggedInFlag = loggedIn;
    }
}

export function getTrafficSourceName() {
    return cookieStorage.get("kuollTrafficSourceName");
}