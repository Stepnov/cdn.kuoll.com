
export function getCookie(name) {
    let result = null;
    document.cookie.split(";").forEach(function (cookie) {
        const parts = cookie.split("=");
        if (decodeURIComponent(parts[0].trim()) === name) {
            result = decodeURIComponent(parts[1]);
        }
    });
    return result;
}                                                                                                           

export function CookieCrossOriginStorage(domain) {

    function buildCookieString(key, value) {
        return encodeURIComponent(key) + "=" + encodeURIComponent(value) + ";domain=" + domain + ";path=/";
    }

    this.put = function put(key, value) {
        document.cookie = buildCookieString(key, value);
    };

    this.append = function append(key, delimiter, value) {
        const newValue = (this.has(key) ? this.get(key)+delimiter : "") + value;
        this.put(key, newValue);
    };

    this.get = function get(key, defaultValue) {
        if (defaultValue === undefined)
            defaultValue = null;
        return this.has(key) ? getCookie(key) : defaultValue;
    };

    this.has = function has(key) {
        return getCookie(key) !== null;
    };

    this.del = function del(key) {
        document.cookie = buildCookieString(key, "") + ";expires=Thu, 01 Jan 1970 00:00:00 GMT";
    };

}