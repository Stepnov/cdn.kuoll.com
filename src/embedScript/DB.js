import reportError from "../common/reportError";
import * as utils from "../common/utils";
import * as logging from "../common/logging";
import treo from "../vendors/embedScript/treo";

let localDataStore;
let dbObj;

function dropOldKuollDbs(theRightName) {
    if(window.indexedDB && typeof window.indexedDB.webkitGetDatabaseNames === "function") {
        var request = window.indexedDB.webkitGetDatabaseNames();
        request.onsuccess = function () {
            var names = request.result;
            for (var i = 0; i < request.result.length; i++) {
                var name = names[i];
                if(name.startsWith("kuoll") && name !== theRightName && "kuoll-test" !== name) {
                    indexedDB.deleteDatabase(name);
                }
            }
        }
    }
}

export function init() {
    // TODO @Vlad check recordId consistency some other way.
    // E.g. create `recordId` store with a single {value:<recordId>} entry.
    // Or, create Stores with names like `localData-<recordId>`
    var dbName = "kuoll";
    dropOldKuollDbs(dbName);

    var schema = treo.schema().
        version(1)
            .addStore("localData", {increment: true})
            .addIndex("frameNum", "data.frameNum");

    dbObj = treo(dbName, schema);
    dbObj.version = 1;

    localDataStore = dbObj.store("localData");
}

function reportDbError(err, msg) {
    if (err) {
        reportError(msg, (err.target && err.target.error) ? err.target.error : err);
    }
}

export function add(data, onSuccess) {
    try {
        var timestamp = utils.currentTimestamp();
        localDataStore.put(undefined, {
            timestamp: timestamp,
            data: data
        }, function (err, key) {
            if (err) {
                reportDbError(err,
                    "Failed to localData.add. " +
                    "data.size=" + JSON.stringify(data).length);
            } else if (onSuccess) {
                onSuccess(err, key);
            }
        });
    } catch (e) {
        reportDbError(e, "Failed to localData.add");
    }
}

export function load(startKey, endKey, onAllProcessed, recordId) {
    try {
        var requests = [];

        var range = {gte: startKey};
        if (endKey) {
            range.lte = endKey;
        }

        localDataStore.cursor({
            range: range,
            iterator: function (cursor) {
                if (cursor.value.data.recordId === recordId) {
                    requests.push({
                        key: cursor.key,
                        value: cursor.value
                    });
                }
                cursor.continue();
            }
        }, function (err) {
            if (err) {
                reportDbError(err, "Failed to localData.load");
            }
            onAllProcessed(requests);
        });
    } catch (e) {
        reportDbError(e, "Failed to localData.load");
    }
}

/**
 * Used to remove entries one by one when submitting issues to the server
 * @param key
 * @param onSuccess
 */
export function del(key, onSuccess) {
    try {
        localDataStore.del(key, function (err) {
            reportDbError(err, "Failed to localData.del");
            if (!err && onSuccess) {
                onSuccess();
            }
        });
    } catch (e) {
        reportDbError(e, "Failed to localData.del");
    }
}

/**
 * Used before submitting issues to the server
 * @param endKey
 * @param onSuccess
 * @param recordId
 */
export function delBefore(endKey, onSuccess, recordId) {
    try {
        var requests = {};
        localDataStore.cursor({
            range: {lt: endKey},
            iterator: function (cursor) {
                if (cursor.value.data.recordId === recordId) {
                    requests[cursor.key] = null;
                }
                cursor.continue();
            }
        }, function () {
            localDataStore.batch(requests, function (err) {
                reportDbError(err, "Failed to localData.delBefore");
                if (!err && onSuccess) {
                    onSuccess();
                }
            });
        });
    } catch (e) {
        reportDbError(e, "Failed to localData.delBefore");
    }
}

export function clear(onSuccess) {
    try {
        if (dbObj) {
            dbObj.drop(function (err) {
                reportDbError(err, "Failed to localData.clear");
                if (!err && onSuccess) {
                    onSuccess();
                }
            });
        }
    } catch (e) {
        reportDbError(e, "Failed to localData.clear");
    }
}

/**
 * IndexedDB might be available but not work properly in Firefox private mode.
 * The method creates test database and performs some trivial operations on it
 * to check if IndexedDB works properly.
 */
export function isIndexedDbAvailable(callback) {
    function callCallback(result) {
        if (!callbackCalled) {
            callback(result);
            callbackCalled = true;
            window.removeEventListener("error", onError, true);
        }
    }
    var callbackCalled = false;

    var onError = function (e) {
        if (dbObj.status === "error") {
            e.ignoredError = true;
            callCallback(false);
        }
    };
    window.addEventListener("error", onError, true);

    setTimeout(function () {
        if (!callbackCalled) {
            reportError("Checking IndexedDB availability exceeded 10s timeout. Using in-memory storage");
            callCallback(false);
        }
    }, 10000);

    try {
        var schema = treo.schema().
            version(1)
            .addStore("store", {increment: true});

        dbObj = treo("kuoll-test", schema);
        dbObj.version = 1;

        var store = dbObj.store("store");
        store.put(null, "data", function (err, key) {
            if (err) {
                reportDbError(err);
                logging.log(err.target.error);
                callCallback(false);
            } else {
                store.get(key, function () {
                    callCallback(true);
                });
            }
        });
    } catch (e) {
        reportError("Error in DB.isIndexedDbAvailable ", e);
        callCallback(false);
    }
}