const requests = [];

export function addRequest(fnName, args, recordId, frameNum, callback) {
    var value = {
        fnName: fnName,
        frameNum: frameNum,
        recordId: recordId,
        args: args
    };
    var key = requests.length + 1;
    requests.push({
        key: key,
        value: {
            data: value
        }
    });
    if (callback) {
        callback(/* no error */ null, key);
    }
}

export function getRequests( startKey, endKey, callback, recordId) {
    var endIndex = endKey ? Math.min(endKey - 1, requests.length) : requests.length;
    callback(requests.slice(startKey - 1, endIndex).filter(function (request) { return request.value.data.recordId === recordId; }));
}

export function del(key) {
    delete requests[key - 1];
}

export function delBefore(key, callback) {
    requests.splice(0, key - 1);
    if (callback) {
        callback();
    }
}