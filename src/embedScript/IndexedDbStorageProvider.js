import * as db from "./DB";

export function addRequest(fnName, args, recordId, frameNum, callback) {
    db.add({
        fnName: fnName,
        frameNum: frameNum,
        recordId: recordId,
        args: args
    }, callback);
}

export function getRequests(startKey, endKey, callback, recordId) {
    db.load(startKey, endKey, callback, recordId);
}

export function del(key) {
    db.del(key);
}

export function delBefore(key, callback) {
    db.delBefore(key, callback);
}

