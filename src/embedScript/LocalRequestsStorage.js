import * as network from "../common/network";
import * as SequentQueue from "../common/SequentQueue";
import reportError from "../common/reportError";
import * as utils from "../common/utils";
import * as recording from "./embedFake";
import * as logging from "../common/logging";
import * as db from "./DB";
import * as IndexedDbStorageProvider from "./IndexedDbStorageProvider";
import * as InMemoryStorageProvider from "./InMemoryStorageProvider";
import * as ResourceLoader from "../common/resourceLoader";

const threeMinutes = 180000;

let frameNum, recordId;

var storageProvider;

let testingIndexedDb = false;

let dataPersistingStopped;

let requestsQueue = [];

let lastPersistedFrameNum = 0;

let fetchingDataToSend = false;
const issuesQueue = [];

const flushCallbacks = {
    "SequentQueue.add": function (request, nextRequestCallback) {
        var batchSent = SequentQueue.add(request.args[0]);
        if (batchSent) {
            network.addQueueEmptyListener(function (removeListener) {
                removeListener();
                nextRequestCallback();
            });
        } else {
            nextRequestCallback();
        }
    },
    "network.doPost": function (request, nextRequestCallback) {
        var args = request.args;
        var origCallback = args[2];
        args[2] = function () {
            try {
                if (origCallback !== undefined) {
                    origCallback.apply(arguments);
                }
            } finally {
                nextRequestCallback();
            }
        };

        network.doPost.apply(this, args);
    },
    "ResourceLoader.loadResourceNow": function (request, nextRequestCallback) {
        ResourceLoader.loadResourceNow.apply(this, request.args);
        nextRequestCallback();
    }
};

function addRequest(fnName, args, recordId, callback) {
    if (typeof recordId !== "number") {
        reportError("recordId must be provided");
        return;
    }

    if (storageProvider) {
        storageProvider.addRequest(fnName, args, recordId, frameNum, callback);
    } else {
        requestsQueue.push({
            fnName: fnName,
            args: args,
            callback: callback,
            recordId: recordId
        });
    }
}

export function saveSequent(sequent) {
    addRequest("SequentQueue.add", [sequent], sequent.recordId);
}

export function putBaseSnapshot(snapshot) {
    addRequest("network.doPost", ["putBaseSnapshot", snapshot], snapshot.recordId);
}

export function loadResource(url, origin, path, resourceType, isInline, resourceContent, frameNum) {
    addRequest("ResourceLoader.loadResourceNow", [url, origin, path, resourceType, isInline, resourceContent, frameNum], recordId);
}

function getFirstOldFrameKey() {
    var framesData = window.sessionStorage["kuoll-framesData"] ? JSON.parse(window.sessionStorage["kuoll-framesData"]) : [];
    var startTime = utils.currentTimestamp() - threeMinutes;
    for (var i = framesData.length - 1; i >= 0; --i) {
        var frame = framesData[i];
        if (frame.startTime < startTime) {
            return frame.startKey;
        }
    }
    return 1;
}

function deleteOldRequests() {
    var key = getFirstOldFrameKey();
    storageProvider.delBefore(key, null, recordId);
}

export function persistRequestsToServer(createIssueBody, onIssueCreatedCallback) {
    function callback() {
        delete window.sessionStorage["kuoll-issueBeingCreated"];
        if (onIssueCreatedCallback) {
            onIssueCreatedCallback.apply(this, arguments);
        }
        if (issuesQueue.length) {
            var issue = issuesQueue.shift();
            persistRequestsToServer.apply(this, issue);
        }
    }

    window.sessionStorage["kuoll-issueBeingCreated"] = JSON.stringify(createIssueBody);

    var startKey = Math.max((parseInt(window.sessionStorage["kuoll-lastPersistedRequestKey"]) + 1) || 0, getFirstOldFrameKey());
    if (!fetchingDataToSend) {
        flushLocalData(startKey, null, function () {
            SequentQueue.flush();
            network.doPost("createIssue", createIssueBody, callback);
        });
    } else {
        // Two issues created with small interval. In this case we cannot guarantee saving data without duplication,
        // so we wait until the last issue is persisted.
        issuesQueue.push(arguments);
    }
}

function flushLocalData(startKey, endKey, callback) {
    if(!storageProvider) {
        // dirty hack to make sure init() has happened
        setTimeout(function() {
            flushLocalData(startKey, endKey, callback)
        }, 200);
        return;
    }

    startKey = startKey || 1;
    lastPersistedFrameNum = (window.sessionStorage["kuoll-lastPersistedFrameNum"] || 0);

    fetchingDataToSend = true;
    storageProvider.getRequests(startKey, endKey, function (requests) {
        function processRequest() {
            function nextRequest() {
                storageProvider.del(request.key);
                ++index;
                setTimeout(function () {
                    processRequest();
                }, 0);
            }

            function validateRequest(request) {
                if (request.recordId !== recordId) {
                    reportError("Found request with unexpected recordId");
                    return false;
                }
                if (request.fnName === "network.doPost") {
                    if (request.args[0] === "saveFrame") {
                        if (request.frameNum > lastPersistedFrameNum) {
                            lastPersistedFrameNum = request.frameNum;
                            window.sessionStorage["kuoll-lastPersistedFrameNum"] = request.frameNum;
                        } else if (request.frameNum === lastPersistedFrameNum) {
                            reportError("Attempt to save same frame twice");
                            return false;
                        } else {
                            reportError("Wrong order of saving frames. frameNum must only increase");
                            return false;
                        }
                    }
                } else {
                    if (request.frameNum > lastPersistedFrameNum) {
                        reportError("Attempt to save data to frame which doesn't exist yet");
                        return false;
                    }
                }
                return true;
            }

            // all request processed
            if (dataPersistingStopped || finished || index >= requests.length) {
                if (!finished) {
                    finished = true;
                    if (storageProvider) {
                        storageProvider.delBefore(endKey, null, recordId);
                    }

                    // Callback should be passes to storageProvider.delBefore and be executed after
                    // persisted requests are deleted from IndexedDB. However, in some cases IndexedDB doesn't
                    // respond if the tab isn't active, causing too much of a delay. In order to call callback
                    // within a reasonable period we will call it as soon as all requests are sent, not waiting for
                    // storageProvider to clean up.
                    if (callback) {
                        callback();
                    }
                }
                return;
            }

            var request = requests[index];
            var requestData = request.value.data;

            startKey = request.key;

            if (!validateRequest(requestData)) {
                nextRequest();
                return;
            }

            var flushCallback = flushCallbacks[requestData.fnName];

            if (!flushCallback) {
                reportError("Unknown request type", null, requestData);
            } else {
                flushCallback(requestData, nextRequest);
            }
        }

        fetchingDataToSend = false;

        if (!endKey) {
            if (requests.length > 0) {
                endKey = requests[requests.length - 1].key;
            } else {
                endKey = 1;
            }
        }

        if (storageProvider) {
            storageProvider.delBefore(endKey, null, recordId);

            window.sessionStorage["kuoll-lastPersistedRequestKey"] = endKey;

            var index = 0, finished = false;
            processRequest();
        } else {
            // Recording was stopped while fetching data from DB. Just stop here, don't call callback
        }
    }, recordId);
}

export function saveFrame(frame) {
    addRequest("network.doPost", ["saveFrame", frame], frame.recordId, function (err, key) {
        var framesData = window.sessionStorage["kuoll-framesData"];

        if (!framesData) {
            framesData = [];
        } else {
            framesData = JSON.parse(framesData);
        }

        framesData.push({
            frameNum: frameNum,
            startKey: key,
            startTime: utils.currentTimestamp()
        });

        window.sessionStorage["kuoll-framesData"] = JSON.stringify(framesData);
    });
}

export function init() {
    var recordingInfo = recording.getRecordingInfo();
    frameNum = recordingInfo.frameNum;
    recordId = recordingInfo.recordId;

    db.init();

    testingIndexedDb = true;

    dataPersistingStopped = false;

    db.isIndexedDbAvailable(function (available) {
        testingIndexedDb = false;

        if (available) {
            storageProvider = IndexedDbStorageProvider;
            deleteOldRequests();
        } else {
            logging.warn("Kuoll can't use IndexedDB in this browser. Recording data will be cleared after " +
            "each page reloading. Probably you're using browser's private move. Use regular browsing mode please.");
            reportError("Recording in browser with disabled IndexedDB");
            storageProvider = InMemoryStorageProvider;
        }

        const issueBeingCreated = window.sessionStorage["kuoll-issueBeingCreated"];
        if (issueBeingCreated) {
            persistRequestsToServer(JSON.parse(issueBeingCreated));
        }

        if (requestsQueue.length) {
            for (var i = 0; i < requestsQueue.length; ++i) {
                var req = requestsQueue[i];
                addRequest(req.fnName, req.args, req.recordId, req.callback);
            }
            requestsQueue = [];
        }
    });
}

export function shutDown() {
    return new Promise((resolve) => {
        dataPersistingStopped = true;
        db.clear(resolve);
        storageProvider = null;
        recordId = null;
        frameNum = null;
    });
}

export function ignoreErrors() {
    return testingIndexedDb;
}