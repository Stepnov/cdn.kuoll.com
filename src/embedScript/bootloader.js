import config from "config";
import initPolyfill from "./polyfill";
import reportError, {init} from "../common/reportError";
import * as utils from "../common/utils";
import {setTimeout} from "../common/origObjects";
import * as ConversionMonitoring from "./ConversionMonitoring";
import {XMLHttpRequest} from "../common/origObjects";

if (window[config.globalName] && window[config.globalName].loaded) {
    // return;
} else if (!window.Promise) {
    // return; // IE 11 is not supported yet
} else {
    /**
     * JavaScript Kuoll API. Allows to handle recording process. May be accessed as <code>window.kuoll</code>
     * on any page that contains bootloader.js
     * @exports kuollAPI
     */
    var kuoll = function () {
        var methodName = arguments[0];
        var args = Array.prototype.slice.call(arguments, 1);
        return kuoll[methodName].apply(this, args);
    };
    kuoll.q = (window[config.globalName] ? window[config.globalName].q : null);
    window[config.globalName] = kuoll;

    initPolyfill();
    init(getRecordingInfo);

    var installingRecorder = false;
    var startingRecord = false;
    let recorderPreliminaryLoaded = false;
    let enableRecordingOnEmbedScriptLoad = false;

    var loadedAsync = !!window.kuoll;

    var kuollDisabled = false;

    var silentMode = false;

    var heartbeatIntervalId;

    function getRecordingInfo() {
        if ("kuollRecording" in window) {
            return kuollRecording.getRecordingInfo();
        } else {
            return null;
        }
    }

    function onBeforePageUnload() {
        delete localStorage["kuoll-activeTabHeartbeat"];
        localStorage["kuoll-lastPageUnloadTimestamp"] = new Date().getTime();
    }

    function validateAndNormalizeParams(params) {
        if (!params) {
            return "No start parameters specified. Check documentation at www.kuoll.com/quick-start.html";
        }

        if (typeof params === "string") {
            params = {
                API_KEY: params
            };
        } else {
            if (typeof params.customerId !== "undefined") { // for backward compatibility
                params.API_KEY = params.customerId;
            } else if (window.kuollApiKey) {
                params.API_KEY = window.kuollApiKey;
            } else if (!params.API_KEY && !params.userToken && !params.domain) {
                return "API_KEY must be specified. Visit www.kuoll.com/settings.html to find your key";
            }
        }

        params.ignoreUrls =
            typeof params.ignoreUrls === "array" ? params.ignoreUrls :
                typeof params.ignoreUrls === "string" ? [params.ignoreUrls] : [];


        params.localRecording = params.localRecording !== undefined ? params.localRecording : true;

        params.createIssueOn = params.createIssueOn || {};
        params.createIssueOn.consoleError = params.createIssueOn.consoleError !== undefined ? params.createIssueOn.consoleError :
            params.createIssueOnConsoleError !== undefined ? params.createIssueOnConsoleError : true;
        params.createIssueOn.error = params.createIssueOn.error !== undefined ? params.createIssueOn.error :
            params.createIssueOnError !== undefined ? params.createIssueOnError : true;
        params.createIssueOn.serverError = params.createIssueOn.serverError !== undefined ? params.createIssueOn.serverError : true;

        params.saveStaticResources = !!params.saveStaticResources;

        if (params.samplingFraction === true) {
            params.samplingFraction = 1;
        } else if (params.samplingFraction === false) {
            params.samplingFraction = 0;
        } else if (typeof params.samplingFraction === "undefined") {
            params.samplingFraction = 1;
        } else if (typeof params.samplingFraction !== "number" || params.samplingFraction < 0 || params.samplingFraction > 1) {
            return "params.samplingFraction may be only a number between 0 and 1 or boolean value";
        }

        return params;
    }

    function isRecordingPossible() {
        var ua = navigator.userAgent.toLowerCase();
        var ieVersion = (ua.indexOf('msie') != -1) ? parseInt(ua.split('msie')[1]) : false;
        if (ieVersion && ieVersion < 9) {
            reportError("Attempt to start recording in IE<9");
            return false;
        }

        try {
            window.sessionStorage["kuoll-testLocalStorage"] = 1;
            var data = window.sessionStorage["kuoll-testLocalStorage"];
            delete window.sessionStorage["kuoll-testLocalStorage"];
            if (data !== "1") {
                throw new Error();
            }
        } catch (e) {
            console.error("Cannot start recording. Probably you're using browser's private mode. It doesn't support technologies required by Kuoll. Use regular browsing mode please.");
            reportError("Attempt to start recording in browser without sessionStorage support", e);
            return false;
        }

        return true;
    }

    var finishCountdown = window.sessionStorage["kuoll-finishRecordInTime"];
    if (finishCountdown) {
        finishRecordInTime(finishCountdown);
    }

    function finishRecordInTime(seconds) {
        var finished = false;
        var countdownStartTime = Date.now();
        setTimeout(function () {
            finished = true;
        }, seconds * 1000);
        window.addEventListener("beforeunload", function () {
            if (!finished) {
                window.sessionStorage["kuoll-finishRecordInTime"] = seconds - (Date.now() - countdownStartTime / 1000);
            }
        });
    }

    kuoll.apiKey = function (apiKey) {
        window.kuollApiKey = apiKey;
        if (!recorderPreliminaryLoaded && !installingRecorder) {
            installRecorder(false);
        }
        return Promise.resolve(true);
    };

    kuoll.setTrafficSourceName = function (trafficSourceName) {
        return Promise.resolve(ConversionMonitoring.setTrafficSourceName(trafficSourceName));
    };

    kuoll.setLoggedIn = function (loggedIn) {
        return Promise.resolve(ConversionMonitoring.setLoggedIn(loggedIn));
    };

    /**
     * Turn Kuoll ON and start new record (if it is not started yet).
     *
     * @param {Object | String} params Object with API_KEY, User info (optional), User Organization info (optional). Or just string with your API_KEY.
     *
     * @param {string} [params.userId] id of User in your web application
     * @param {string} [params.userEmail] User email
     * @param {string} [params.orgId] User organization Id in your web application
     * @param {string} [params.orgName] User organization name in your web application
     *
     * @param {string} [params.API_KEY] Your company API_KEY
     *
     * @param {boolean} [params.saveStaticResources = false] If enabled, save a copy of all static resources to Kuoll. Affects network performance.
     *
     * @param {string} [params.notes] Any notes about the record
     *
     * @param {string | string[]} [params.ignoreUrls] an array of private URLs. XmlHttpRequests to these URL will not be recorded
     *
     * @param {boolean} [params.localRecording = false] Performance parameter. Persist records only when issue created.
     *
     * @param {object} [params.createIssueOn] Specifies in which cases Kuoll must create new issue.
     * @param {boolean} [params.createIssueOn.Error = true] if true, will create an issue whenever unhandled JS error occurs.
     * @param {boolean} [params.createIssueOn.consoleError = false] if true, will create an issue whenever error logged to console.error.
     * @param {boolean} [params.createIssueOn.serverError = false] if true, will create an issue whenever XHR responses with 500 error.
     *
     * @param {string} [params.kuollUserId] id of User given by Kuoll
     *
     * @param {number} [params.duration] record will be automatically finished after specified time in seconds
     *
     * @param {number} [params.sequentsBatchSize] amount of sequents that are accumulated before sent to server
     *
     * @param {boolean} [params.silentMode=false] if true, no recording messages will be written to console
     *
     * @param {number|boolean} [params.samplingFraction=1] fraction of users for which recording should be started. In other words, it's probability that this `startRecord` call will actually do anything. This doesn't take into account users for which recording cannot be started at all (due to unsupported browser or because recording is already started). So specified samplingFraction is applied only to the users that actually can be recorded. False is treated as 0 and true is treated as 1.
     *
     * @param {boolean} [params.enableDebug=false] enables debug logging in all environments
     *
     * @returns {Promise} Resolves to an object with info about started record if started successfully. Fails with an error message otherwise.
     */
    kuoll.startRecord = function startRecord (params) {
        return new Promise(function (resolve, reject) {
            if (kuollDisabled) reject("Kuoll is turned off");

            var startParams = JSON.stringify(params);
            params = validateAndNormalizeParams(params);
            if (typeof params === "string") {
                reportError(params);
                reject(params);
                return;
            }

            let apiKey = params.API_KEY || window.kuollApiKey;
            if (!window.kuollApiKey) {
                window.kuollApiKey = apiKey;
            }

            if (!isRecordingPossible()) {
                reject("Cannot record session in this browser");
                return;
            }

            // If recording is already being started, just keep current record
            if (startingRecord) {
                reject("Recording is already being started");
                return;
            }

            if ("kuollRecordingInfo" in sessionStorage) {
                restoreRecord();
                return;
            }

            var recordingActive = ("kuollRecording" in window) && !!kuollRecording.getRecordingInfo().active;
            if (recordingActive) {
                var recordInfo = JSON.parse(sessionStorage.kuollRecordingInfo);
                var oldStartParams = JSON.parse(recordInfo.startParams);
                if (recordInfo.apiKey === apiKey && (!params.userId || params.userId === oldStartParams.userId)) {
                    kuoll.getRecordInfo().then(function (info) {
                        resolve({
                            kuollUserId: info.userId,
                            recordCode: info.recordCode
                        });
                    }, reject);
                } else if (recordInfo.envSuffix === config.envSuffix) {
                    kuoll.stopRecord();
                    doStartRecordRequest();
                }
            } else if (localStorage["kuoll-activeTabHeartbeat"] > new Date().getTime() - 20000) {
                reject("There is another active record of this domain in another tab. Kuoll does not support simultaneous recording");
            } else if (installingRecorder) {
                var onEmbedScriptInitiated = function (e) {
                    var detail = e.detail;
                    var parsed = ((typeof detail) === "string") ? JSON.parse(detail) : detail;
                    if (parsed.action === "embedScriptInitiated") {
                        window.removeEventListener("kuollInterdom", onEmbedScriptInitiated);
                        var recordInfo = JSON.parse(sessionStorage.kuollRecordingInfo);
                        var oldStartParams = JSON.parse(recordInfo.startParams);
                        if (recordInfo.apiKey === apiKey && (!params.userId || params.userId === oldStartParams.userId)) {
                            kuoll.getRecordInfo().then(function (info) {
                                resolve({
                                    kuollUserId: info.userId,
                                    recordCode: info.recordCode
                                });
                            }, reject);
                        } else if (recordInfo.envSuffix === config.envSuffix) {
                            kuoll.stopRecord();
                            doStartRecordRequest();
                        }
                    }
                };
                window.addEventListener("kuollInterdom", onEmbedScriptInitiated);
            } else if (Math.random() > params.samplingFraction) {
                reject("Record was not started because it didn't fall into sampling fraction (params.samplingFraction=" + params.samplingFraction + ")");
            } else {
                doStartRecordRequest();
            }

            function doStartRecordRequest() {
                ConversionMonitoring.resetRecordId();
                
                const xhr = new XMLHttpRequest();
                xhr.open("POST", config.server + "startRecordImplicit");
                xhr.onerror = function () {
                    startingRecord = false;
                    reportError("Request for starting record failed");
                };
                xhr.onload = function () {
                    startingRecord = false;

                    if (xhr.status === 400) {
                        reject(xhr.responseText);
                        return;
                    }

                    var response = JSON.parse(xhr.responseText);

                    if (response.errorMsg) {
                        reportError(response.errorMsg);
                        reject(response.errorMsg);
                        return;
                    }

                    if (response.optout === true) {
                        var errorMsg = "Opt out is enabled, record wasn't started therefore";
                        reject(errorMsg);
                        return;
                    }

                    var userId = response.userId;
                    var recordId = response.recordId;
                    var recordCode = response.recordCode;

                    var recordingInfo = {
                        recordId: recordId,
                        recordCode: recordCode,
                        resourcesCachingInfo: {},
                        autonomousMode: params.saveStaticResources,
                        frameNum: 0,
                        active: true,

                        apiKey: apiKey,

                        kuollCustomerId: response.kuollCustomerId,

                        orgId: response.orgId,

                        userId: response.userId,

                        ignoreUrls: params.ignoreUrls.concat(response.ignoreUrls),

                        ignoreRules: response.ignoreRules || [],

                        localRecording: params.localRecording,

                        createIssueOn: params.createIssueOn,

                        sequentsBatchSize: params.sequentsBatchSize,
                        silentMode: params.silentMode,

                        startParams: startParams,

                        envSuffix: config.envSuffix,

                        enableDebug: params.enableDebug,

                        metaData: null
                    };
                    sessionStorage.kuollRecordingInfo = JSON.stringify(recordingInfo);

                    silentMode = !!params.silentMode;

                    if (params.duration) {
                        setTimeout(function () {
                            finishRecordInTime(params.duration);
                        })
                    }

                    installRecorder(true);

                    resolve({
                        kuollUserId: userId,
                        recordCode: recordCode
                    });
                };

                if ("withCredentials" in xhr) {
                    xhr.withCredentials = true;
                }

                startingRecord = true;

                xhr.send(JSON.stringify({
                    userToken: params.userToken,
                    apiKey: apiKey,

                    userId: params.kuollUserId,
                    externalUserId: params.userId,

                    orgId: params.orgId,
                    orgName: params.orgName,

                    autonomousMode: params.saveStaticResources,
                    notes: params.notes,

                    domain: params.domain || document.location.hostname,

                    userEmail: params.userEmail,

                    localRecording: params.localRecording
                }));
            }
        });
    };

    /**
     * Persists current record and marks current moment of the record as <strong>Issue</strong>.
     * Some issues are created automatically by unhandled <code>Error<code>s and <code>console.error</code> calls.
     *
     * @param {String} [type="error"] Type of created issue. Later you can group issues by type.
     * @param {String} [description] issue description.
     * @param {StackFrame[]} [stacktrace] issue stacktrace.
     *
     * @returns {Promise} Resolves to recording information is successfully created issue; Fails if recording isn't active
     */
    kuoll.createIssue = function (type, description, stacktrace) {
        return new Promise(function (resolve, reject) {
            if (kuollDisabled) {
                reject();
                return;
            }

            kuoll.isRecordActive().then((active) =>  {
                if (!description) {
                    reportError("Missing issue description");
                }
                window.dispatchEvent(new CustomEvent("kuollInterdom", {
                    "detail": {
                        action: active ? "createIssue" : "createIssueWithoutRecord",

                        type: type || "error",
                        description: description,
                        stacktrace: stacktrace
                    }
                }));
                resolve(window.kuollRecording ? kuollRecording.getRecordingInfo() : null);
            })
        });
    };

    /**
     * Alias for kuoll.createIssue(null, description, exception);
     *
     * @param {String} [description] issue description.
     * @param {StackFrame[] | Error} [exception] exception.
     *
     * @returns {Promise} Resolves to recording information is successfully created issue; Fails if recording isn't active
     */
    kuoll.notifyException = function (exception, description) {
        return kuoll.createIssue(null, description, exception);
    };

    /**
     * Attaches arbitrary object to current record. It should be used to store custom record or user-related data.
     *
     * Adding the same metadata key twice will result in overriding the former value. E.g. after two subsequent calls
     *
     * <code>
     *      kuoll.addMetaData({userId: 1});
     *      kuoll.addMetaData({userId: 2});
     * </code>
     *
     * userId will be 2.
     *
     * Be warn that merging previous metadata with new one is shallow meaning the following code
     *
     * <code>
     *      kuoll.addMetaData({user: {age: 20} });
     *      kuoll.addMetaData({user: {name: 'Stan'} });
     * </code>
     *
     * results in metadata being {user: {name: 'Stan'} } and user's age is overridden.
     *
     * Passing null as new metadata clears all previously saved data.
     *
     * Metadata is stored on remote server and needs to be serialized to JSON before sending. Serialization is
     * performed using JSON.stringify function. So consider its limitations before saving any metadata. For example,
     * functions can't be serialized properly.
     *
     * @param {Object} metadata arbitrary object containing any JSON-serializable data
     */
    kuoll.addMetaData = function (metadata) {
        return new Promise(function (resolve, reject) {
            if (kuollDisabled) {
                reject();
                return;
            }

            kuoll.isRecordActive((active) => {
                if (active) {
                    window.kuollRecording.updateMetaData(metadata);
                } else {
                    reject();
                }
            });
        });
    };

    function installRecorder(enableRecording, loadCallback) {
        function onLoaded() {
            if (loadCallback) {
                loadCallback();
            }

            var lastTimestamp;
            heartbeatIntervalId = setInterval(function () {
                var currentTimestamp = localStorage["kuoll-activeTabHeartbeat"];
                if (lastTimestamp && currentTimestamp && lastTimestamp !== parseInt(currentTimestamp)) {
                    if (!silentMode) {
                        console.log("[Kuoll] Another tab is recordings the same domain. Stopping recording");
                    }
                    kuoll.stopRecord();
                } else {
                    lastTimestamp = new Date().getTime();
                    localStorage["kuoll-activeTabHeartbeat"] = lastTimestamp;
                }
            }, 1000);

            window.addEventListener("beforeunload", onBeforePageUnload);
        }

        if (installingRecorder) {
            if (!enableRecordingOnEmbedScriptLoad && enableRecording) {
                enableRecordingOnEmbedScriptLoad = true;
            }
            return;
        }

        if ("kuollRecording" in window || recorderPreliminaryLoaded) {
            var event = new CustomEvent("kuollInterdom", {"detail": {action: "initEmbedScript"}});
            window.dispatchEvent(event);
            onLoaded();
        } else {                                                                             
            var s = document.createElement('script');
            s.src = config.resourcesServer + 'embedScript.js';
            if (loadedAsync) {                                                   
                s.async = 1;
            }
            s.onload = function () {
                this.parentNode.removeChild(s);
                if (installingRecorder) {
                    if (enableRecording || enableRecordingOnEmbedScriptLoad) {
                        window.dispatchEvent(new CustomEvent("kuollInterdom", {"detail": {action: "initEmbedScript"}}));
                        onLoaded();
                    } else {
                        recorderPreliminaryLoaded = true;
                    }
                    installingRecorder = false;
                }
            };
            installingRecorder = true;
            if (window[config.globalName]) {
                window[config.globalName].blockEmbedScriptExecution = false;
            }
            (document.head || document.documentElement).appendChild(s);
        }
    }

    function installInPageErrorInterceptor() {
        if (window.kuollInPageErrorInterceptor) {
            return;
        }

        const s = document.createElement('script');
        s.innerText = 'window.addEventListener("error", function (e) { window.dispatchEvent(new CustomEvent("kuollInPageError", {detail: {errorEvent: e}}))});';
        s.onload = function () {
            this.parentNode.removeChild(s);
            window.kuollInPageErrorInterceptor = true;
        };
        (document.head || document.documentElement).appendChild(s);
    }

    /**
     * Marks the record as finished and stops the recording.
     * @param {object} [params]
     * @param {string} [params.notes] User notes related to the record
     * @param {boolean} [params.restart = false] if true, current record will be finished and new one started.
     * @param {boolean} [params.dontSave = false] if true, current record will not be persisted to Kuoll servers and all already saved data will be erased.
     * @returns {Promise} Resolves if recording is successfully stopped; If params.restart is true, resolves with
     * a promise returned from kuoll.startRecord method. Fails with an error message if record cannot be stopped.
     */
    kuoll.stopRecord = function stopRecord(params) {
        return new Promise(function (resolve, reject) {
            if (kuollDisabled) {
                reject("Kuoll is turned off");
                return;
            }

            kuoll.isRecordActive().then(function (active) {
                if (!active) {
                    if (installingRecorder) {
                        installingRecorder = false;
                        window[config.globalName].blockEmbedScriptExecution = true;
                    }
                    reject("Recording isn't active");
                    return;
                }

                if (!("kuollRecording" in window)) {
                    resolve();
                    return;
                }

                params = params || {};

                var recordingInfo = kuollRecording.getRecordingInfo();

                try {
                    kuollRecording.finish(params.notes, params.dontSave).then(() => {
                        delete sessionStorage.kuollRecordingInfo;

                        window.clearInterval(heartbeatIntervalId);
                        onBeforePageUnload();
                        window.removeEventListener("beforeunload", onBeforePageUnload);

                        if (params.restart === true) {
                            resolve(kuoll.startRecord(JSON.parse(recordingInfo.startParams)));
                        } else {
                            resolve();
                        }
                    });
                } catch (e) {
                    reportError("Could not finish recording", e);
                }
            });
        })
    };

    /**
     * @deprecated
     * Stop recording and mark the record as finished. Placeholder for kuoll.stopRecord with no parameters.
     */
    kuoll.finishRecord = function finishRecord() {
        return kuoll.stopRecord();
    };

    /**
     * @returns {Promise} Resolves to recording information if it's available, fails otherwise
     */
    kuoll.getRecordInfo = function () {
        return new Promise(function (resolve, reject) {
            if ("kuollRecordingInfo" in sessionStorage) {
                var recording = JSON.parse(sessionStorage.kuollRecordingInfo);
                resolve(recording);
            } else {
                reject("Recording isn't active");
            }
        })
    };

    /**
     * Check if the recording is active.
     * @returns {Promise} Resolves to true, if the recording has been started and hasn't been stopped yet
     */
    kuoll.isRecordActive = function isRecordActive() {
        return new Promise(function (resolve, reject) {
            resolve(!kuollDisabled && ((("kuollRecording" in window) && !!kuollRecording.getRecordingInfo().active) || installingRecorder || startingRecord));
        });
    };

    /**
     * Updates custom data attached to current user.
     *
     * @param {String} [params.userId]
     * @param {String} [params.userEmail]
     * @param {String} [params.orgId]
     * @param {String} [params.orgName]
     */
    kuoll.setUserData = function setUserData(params) {
        return new Promise(function (resolve, reject) {
            if (!("kuollRecording" in window)) {
                reject("Recording isn't active");
                return;
            }

            var requestBody = {
                recordId: kuollRecording.getRecordingInfo().recordId,
                externalUserId: params.userId === null ? "" : params.userId,
                externalUserEmail: params.userEmail === null ? "" : params.userEmail,
                externalOrgId: params.orgId === null ? "" : params.orgId,
                externalOrgName: params.orgName === null ? "" : params.orgName
            };

            const xhr = new XMLHttpRequest();
            xhr.open("POST", config.server + "changeExternalUserInfo");
            xhr.onerror = function () {
                reportError("Request for changing user info failed");
            };
            xhr.onload = function () {
                resolve();
            };

            xhr.send(JSON.stringify(requestBody));
        })
    };

    /**
     * Finishes the record and starts new one for the same user.
     * @deprecated Use finishRecord instead
     */
    kuoll.restartRecord = function restartRecord() {
        return kuoll.stopRecord({restart: true});
    };

    /**
     * Deletes the record and stops the recording.
     * @deprecated Use finishRecord instead
     */
    kuoll.cancelRecord = function cancelRecord() {
        return kuoll.stopRecord({dontSave: true});
    };

    /**
     * Stacktrace hashing function is in process of refactoring. It will be moved to bootloader completely.
     * For now "reference implementation" is in Stacktrace.java. This function is not fully tested and is to be used
     * for debug purposes only
     */
    kuoll.calculateStacktraceHash = function (error, url) {
        return new Promise(function (resolve) {
            utils.parseStacktrace(error, function (stacktrace) {
                resolve(utils.calculateStacktraceHash(stacktrace, url));
            });
        })
    };

    kuoll.resetConversionMonitoring = function () {
        ConversionMonitoring.resetRecordId();
    };

    kuoll.ecommerce = function (event) {
        return ConversionMonitoring.ecommerce(event);
    };

    kuoll.track = function(event, rules) {
        if(!event || !rules) {
            return false;
        }
        for (var i = 0; i < rules.length; i++) {
            var rule = rules[i];
            if (rule.xhr) {
                
            }
        }
        return true;
    };

    function getParameterByName(name) {
        name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)");
        var results = regex.exec(location.search);
        return results == null ? null : decodeURIComponent(results[1].replace(/\+/g, " "));
    }

    let recordRestored = false;

    function restoreRecord() {
        if (recordRestored) {
            return;
        }
        recordRestored = true;

        const pageUnloadTimestamp = localStorage["kuoll-lastPageUnloadTimestamp"];
        const recordRestartThreshold = 1000 * 60 * 30;
        const wasRecordingInactiveForTooLong = pageUnloadTimestamp && (pageUnloadTimestamp < new Date().getTime() - recordRestartThreshold);
        let restartRecord = !!wasRecordingInactiveForTooLong;
        delete localStorage["kuoll-lastPageUnloadTimestamp"];      
        console.debug("restartRecord: ", restartRecord);

        if (restartRecord) {
            try {
                const recordingInfo = JSON.parse(sessionStorage["kuollRecordingInfo"]);
                const startParams = JSON.parse(recordingInfo.startParams);

                delete window.localStorage["kuoll-activeTabHeartbeat"];
                delete window.sessionStorage["kuollRecordingInfo"];
                delete window.sessionStorage["kuoll-framesData"];
                delete window.sessionStorage["kuoll-pendingFrames"];
                delete window.sessionStorage["kuoll-pendingSnapshots"];
                delete window.sessionStorage["kuoll-lastPersistedFrameNum"];
                delete window.sessionStorage["kuoll-sequentQueue"];

                kuoll.startRecord(startParams);
            } catch (e) {
                // If any of the parsed data is missing or isn't valid json, don't try to restart record
                restartRecord = false;
            }
        } else {
            installRecorder(true);
        }
    }


    (function () {
        kuollDisabled = (getParameterByName("disableKuoll") === "true");
        if (!kuollDisabled && "kuollRecordingInfo" in sessionStorage) {
            var recordingInfo = JSON.parse(sessionStorage["kuollRecordingInfo"]);
            var envSuffix = recordingInfo.envSuffix;
            if (config.envSuffix === envSuffix) {
                window.kuollApiKey = recordingInfo.apiKey;
                restoreRecord();
            }
        }

        window.addEventListener("kuollAPICall", function (e) {
            if (e.detail && e.detail.method && e.detail.params) {
                var method = window[config.globalName][e.detail.method];
                method.apply(window, e.detail.params);
            }
        });

        kuoll.loaded = true;


        // Proxy all Kuoll methods to log invocations and errors without boilerplate.
        // Also ensure that is one Kuoll method invokes another, only first one is logged.
        var invocationLogged = false;
        var keys = Object.keys(kuoll);
        for (var i = 0; i < keys.length; ++i) {
            var key = keys[i];
            var value = kuoll[key];
            if (value instanceof Function) {
                kuoll[key] = (function (key, origMethod) {
                    return function () {
                        var methodArgs = Array.prototype.slice.call(arguments);
                        if (!invocationLogged) {
                            if (!silentMode) {
                                if (methodArgs.length) {
                                    console.log.apply(console, ["[Kuoll] kuoll." + key + " was invoked with arguments:"].concat(methodArgs));
                                } else {
                                    console.log("[Kuoll] kuoll." + key + " was invoked.");
                                }
                            }
                            invocationLogged = true;
                        }
                        var result = origMethod.apply(kuoll, methodArgs);
                        if (result instanceof Promise) {
                            result = result.catch(function () {
                                var rejectionArgs = Array.prototype.slice.call(arguments);
                                if (!silentMode) {
                                    if (rejectionArgs.length) {
                                        console.warn.apply(console, ["[Kuoll] kuoll." + key + " failed with an error:"].concat(rejectionArgs));
                                    } else {
                                        console.warn("[Kuoll] kuoll." + key + " failed.")
                                    }
                                }
                                return rejectionArgs;
                            });
                        }
                        invocationLogged = false;
                        return result;
                    }
                }(key, value));
            }                                           
        }

        if (kuoll.q) {
            for (var i = 0; i < kuoll.q.length; ++i) {
                var task = kuoll.q[i];
                (function (task) {
                    // d.y - callback to fulfill Promise, d.n - reject.
                    kuoll.apply(this, task.a).then(function (v) {
                        task.d.y(v);
                    }, function (v) {
                        task.d.n(v);
                    });
                }(task));
            }
        }
    }());

    installInPageErrorInterceptor();

    if (window.kuollApiKey || Shopify.shop) {
        installRecorder(false);
    }

    ConversionMonitoring.init({
        magentoAutowiring: !window.kuollDisableMagentoAutowiring,
        googleAnalyticsAutowiring: !window.kuollDisableGoogleAnalyticsAutowiring
    });

}
