var config = {
    server: "http://localhost:8080/api/",
    resourcesServer: "http://localhost:9000/cdn/",
    envSuffix: "Dev",
    globalName: "kuoll",
    artifact: "embed",
    buildNumber: "@@BUILD_NUMBER"
};