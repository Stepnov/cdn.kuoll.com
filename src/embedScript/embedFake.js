import * as network from "../common/network";
import * as capturing from "../common/capturing";
import reportError from "../common/reportError";
import * as utils from "../common/utils";
import * as SequentQueue from "../common/SequentQueue";
import * as saving from "../common/saving";
import * as SequentSaver from "../common/SequentSaver";
import * as LocalRequestsStorage from "./LocalRequestsStorage";
import * as domTools from "../common/domTools";
import * as cycles from "../common/sequentCycles";

const RECORDING_INFO_KEY = "kuollRecordingInfo";

let recordingInfo = getInitialRecordingInfo();

function getInitialRecordingInfo() {
    return {
        orgId: null,
        recordCode: null,
        userId: null,
        recordId: null,
        frameNum: 1,
        active: false,
        resourcesCachingInfo: {},
        autonomousMode: false,
        silentMode: false,
        metaData: null
    };
}

function saveRecording() {
    if (RECORDING_INFO_KEY in sessionStorage) {
        sessionStorage[RECORDING_INFO_KEY] = JSON.stringify(recordingInfo);
    }
}

export function isActive() {
    return recordingInfo.active;
}

export function finish(notes, dontSave) {
    return new Promise((resolve, reject) => {
        try {
            var recordCode = recordingInfo.recordCode;
            SequentQueue.flush(function () {
                if (dontSave === true) {
                    network.doPost("deleteRecord", {
                        recordCode: recordCode
                    });
                } else {
                    network.doPost("finishRecord", {
                        recordCode: recordCode,
                        notes: notes,
                        finishType: "api"
                    });
                }

                network.addQueueEmptyListener(function () {
                    network.shutdown();
                })
            });

            capturing.shutdown();
            SequentSaver.clearBuffer();
            domTools.clearCache();
            delete window.sessionStorage[RECORDING_INFO_KEY];
            delete window.sessionStorage["kuoll-framesData"];
            delete window.sessionStorage["kuoll-pendingFrames"];
            delete window.sessionStorage["kuoll-pendingSnapshots"];
            delete window.sessionStorage["kuoll-lastPersistedFrameNum"];
            delete window.sessionStorage["kuoll-lastPersistedRequestKey"];
            delete window.sessionStorage["kuoll-sequentQueue"];
            LocalRequestsStorage.shutDown().then(() => {
                recordingInfo = getInitialRecordingInfo();
                resolve();
            });
        } catch (e) {
            reportError("Error while finishing recording", e);
            reject();
        }
    })
}

export function getRecordingInfo() {
    return recordingInfo;
}

export function init() {
    recordingInfo = JSON.parse(sessionStorage.kuollRecordingInfo);
    window.kuollRecording = this;
}

export function nextFrame() {
    SequentQueue.flush();
    recordingInfo.frameNum++;
    saveRecording();
    domTools.saveFrame();
}

export function updateMetaData(metadata) {
    let mergedMetadata;
    if (metadata === null) {
        mergedMetadata = null;
    } else {
        mergedMetadata = Object.assign(recordingInfo.metaData || {}, metadata);
    }
    recordingInfo.metaData = mergedMetadata;
    saveRecording();
    const serializedMetadata = mergedMetadata !== null ? JSON.stringify(mergedMetadata) : null;
    saving.saveMetaData(serializedMetadata);
}

export function saveSequent(sequent) {
    if (isActive()) {
        if (!capturing.isInitialSequentRecorded()) {
            capturing.recordInitialSequent();
        }
        cycles.uncycleSequent(sequent, function () {
            try {
                SequentSaver.save(sequent);
            } catch (e) {
                reportError("Sequent saving failed", e);
            }
        });
    }
}

function createIssueWithoutRecord(issue) {
    let stacktrace;
    if (issue.stacktrace instanceof Error) {
        stacktrace = utils.parseStacktrace(issue.stacktrace);
        return;
    } else if (issue.stacktrace) {
        stacktrace = issue.stacktrace;
    } else {
        stacktrace = utils.getStacktrace();
    }

    const description = utils.normalizeErrorDescription(issue.description);
    
    saving.createIssueWithoutRecord({
        apiKey: window.kuollApiKey,
        domain: Shopify.shop,
        userAgent: window.navigator.userAgent,
        type: issue.type,
        stacktrace: stacktrace,
        description: description,
    });
}

if (!window.kuollIssueListener) {
    window.kuollIssueListener = true;
    window.addEventListener("kuollInterdom", function (e) {
        const detail = e.detail;
        const parsed = ((typeof detail) === "string") ? JSON.parse(detail) : detail;
        if (parsed.action === "createIssue") {
            const recordingInfo = getRecordingInfo();

            let sequent;
            if (recordingInfo.active) {
                const description = utils.normalizeErrorDescription(parsed.description);
                sequent = capturing.recordBrowserEvent(null, {
                    type: "[createIssue]",
                    issueType: parsed.type,
                    description: description
                }, true);

                let stacktrace;
                if (parsed.stacktrace instanceof Error) {
                    stacktrace = utils.parseStacktrace(parsed.stacktrace);
                    return;
                } else if (parsed.stacktrace) {
                    stacktrace = parsed.stacktrace;
                } else {
                    stacktrace = utils.getStacktrace();
                }

                saving.createIssue({
                    recordCode: recordingInfo.recordCode,
                    type: parsed.type,
                    stacktrace: stacktrace,
                    description: description,
                    frameNum: recordingInfo.frameNum,
                    sequentNum: sequent ? sequent.sequentNum : 0
                });
            } else {
                createIssueWithoutRecord(parsed);
            }
        } else if (parsed.action === "createIssueWithoutRecord") {
            createIssueWithoutRecord(parsed);
        }
    });
}