import * as capturing from "../common/capturing";
import config from "config";
import initPolyfill from "./polyfill";
import * as utils from "../common/utils";
import * as StacktraceRestoreService from "../common/StacktraceRestoreService";
import * as network from "../common/network";
import * as recording from "./embedFake";
import * as logging from "../common/logging";
import * as ignoring from "../common/ignoring";
import * as saving from "../common/saving";
import * as ResourceLoader from "../common/resourceLoader";
import * as cycles from "../common/sequentCycles";
import * as SequentQueue from "../common/SequentQueue";
import * as LocalRequestsStorage from "./LocalRequestsStorage";
import * as reportError from "../common/reportError";

function init() {
    initPolyfill(); 

    utils.init();

    StacktraceRestoreService.init();

    network.init();

    recording.init();

    reportError.init(recording.getRecordingInfo);

    var recordingInfo = recording.getRecordingInfo();
    logging.init(recordingInfo.silentMode);

    ignoring.init();

    var localRecording = recordingInfo.localRecording;
    if (localRecording) {
        saving.enableLocalRecording();
        ResourceLoader.enableLocalRecording();
    }

    cycles.init();
    capturing.init();

    if (recordingInfo.active) {
        SequentQueue.init(recordingInfo.sequentsBatchSize);
        ResourceLoader.init();
        recording.nextFrame();
    }

    LocalRequestsStorage.init();

    if (recordingInfo.active) {
        capturing.recordInitialSequent();
    }
}

if (!window.kuollEmbedScript) {
    window.kuollEmbedScript = true;
    if (window[config.globalName].blockEmbedScriptExecution !== true) {
        window.addEventListener("kuollInterdom", function (e) {
            var detail = e.detail;
            var parsed = ((typeof detail) === "string") ? JSON.parse(detail) : detail;
            if (parsed.action === "initEmbedScript") {
                init();
                window.dispatchEvent(new CustomEvent("kuollInterdom", {"detail": {action: "embedScriptInitiated"}}));
            }
        });
    }
}
