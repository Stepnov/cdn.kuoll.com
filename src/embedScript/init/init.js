(function () {
    function init() {
        polyfill.init();

        origObjects.init();

        utils.init();
        StacktraceRestoreService.init();

        network.init();

        recording.init();

        var recordingInfo = recording.getRecordingInfo();
        logging.init(recordingInfo.silentMode);

        ignoring.init();

        var localRecording = recordingInfo.localRecording;
        if (localRecording) {
            saving.enableLocalRecording();
            ResourceLoader.enableLocalRecording();
        }

        cycles.init();
        capturing.init();

        if (recordingInfo.active) {
            SequentQueue.init(recordingInfo.sequentsBatchSize);
            ResourceLoader.init();
            recording.nextFrame();
        }

        LocalRequestsStorage.init();

        if (recordingInfo.active) {
            capturing.recordInitialSequent();
        }
    }

    if (window[config.globalName].blockEmbedScriptExecution !== true) {
        window.addEventListener("kuollInterdom", function (e) {
            var detail = e.detail;
            var parsed = ((typeof detail) === "string") ? JSON.parse(detail) : detail;
            if (parsed.action === "initEmbedScript") {
                init();
                window.dispatchEvent(new CustomEvent("kuollInterdom", {"detail": {action: "embedScriptInitiated"}}));
            }
        });
    }

}());
