function polyfillCustomEvent() {
    try {
        new window.CustomEvent('test');
        return;
    } catch (e) {
        // IE
    }

    function CustomEvent(event, params) {
        params = params || {};
        params.bubbles = params.bubbles === undefined ? false : params.bubbles;
        params.cancelable = params.cancelable === undefined ? false : params.cancelable;

        var evt = document.createEvent('CustomEvent');
        evt.initCustomEvent(event, params.bubbles, params.cancelable, params.detail);
        return evt;
    }

    CustomEvent.prototype = window.Event.prototype;
    window.CustomEvent = CustomEvent;
}

export default function init() {
    polyfillCustomEvent();
}