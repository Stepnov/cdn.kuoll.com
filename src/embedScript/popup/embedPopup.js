window.kuollPopup = (function () {

    var kuollTools;
    var showPopupBtn, startRecordBtn;
    var popupMenu;
    var submitRecordLink, openRecordLink, recordListLink, helpLink, restartLink, cancelRecordLink;

    var popupShown = false;
    var permanentPopup = false;

    var customerId, userId, recordCode;
    var iconPosition;

    function togglePopup() {
        if (popupShown) {
            popupMenu.style.display = "none";
        } else {
            popupMenu.style.display = "block";
        }
        popupShown = !popupShown;
    }

    function appendKuollTools(html, callback) {
        function initKuollTools() {
            document.body.appendChild(kuollTools);

            window.dispatchEvent(new CustomEvent("kuollInterdom", {detail: {action: "setKuollToolsNode"}}));

            showPopupBtn = document.getElementById("showPopupBtn");
            startRecordBtn = document.getElementById("startRecordBtn");
            popupMenu = document.getElementById("popupMenu");
            submitRecordLink = document.getElementById("submitRecordLink");
            openRecordLink = document.getElementById("openRecordLink");
            recordListLink = document.getElementById("recordListLink");
            helpLink = document.getElementById("helpLink");
            restartLink = document.getElementById("restartLink");
            cancelRecordLink = document.getElementById("cancelRecordLink");

            submitRecordLink.addEventListener("click", function (e) {
                window[config.globalName].stopRecord({
                    finishedWithPopup: true
                });
                e.preventDefault();
            });
            openRecordLink.addEventListener("click", function (e) {
                if (!recordCode) {
                    e.preventDefault();
                }
            });
            restartLink.addEventListener("click", function (e) {
                window[config.globalName].stopRecord({restart: true});
                e.preventDefault();
            });
            cancelRecordLink.addEventListener("click", function (e) {
                if (window.confirm("Are you sure that you want to cancel record? It will be deleted.")) {
                    window[config.globalName].stopRecord();
                    var XhrConstructor = (typeof window.XMLHttpRequest_orig !== "undefined" ? window.XMLHttpRequest_orig : window.XMLHttpRequest);
                    var xhr = new XhrConstructor();
                    xhr.open("POST", config.server + "deleteRecord");
                    xhr.send(JSON.stringify({
                        recordCode: recordCode
                    }));
                }
                e.preventDefault();
            });

            startRecordBtn.addEventListener("click", function (e) {
                window[config.globalName].startRecord({
                    customerId: customerId,
                    userId: userId,
                    iconPosition: iconPosition
                });
                togglePopup();
                startRecordBtn.style.display = "none";
                showPopupBtn.style.display = "initial";
                e.stopPropagation();
            });
            showPopupBtn.addEventListener("click", function (e) {
                togglePopup();
                e.stopPropagation();
            });

            if ("right" === iconPosition) {
                showPopupBtn.classList.add("kuollPopupBtnRight");
                startRecordBtn.classList.add("kuollPopupBtnRight");
                popupMenu.classList.add("popupMenuRight");
            }

            updateRecordCode(recordCode);

            callback();
        }

        kuollTools = document.createElement("div");
        kuollTools.setAttribute("id", "kuollTools");
        kuollTools.setAttribute("kuoll-ignore", "true");
        kuollTools.innerHTML = html;

        if (typeof document.body !== "undefined" && document.body !== null) {
            initKuollTools();
        } else {
            window.addEventListener("DOMContentLoaded", function () {
                initKuollTools();
            });
        }
    }

    function loadPopup(onAppended) {
        var XhrConstructor = (typeof window.XMLHttpRequest_orig !== "undefined" ? window.XMLHttpRequest_orig : window.XMLHttpRequest);
        var xhr = new XhrConstructor();
        xhr.open("GET", config.resourcesServer + "embedPopup.html");
        xhr.onload = function () {
            appendKuollTools(xhr.responseText, onAppended);
        };
        xhr.onerror = function () {
            console.error("Can not load embedPopup.html");
        };
        xhr.send();
        document.addEventListener("click", function (e) {
            if (popupShown) {
                togglePopup();
                e.stopPropagation();
            }
        });
    }

    function createStartBtn(_customerId, _userId, _iconPosition) {
        if (!_customerId) {
            throw new Error("customerId must be specified");
        }
        permanentPopup = true;
        customerId = _customerId;
        userId = _userId;
        iconPosition = _iconPosition;

        loadPopup(function () {
            showPopupBtn.style.display = "none";
            startRecordBtn.style.display = "initial";
        });
    }

    function createPopup(_customerId, _userId, _recordCode, _iconPosition) {
        if (!_userId) {
            throw new Error("userId must be specified to createPopup");
        }
        if (!_recordCode) {
            throw new Error("recordCode must be specified to createPopup");
        }
        customerId = _customerId;
        userId = _userId;
        recordCode = _recordCode;
        iconPosition = _iconPosition;

        if (typeof kuollTools !== "undefined") {
            if (kuollTools.style.display === "none") {
                kuollTools.style.display = "block";
            }
            updateRecordCode(recordCode);
            return;
        }
        document.addEventListener("click", function (e) {
            if (popupShown) {
                togglePopup();
                e.stopPropagation();
            }
        });

        loadPopup(function () {
            startRecordBtn.style.display = "none";
        });
    }

    function destroyPopup() {
        if (permanentPopup && startRecordBtn) {
            startRecordBtn.style.display = "initial";
            showPopupBtn.style.display = "none";
        } else if (kuollTools !== undefined) {
            kuollTools.parentNode.removeChild(kuollTools);
            kuollTools = null;
        }
    }

    function updateRecordCode(recordCode) {
        // TODO tech debt: remove it after all resources are saved on server
        var recordUrl = config.server.replace("https://", "http://") + "../play.html?recordCode=" + recordCode;
        openRecordLink.setAttribute("href", recordUrl);
        var recordsUrl = config.server + "../userRecords.html?orgId=" + customerId + "&userId=" + userId;
        recordListLink.setAttribute("href", recordsUrl);
        helpLink.setAttribute("href", config.server + "../help.html");
    }

    function recordIsFinished() {
        recordCode = null;
        updateRecordCode(null);
    }

    window.addEventListener("message", function (event) {
        if (event.origin + "/" !== config.resourcesServer && event.origin + "/" !== config.resourcesServer.replace("https://", "http://")) {
            return;
        }

        var data = JSON.parse(event.data);

        if (data.action === "updateIframeSize") {
            if (data.height) {
                popupIframe.style.height = data.height + parseInt(popupIframe.style.borderTopWidth) * 2
                    + parseInt(popupIframe.style.borderBottomWidth) * 2 + "px";
            }
        } else if (data.action === "finishRecord" || data.action === "restartRecord") {
            window[config.globalName].stopRecord({
                restart: (data.action === "restartRecord")
            });
        }
    });

    return {
        createPopup: createPopup,
        createStartBtn: createStartBtn,
        recordIsFinished: recordIsFinished,
        destroyPopup: destroyPopup
    };

})();