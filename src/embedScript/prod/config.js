var config = {
    server: "https://api.kuoll.com/api/",
    resourcesServer: "https://cdn.kuoll.com/",
    embedScript: "https://cdn.kuoll.com/embedScript.js",
    envSuffix: "Prod",
    globalName: "kuoll",
    artifact: "embed",
    buildNumber: "@@BUILD_NUMBER"
};