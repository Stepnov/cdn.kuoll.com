(function() {

    function storeBit(bit) {

    }

    function pickElt(path) {
        
        var i = 0;
        var retVal = 0;
        while (i < path.length) {
            var elt = path[i];
            if(
                "button" == elt.tagName ||
                ("input" == elt.tagName && "hidden" == elt.type)

            ) {

            }
        }

        return retVal;

    }

    function grabElt(elt) {
        var tagName = elt.tagName;
        var empty = document.createElement(tagName);

        var eltStyle = window.getComputedStyle(elt);
        var emptyStyle = window.getComputedStyle(empty);
        var styles = {};

        for (var i = 0; i < eltStyle.length; i ++) {
            var cssAttr = eltStyle[i];
            var val = eltStyle[cssAttr];
            if (val != emptyStyle[cssAttr]) {
                styles[cssAttr] = val;
            } 
        }
        
        return {
            style: styles,
            tagName: tagName,
            innerHTML: elt.innerHTML
        }
    }

    var lastElt = null;

    function relocate(info) {
        if (lastElt) {
            document.body.removeChild(lastElt);
        }
        var elt = document.createElement(info.tagName);
        elt.setAttribute("style", info.style);
        elt.style.position = "absolute";
        elt.style.left = "20px";
        elt.style.top = "20px";
        elt.style.zIndex = "999";
        elt.innerHTML = info.innerHTML;
        document.body.appendChild(elt);
        lastElt = elt;
    }

    function init() {
        window.addEventListener("click", function(e) {
            var target = e.toElement || e.srcElement;
            var info = grabElt(target);
            relocate(info);
            throw Error();
        });
    }

    init();
}());