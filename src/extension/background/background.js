(function () {

    "use strict";


    function activateRecording(userId, recordId, recordCode, autonomousMode, tabId) {
        recording.activateRecording(userId, recordId, recordCode, autonomousMode, tabId);
        network.doPost("changeRecordStatus", {
            recordCode: recordCode,
            status: "started"
        });
    }


    var csChannel = channel.create(channel.type.FROM_CONTENT_SCRIPT);
    csChannel.register(saving.saveSequent);
    csChannel.register(saving.saveBaseSnapshot);
    csChannel.register(saving.putRecordInfo);
    csChannel.register(saving.saveFrame);
    csChannel.register(saving.saveResource);
    csChannel.register(ResourceLoader.loadResource);
    csChannel.register(ResourceLoader.loadInlineResource);

    csChannel.register(function () {
        expectly.turn(arguments);
    }, "turn");
    csChannel.register(function () {
        expectly.enable(arguments);
    }, "enable");
    csChannel.register(expectly.expect, "expect");
    csChannel.register(expectly.happened, "happened");
    csChannel.register(expectly.ignore, "ignore");
    csChannel.register(expectly.unignore, "unignore");
    csChannel.register(expectly.finishTesting, "finishTesting");

    function getParameterByName(search, name) {
        name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(search);
        return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    }

    /**
     * Diff between onCommited vs onBeforeNavigate and onErrorOccurred events
     * @see https://developer.chrome.com/extensions/webNavigation#frame_ids
     */
    chrome.webNavigation.onCommitted.addListener(function (data) {
        if (data.frameId === 0 && data.tabId == recording.getRecordingInfo().tabId) {
            console.log("Injecting to " + recording.getRecordingInfo().tabId);

            SequentQueue.flush();
            recording.nextFrame();
            SequentQueue.init(10);
            ResourceLoader.init();
            chrome.tabs.executeScript(data.tabId, {
                file: "contentScript.js", allFrames: false, runAt: "document_start"
            }, function () {
                chrome.tabs.sendMessage(data.tabId, {
                    action: "recordingInfo",
                    recordingInfo: recording.getRecordingInfo()
                }, function () {
                    chrome.tabs.sendMessage(data.tabId, {
                        action: "gatherRecordInfo",
                        frameNum: recording.getRecordingInfo().frameNum
                    });
                });
            });

        } else if (data.url.indexOf(config.server + "activateRecording.html") !== -1) {
            try {
                var a = document.createElement("a");
                a.href = data.url;
                var search = a.search;

                var recordCode = getParameterByName(search, "recordCode");
                var userId = getParameterByName(search, "userId");
                var recordId = getParameterByName(search, "recordId");
                var redirectTo = getParameterByName(search, "redirectTo");
                var autonomousMode = ("true" == getParameterByName(search, "autonomousMode"));

                activateRecording(userId, recordId, recordCode, autonomousMode, data.tabId);

                chrome.tabs.update(data.tabId, {
                    url: redirectTo
                });
            } catch (error) {
                console.error(error);
                reportError("Error while interacting with activateRecording.html page", error);
            }
        }
    });

    chrome.runtime.onUpdateAvailable.addListener(function () {
        if (!recording.getRecordingInfo().active) {
            chrome.runtime.reload();
        }
    });

    chrome.tabs.onRemoved.addListener(function (tabId, removeInfo) {
        if (tabId == recording.getRecordingInfo().tabId && removeInfo.windowId == recording.getRecordingInfo().windowId) {
            recording.finish(true);
        }
    });

    chrome.tabs.onActivated.addListener(function (activeInfo) {
        if (activeInfo.tabId != recording.getRecordingInfo().tabId) {
            SequentQueue.flush();
        }
    });


    chrome.webNavigation.onBeforeNavigate.addListener(function (data) {
        if (data.frameId === 0 && data.tabId == recording.getRecordingInfo().tabId) {
            //TODO log navigation error
            console.log("chrome.webNavigation.onBeforeNavigate: " + JSON.stringify(data))
        }
    });

    chrome.webNavigation.onErrorOccurred.addListener(function (data) {
        if (data.frameId === 0 && data.tabId == recording.getRecordingInfo().tabId) {
            var errorData = "chrome.webNavigation.onErrorOccurred: " + JSON.stringify(data);
            reportError(errorData);
            console.log(errorData);
        }
    });

    chrome.runtime.onMessage.addListener(
        function (request, sender, sendResponse) {
            if (request.target == "background") {
                if (request.action == "clearRecordData") {
                    network.clearCommandQueue();
                    SequentSaver.clearBuffer();
                    chrome.browserAction.setBadgeText({text: ""})
                } else if (request.action == "reportError") {
                    reportError(request.error);
                } else if (request.action == "isAutonomous") {
                    sendResponse({
                        autonomousMode: recording.getRecordingInfo().autonomousMode
                    });
                }
            }
        });
}());
