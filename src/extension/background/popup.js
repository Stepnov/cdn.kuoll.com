"use strict";

// TODO minor priority: get rid of angular
var kuollPopupApp = angular.module('kuollPopupApp', []);

function startRecord(tab, autonomousMode) {
    var tabId = tab.id;
    var recordingInfo = recording.getRecordingInfo();
    if (recordingInfo.tabId) {
        if (recordingInfo.tabId === tabId) {
            window.alert("Kuoll is already recording this tab.");
        } else {
            window.alert("Kuoll is already recording another tab.");
        }
        return;
    }

    chrome.cookies.get({url: config.resourcesServer, name: "userToken"}, function (cookie) {
        if (cookie && cookie.value) {
            network.doPost("startRecord", {
                userToken: cookie.value,
                //autonomousMode: autonomousMode
                autonomousMode: false
            }, function (response) {
                response = JSON.parse(response);

                if (response.errorMsg) {
                    console.error(response.errorMsg);
                    chrome.tabs.create({url: config.resourcesServer + "../login.html"});
                    return;
                }

                var orgId = response.orgId;
                var userId = response.userId;
                var recordId = response.recordId;
                var recordCode = response.recordCode;
                if (!orgId || !recordCode || !userId || !recordId) {
                    throw new Error("Response from /saveRecord doesn't contains necessary data. userId=" +
                    userId + "; recordId=" + recordId + "; recordCode=" + recordCode);
                }

                recording.start(tab.windowId, tab.id, tab.index, orgId, userId, recordId, recordCode, autonomousMode, function () {
                    console.log("Injecting to " + tabId);
                    chrome.tabs.executeScript(tabId, {
                        file: "contentScript.js",
                        allFrames: false,
                        runAt: "document_start"
                    }, function () {
                        chrome.tabs.sendMessage(tabId, {
                            action: "recordingInfo",
                            recordingInfo: recording.getRecordingInfo()
                        }, function () {
                            chrome.tabs.sendMessage(tabId, {
                                action: "gatherRecordInfo",
                                frameNum: recording.getRecordingInfo().frameNum
                            });
                        });
                        chrome.browserAction.setBadgeText({text: "ON"});
                        chrome.browserAction.setBadgeBackgroundColor({color: "#c0392b"});
                    });
                });
            });
        } else {
            chrome.tabs.create({url: config.resourcesServer + "../signup.html"});
        }
    });


}

function finishRecord() {
    recording.finish();
    chrome.browserAction.setBadgeText({text: ""});
}


kuollPopupApp.controller("KuollExtensionPopupCtrl", ["$scope", "$q", function ($scope, $q) {

    $scope.recording = {
        active: false,
        recordCode: null,
        tabId: null,
        windowId: null,
        autonomousMode: false
    };

    $scope.tab = {};
    $scope.recordingLinkCopied = false;
    $scope.test = true;

    $scope.userLogged = true;

    $scope.startRecord = function () {

        if ($scope.recording.active) {
            throw Error("Recording is already ON");
        }

        console.log("Start record test session");


        chrome.tabs.query({active: true, currentWindow: true}, function (tabs) {
            if (tabs.length > 1) {
                throw new Error("More than one active tabs in current window");
            }
            if (tabs.length == 0) {
                return;
            }

            var tab = tabs[0];

            //startRecord(tab, true);
            startRecord(tab, $scope.recording.autonomousMode);
        });

    };

    $scope.finishRecord = function () {
        console.log("Stop record test session");
        finishRecord();
    };

    $scope.showRecordingTab = function () {
        var recordingInfo = recording.getRecordingInfo();
        chrome.tabs.update(recordingInfo.tabId, {active: true});
        chrome.windows.update(recordingInfo.windowId, {focused: true});
    };

    $scope.switchToTestTabAction = function () {
        var recordingInfo = recording.getRecordingInfo();
        chrome.tabs.query({active: true, currentWindow: true}, function (tabs) {
            if (tabs.length == 0) {
                chrome.tabs.create({url: config.resourcesServer + "../play.html?recordCode=" + recordingInfo.recordCode});
                return;
            }
            //else
            var tab = tabs[0];
            chrome.tabs.create({
                url: config.resourcesServer + "../play.html?recordCode=" + recordingInfo.recordCode,
                index: tab.index + 1
            });
        });
    };

    $scope.getRecordingLink = function () {
        if (!$scope.recording.recordCode) return;
        console.log("Get recording link");
        var area = document.getElementById("copyToClipboardTextarea");
        area.style.display = "block";
        var href = config.resourcesServer.replace("https://", "http://") + "../play.html?recordCode=" + $scope.recording.recordCode;
        chrome.tabs.create({
            url: href,
            index: ($scope.recording.tabIndex || 0) + 1
        });
        chrome.windows.update($scope.recording.windowId, {focused: true});

        area.innerText = href;
        area.select();
        document.execCommand("Copy", false, null);
        area.style.display = "none";
    };

    $scope.openRecordsList = function () {
        chrome.tabs.create({
            url: config.resourcesServer + "../records.html"
        });
    };

    $scope.toggleAutonomousMode = function ($event) {
        $scope.recording.autonomousMode = !$scope.recording.autonomousMode;
        $event.stopPropagation();
    };

    function checkUserToken() {
        chrome.cookies.get({url: config.resourcesServer, name: "userToken"}, function (cookie) {
            if (cookie && cookie.value) {
                network.doPost("../getUserId", {userToken: cookie.value}, function (response) {
                    response = JSON.parse(response);
                    if (response.errorMsg || typeof response.userId == "undefined" && !response.userId) {
                        updateUserLoggedFlag(false);
                        redirectToLoginOrSignupPage();
                    } else {
                        updateUserLoggedFlag(true);
                    }
                });
            } else {
                updateUserLoggedFlag(false);
                redirectToLoginOrSignupPage();
            }
        });
    }

    function redirectToLoginOrSignupPage() {
        var loginPageUrl = config.resourcesServer + "../login.html";
        var signupPageUrl = config.resourcesServer + "../signup.html";
        chrome.tabs.query({active: true, currentWindow: true}, function (tabs) {
            var tab = tabs[0];
            if (tab.url != loginPageUrl && tab.url != signupPageUrl) {
                chrome.cookies.get({url: config.resourcesServer, name: "visited"}, function (cookie) {
                   if (cookie && cookie.value && cookie.value == "1") {
                       chrome.tabs.create({url: loginPageUrl});
                   } else {
                       chrome.tabs.create({url: signupPageUrl});
                   }
                });
            }
        });
    }

    function updateUserLoggedFlag(newValue) {
        $scope.$apply(function () {
            $scope.userLogged = newValue;
        })
    }

    function updateRecordingView(newValue) {
        console.log("updateRecordingView");
        $scope.$apply(function () {
            $scope.recording = newValue;
            if ($scope.tab && $scope.recording && $scope.recording.active) {
                $scope.recordingThisTab = ($scope.tab && $scope.tab.id && $scope.tab.id == $scope.recording.tabId);
            }
        });
    }

    chrome.tabs.query({active: true, currentWindow: true}, function (tabs) {
        console.log("Got current tab");
        $scope.tab = tabs[0];
        recording.subscribeChanges(updateRecordingView);
    });

    checkUserToken();
}]);
