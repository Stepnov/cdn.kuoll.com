var channel = (function () {

    var type = {
        TO_CONTENT_SCRIPT: {
            delivery: deliveryFromInjected,
            side: false
        },
        FROM_INJECT_SCRIPT: {
            delivery: deliveryFromInjected,
            side: true
        },
        TO_BACKGROUND: {
            delivery: csDelivery,
            side: false
        },
        FROM_CONTENT_SCRIPT: {
            delivery: csDelivery,
            side: true
        }
    };

    function deliveryFromInjected(side) {

        function remote(name, params) {
            var event = new CustomEvent("kuollInterdom", {"detail": {name: name, params: params, side: side}});
            window.dispatchEvent(event);
        }

        var subscribers = [];

        function subscribe(fn) {
            subscribers.push(fn);
        }

        window.addEventListener("kuollInterdom", function (event) {
            var detail = event.detail;
            var parsed = ((typeof detail) === "string") ? JSON.parse(detail) : detail;

            if (parsed.side === side || "kuollInterdom" !== event.type) {
                return;
            }

            var params = parsed.params;
            for (var i in subscribers) {
                if (subscribers.hasOwnProperty(i)) {
                    subscribers[i](parsed.name, params);
                }
            }
        }, false);

        return {
            remote: remote,
            subscribe: subscribe
        };
    }


    function csDelivery(isBackground) {

        function remote(name, params) {
            var data = {name: name, params: params};

            if (isBackground) {
                chrome.tabs.sendMessage(recording.getRecordingInfo().tabId, data);
            } else {
                chrome.runtime.sendMessage(data);
            }
        }

        var subscribers = [];

        function subscribe(fn) {
            subscribers.push(fn);
        }

        if (isBackground) {
            chrome.runtime.onMessage.addListener(function (event) {
                if (event.outsideChannel) {
                    return;
                }

                for (var i in subscribers) {
                    if (subscribers.hasOwnProperty(i)) {
                        subscribers[i](event.name, event.params);
                    }
                }
            });
        }

        return {
            remote: remote,
            subscribe: subscribe
        };
    }

    function createChannel(type) {
        var delivery = type.delivery(type.side);

        var slots = {};

        function register(fn, name) {
            name = name || fn.name;
            if (!name) {
                throw new Error("Name of slot function is not defined: " + fn);
            }
            if (slots[name]) {
                throw new Error("Name is already used: " + fn);
            }
            slots[name] = fn;
        }

        function proxy(fn) {
            var name = typeof fn === "string" ? fn : fn.name;
            return function () {
                delivery.remote(name, Array.prototype.slice.call(arguments, 0));
            };
        }

        delivery.subscribe(function (name, params) {
            if (slots[name]) {
                slots[name].apply(null, params);
            }
        });

        return {
            register: register,
            proxy: proxy
        };
    }

    return {
        create: createChannel,
        type: type
    };

}());
