var saving, ResourceLoader, recording;
(function () {
    "use strict";

    var autonomousMode;
    var isActive = true;

    var recordingInfo;

    var injectScriptInit = false;

    var inChannel = channel.create(channel.type.FROM_INJECT_SCRIPT);
    var bgChannel = channel.create(channel.type.TO_BACKGROUND);

    saving = {
        saveSequent: bgChannel.proxy("saveSequent"),
        saveResource: bgChannel.proxy("saveResource"),
        saveFrame: bgChannel.proxy("saveFrame"),
        loadAndSaveResource: bgChannel.proxy("loadAndSaveResource"),
        loadAndSaveInlineResource: bgChannel.proxy("loadAndSaveInlineResource"),
        saveBaseSnapshot: bgChannel.proxy("saveBaseSnapshot"),
        putRecordInfo: bgChannel.proxy("putRecordInfo")
    };
    ResourceLoader = {
        loadResource: bgChannel.proxy("loadResource"),
        loadInlineResource: bgChannel.proxy("loadInlineResource")
    };
    recording = {
        getRecordingInfo: function () {
            return recordingInfo;
        }
    };

    inChannel.register(function (sequent) {
        if (isActive) {
            try {
                if (sequent.sequentSubtype === "[initialPageLoad]") {
                    if (typeof(autonomousMode) === "undefined") {
                        chrome.runtime.sendMessage({
                            outsideChannel: true,
                            target: "background",
                            action: "isAutonomous"
                        }, function (response) {
                            autonomousMode = response.autonomousMode;
                            if (autonomousMode) {
                                domTools.saveResources();
                            }
                        });
                    } else if (autonomousMode === true) {
                        domTools.saveResources();
                    }
                }
                saving.saveSequent(sequent);
            } catch (e) {
                console.warn("Can not save sequent because of error\n" + e.stack);
                reportError("Sequent saving failed", e);
            }
        }
    }, "saveSequent");

    inChannel.register(saving.saveBaseSnapshot, "saveBaseSnapshot");
    inChannel.register(saving.putRecordInfo, "putRecordInfo");

    if ("Dev" === config.envSuffix) {
        inChannel.register(function () {
            bgChannel.proxy("turn").apply(null, arguments);
        }, "turn");
        inChannel.register(function () {
            bgChannel.proxy("enable").apply(null, arguments);
        }, "enable");
        inChannel.register(function () {
            bgChannel.proxy("expect").apply(null, arguments);
        }, "expect");
        inChannel.register(bgChannel.proxy("happened"), "happened");
        inChannel.register(bgChannel.proxy("ignore"), "ignore");
        inChannel.register(bgChannel.proxy("unignore"), "unignore");
        inChannel.register(bgChannel.proxy("finishTesting"), "finishTesting");
    }

    function injectResultingActions() {
        var s = document.createElement('script');
        s.src = chrome.extension.getURL('injectScript.js');
        s.onload = function () {
            this.parentNode.removeChild(s);
            if (recordingInfo) {
                inChannel.proxy("setRecordingInfo")(recordingInfo);
                injectScriptInit = true;
            }
        };
        (document.head || document.documentElement).appendChild(s);
    }

    chrome.runtime.onMessage.addListener(
        function (request, sender, sendResponse) {
            if (request.action === "finishRecording") {
                isActive = false;
            } else if (request.action === "gatherRecordInfo") {
                inChannel.proxy("gatherRecordInfo")();
            } else if (request.action === "recordingInfo") {
                recordingInfo = request.recordingInfo;
                autonomousMode = recordingInfo.autonomousMode;
                if (!injectScriptInit) {
                    inChannel.proxy("setRecordingInfo")(recordingInfo);
                }
            }
            sendResponse();
        }
    );

    inChannel.register(reportError);

    domTools.saveFrame();
    injectResultingActions();

// TODO dk make it part of API
    if (document.getElementsByClassName("supportsKuoll").length > 0) {
        document.body.classList.add("isKuollRunning");
    }

}());