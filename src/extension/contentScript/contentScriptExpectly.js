(function () {

    "use strict";

    var resourceNum = 1;

    var inChannel = channel.create(channel.type.FROM_INJECT_SCRIPT);
    var bgChannel = channel.create(channel.type.TO_BACKGROUND);

    var saveResource = bgChannel.proxy("saveResource");
    var saveFrame = bgChannel.proxy("saveFrame");

    {
        inChannel.register(function (sequent) {
            if (sequent.sequentType == "mutation") {
                sequent.domSnapshot = domTools.getDomSnapshot();
            }
            bgChannel.proxy("saveSequent")(sequent);
        }, "saveSequent");
    }

    {
        inChannel.register(function() {
            bgChannel.proxy("turn").apply(null, arguments);
        }, "turn");
        inChannel.register(function () {
            bgChannel.proxy("enable").apply(null, arguments);
        }, "enable");
        inChannel.register(function() {
            bgChannel.proxy("expect").apply(null, arguments);
        }, "expect");
        inChannel.register(bgChannel.proxy("happened"), "happened");
    }

    if (window.qaToolInjectedTimes > 1) {
        alert("Injected more than once: " + window.qaToolInjectedTimes);
    }

    var domTools = {

        getWholeDom: function () {
            var $body = $(document.body).clone();
            $body.find("script").remove();
            $body.find("iframe,frame").attr("sandbox", "");
            $body.find("input").each(function (index, elt) {
                var $elt = $(elt);
                $elt.attr("value", $elt.val());
            });
            var bodyClasses = $body.attr("class");
            return "<body class='" + bodyClasses + "' id='" + $body.attr("id") + "' >" + $body.html() + "</body>";
        },

        getCss: function () {
            var styles = document.querySelectorAll("style, link[rel='stylesheet']");
            var str = "";
            for (var i = 0; i < styles.length; i++) {
                str += styles[i].outerHTML;
            }
            return str;
        },

        getDoctype: function () {
            var node = document.doctype;
            return "<!DOCTYPE "
                + node.name
                + (node.publicId ? ' PUBLIC "' + node.publicId + '"' : '')
                + (!node.publicId && node.systemId ? ' SYSTEM' : '')
                + (node.systemId ? ' "' + node.systemId + '"' : '')
                + '>';
        },

        getDomSnapshot: function () {
            var charset = (document.charset == "iso-8859-1") ? "utf-8" : document.charset;
            var snapshotData = {
                url: document.location.href,
                html: this.getWholeDom(),
                css: this.getCss(),
                base: document.baseURI,
                charset: charset,
                doctype: this.getDoctype()
            };
            return snapshotData;
        },

        saveDomSnapshot: function () {
            var snapshotData = this.getDomSnapshot();
            saveFrame(snapshotData);
        }
    };

    function injectResultingActions() {
        var s = document.createElement('script');
        s.src = chrome.extension.getURL('injectScript' + config.envSuffix + '.js');
        s.onload = function () {
            this.parentNode.removeChild(s);
        };
        (document.head || document.documentElement).appendChild(s);
    }

    injectResultingActions();

    $(function () {
        domTools.saveDomSnapshot();

        console.log("in window.addEventListener('load', function () {");
        var resourceTypes = {
            CSS: {
                header: "text/css,*/*;q=0.1",
                contentType: "text/css"

            }
        };

        function getResource(url, resourceType) {
            var xhr = new XMLHttpRequest();
            xhr.open("GET", url, true);
            xhr.setRequestHeader("Accept", resourceType.header);

            xhr.onerror = function () {
                reportError("Could not load resource. url: " + url);
            };
            xhr.onload = function (/*XMLHttpRequestProgressEvent*/event) {
                var headers = xhr.getAllResponseHeaders();
                var responseText = xhr.responseText;
                console.log("saving " + url);
                saveResource({
                    url: url,
                    inline: false,
                    contentType: resourceType.contentType,
                    headers: headers,
                    content: responseText
                });

                // Go to http://localhost:8080/loadResource?userId=1&resourceId=1&frameId=1&recordId=1&sequentId=1&url=http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css
                // after resource is saved
            };
            xhr.send();
        }

        function saveResources() {
            console.log("in saveResources");
            try {
                var links = document.getElementsByTagName("link");
                for (var i = links.length - 1; i >= 0; i--) {
                    var link = links.item(i);
                    var rel = link.getAttribute("rel");
                    rel = rel ? rel.toLowerCase() : rel;
                    if ("stylesheet" === rel && !link.kuollSaved) {
                        var href = link.getAttribute("href");
                        if (href.substr(0, 2) == "//") {
                            href = document.location.protocol + href;
                        } else if (href.substr(0, 1) == "/") {
                            href = document.location.protocol + "//" + document.location.host + href;
                        }
                        getResource(href, resourceTypes.CSS);
                        link.kuollSaved = true;
                    }
                }
                links = document.getElementsByTagName("style");
                for (var i = links.length - 1; i >= 0; i--) {
                    var htmlElement = links.item(i);
                    saveResource({
                        url: "",
                        inline: true,
                        contentType: "text/css",
                        headers: "",
                        content: htmlElement.innerText
                    });
                }
            } catch (e) {
                reportError("Error while parsing stylesheets", e);
            }
        }

        saveResources();
    });
})();

