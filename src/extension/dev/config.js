var config = {
    server: "http://localhost:8080/api/",
    resourcesServer: "http://localhost:9000/cdn/",
    envSuffix: "Dev",
    artifact: "extension",
    buildNumber: "@@BUILD_NUMBER"
};