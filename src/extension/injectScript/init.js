(function () {
    origObjects.init();
    utils.init();
    injectBridge.init(function () {
        ignoring.init();
        cycles.init();
        capturing.init();
        capturing.recordInitialSequent();
    });
}());
