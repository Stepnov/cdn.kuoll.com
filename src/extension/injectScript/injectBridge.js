var saving, expectly, reportError, recording;

var injectBridge = (function () {
    function init(onRecordingInfoReady) {
        var csChannel = channel.create(channel.type.TO_CONTENT_SCRIPT);

        var recordingInfo;
        csChannel.register(function (info) {
            recordingInfo = info;
            onRecordingInfoReady();
        }, "setRecordingInfo");

        reportError = csChannel.proxy("reportError");

        saving = {
            saveSequent: csChannel.proxy("saveSequent"),
            saveBaseSnapshot: csChannel.proxy("saveBaseSnapshot"),
            putRecordInfo: csChannel.proxy("putRecordInfo")
        };

        recording = {
            getRecordingInfo: function () {
                return recordingInfo;
            }
        };

        if ("Dev" === config.envSuffix) {
            expectly = window.expectly = {
                turn: csChannel.proxy("turn"),
                enable: csChannel.proxy("enable"),
                expect: csChannel.proxy("expect"),
                happened: csChannel.proxy("happened"),
                ignore: csChannel.proxy("ignore"),
                unignore: csChannel.proxy("unignore"),
                finishTesting: csChannel.proxy("finishTesting")
            };
        }
    }

    return {
        init: init
    };

}());
