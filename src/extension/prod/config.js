var config = {
    server: "https://api.kuoll.com/api/",
    resourcesServer: "https://www.kuoll.com/cdn/",
    envSuffix: "Prod",
    artifact: "extension",
    buildNumber: "@@BUILD_NUMBER"
};