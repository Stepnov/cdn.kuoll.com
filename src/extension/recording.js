var recording = (function () {
    "use strict";

    var recordingInfo = {
        windowId: null,
        tabId: null,

        orgId: null,
        recordCode: null,
        userId: null,
        recordId: null,
        frameNum: null,
        active: false,
        resourcesCachingInfo: {},
        autonomousMode: false,
        createIssueOn: {
            consoleError: false,
            error: true,
            serverError: true
        },
        localRecording: false
    };
    var loaded = false;


    function saveRecording(successFn) {
        var items = {recording: JSON.stringify(recordingInfo)};
        chrome.storage.local.set(items, function () {
            var lastError = chrome.runtime.lastError;
            if (lastError) {
                reportError("Got error while saving recording: " + lastError);
            } else {
                logging.log("Recording saved ok.");
                typeof successFn === "function" ? successFn() : 0;
            }
        });
    }

    function isActive() {
        return recordingInfo.active;
    }


    function start(windowId, tabId, tabIndex, orgId, userId, recordId, recordCode, autonomousMode, successFn) {
        if (isActive()) {
            throw new Error("Could not start recording: it is already active");
        }
        recordingInfo = {
            windowId: windowId,
            tabId: tabId,
            tabIndex: tabIndex,

            orgId: orgId,
            recordCode: recordCode,
            userId: userId,
            recordId: recordId,
            frameNum: 1,
            active: true,
            resourcesCachingInfo: {},
            autonomousMode: autonomousMode,
            createIssueOn: {
                consoleError: false,
                error: true,
                serverError: true
            },
            localRecording: false
        };

        network.init();

        saveRecording(successFn);
    }

    function activateRecording(orgId, userId, recordId, recordCode, autonomousMode, tabId) {
        chrome.tabs.get(tabId, function (tab) {
            recordingInfo = {
                windowId: tab.windowId,
                tabId: tab.id,
                tabIndex: tab.index,
                recordCode: recordCode,
                orgId: orgId,
                userId: userId,
                recordId: recordId,
                frameNum: 0,
                active: true,
                resourcesCachingInfo: {},
                autonomousMode: autonomousMode,
                createIssueOn: {
                    consoleError: false,
                    error: true,
                    serverError: true
                },
                localRecording: false
            };
        });
    }

    function finish(tabClosed) {
        if (!isActive()) {
            throw new Error("Could not finish recording: it is not active");
        }

        network.doPost("finishRecord", {
            recordCode: recording.getRecordingInfo().recordCode,
            finishType: tabClosed ? "tabClosed" : "extensionPopup"
        });
        chrome.runtime.sendMessage({
            outsideChannel: true,
            target: "background",
            action: "clearRecordData"
        });
        chrome.tabs.sendMessage(recordingInfo.tabId, {
            action: "finishRecording"
        });
        recordingInfo.windowId = null;
        recordingInfo.tabId = null;
        recordingInfo.active = false;
        recordingInfo.resourcesCachingInfo = {};
        recordingInfo.autonomousMode = false;
        saveRecording();
    }

    var listeners = [];

    function subscribeChanges(listener) {
        if (loaded) {
            listener(recordingInfo);
        }
        listeners.push(listener);
    }

    function notifyListeners() {
        for (var i in listeners) {
            if (listeners.hasOwnProperty(i)) {
                listeners[i](recordingInfo);
            }
        }
    }

    chrome.storage.onChanged.addListener(function (changes, areaName) {
        var recordingChanges = changes["recording"];
        if ("local" == areaName && recordingChanges) {
            recordingInfo = JSON.parse(recordingChanges.newValue);
            loaded = true;
            notifyListeners();
        }
    });

    function loadRecording() {
        var updateRecordingInfo = function (value) {
            var lastError = chrome.runtime.lastError;
            if (lastError) {
                reportError(lastError);
            }

            if (value.recording) {
                recordingInfo = JSON.parse(value.recording);
            }
            loaded = true;
            notifyListeners();
        };
        chrome.storage.local.get("recording", updateRecordingInfo);
    }

    function getRecordingInfo() {
        return recordingInfo;
    }

    function nextFrame() {
        recordingInfo.frameNum++;
        saveRecording();
    }

    loadRecording();

    return {
        isActive: isActive,
        start: start,
        finish: finish,
        nextFrame: nextFrame,
        getRecordingInfo: getRecordingInfo,
        subscribeChanges: subscribeChanges,
        activateRecording: activateRecording
    }
})();