(function () {

    function getXHRConstructor() {
        if (typeof window.XMLHttpRequest_orig != "undefined") {
            return window.XMLHttpRequest_orig;
        } else if (typeof XMLHttpRequest != "undefined") {
            return XMLHttpRequest;
        } else if (typeof window.XMLHttpRequest != "undefined") {
            return window.XMLHttpRequest;
        } else {
            throw new Error("Can not find XMLHttpRequest constructor")
        }
    }

    function startRecording() {
        kuoll.startRecord({orgWebDomain: window.location.hostname, customerWebDomain: "pipedrive.com"}); //FIXME save userId even if not provided
        hideAll();
        setDisplay(document.getElementById("kuollRecordStateStarting"), true);
        setTimeout(function() {
            hideAll();
            setDisplay(document.getElementById("kuollRecordStateStarted"), true);
        }, 1000);
    }

    function finishRecording() {
        kuoll.stopRecord();
        hideAll();
        setDisplay(document.getElementById("kuollRecordStateFinishing"), true);
        setTimeout(function() {
            hideAll();
            setDisplay(document.getElementById("kuollRecordStateFinished"), true);
        }, 1000);
    }

    function hideAll() {
        setDisplay(document.querySelectorAll(".kuollRecordState"), false);
    }
    function setDisplay(elt, display) {
        if (elt.length) {
            for (var i = elt.length - 1; i >= 0; i--) {
                var e = elt[i];
                e.style.display = (display ? "block" : "none");
            }
        } else {
            elt.style.display = (display ? "block" : "none");
        }
    }

    function catchModalWindowCreating(mutations) {
        if (!mutations.length) {
            return;
        }
        if (mutations[0].type == "attributes" && mutations[0].attributeName == "data-content") {
            return;
        }

        for (var i = 0; i < mutations.length; ++i) {
            var mutation = mutations[i];
            var addedNodes = mutation.addedNodes;
            if (addedNodes) {
                for (var j = 0; j < addedNodes.length; ++j) {
                    var addedNode = addedNodes[j];
                    if (addedNode && addedNode instanceof HTMLElement) {
                        var supportForm = addedNode.querySelector("#feedbackForm") || addedNode.querySelector("#helpFeedbackForm");
                        if (supportForm) {
                            if (document.getElementsByClassName("kuollRecordControls").length == 0) {
                                createUI(supportForm);
                            }
                        }
                    }
                }
            }
        }
    }

    function createUI(supportForm) {
        var xhr = new (getXHRConstructor());
        xhr.open("GET", "chrome-extension://fondeameenbmkaaclcffbodlpnieekhd/pipedriveSupportMenu.html");
        xhr.onload = function () {
            var html = xhr.responseText;
            var recordingBox = document.createElement("div");
            recordingBox.classList.add("kuollRecordControls");
            recordingBox.innerHTML = html;

            if (supportForm.nextSibling) {
                supportForm.parentNode.insertBefore(recordingBox, supportForm.nextSibling);
            }
            else {
                supportForm.parentNode.appendChild(recordingBox);
            }

            hideAll();

            var btnStartRecord = recordingBox.querySelector("#kuollBtnStartRecord");
            btnStartRecord.addEventListener("click", startRecording);
            setDisplay(recordingBox.querySelector("#kuollRecordStateStart"), !kuoll.isRecordActive());

            var btnFinishRecord = recordingBox.querySelector("#kuollBtnFinishRecord");
            btnFinishRecord.addEventListener("click", finishRecording);
            setDisplay(recordingBox.querySelector("#kuollRecordStateFinish"), kuoll.isRecordActive());
        };

        xhr.send();



    }

    (function () {
        var appendNode = document.querySelector("#feedbackForm") || document.querySelector("#helpFeedbackForm");
        if (appendNode != null) {
            createUI(appendNode);
        } else {
            var mutationObserver = new MutationObserver(catchModalWindowCreating);

            var domObserverConfig = {
                attributes: true,
                childList: true,
                subtree: true
            };

            mutationObserver.observe(document, domObserverConfig);
        }
    })();
})();
