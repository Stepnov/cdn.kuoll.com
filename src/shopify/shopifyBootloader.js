import config from "config";

if (!Shopify || !Shopify.shop) {
    console.warn("[Kuoll] This site doesn't look like Shopify shop. Expecting `Shopify.shop` property to identify Kuoll customer");
} else {

    (function (w, k, t) {
        w[k] = w[k] || function () {
            const a = arguments;
            return new Promise(function (y, n) {
                (w[k].q = w[k].q || []).push({a: a, d: {y: y, n: n}});
            });
        };

        const s = document.createElement(t),
            f = document.getElementsByTagName(t)[0];
        s.async = 1;
        s.src = 'https://359afad9.ngrok.io/bootloader.js';
        f.parentNode.insertBefore(s, f);
    }(window, 'kuoll', 'script'));

    kuoll('startRecord', {
        domain: Shopify.shop
    });
}
