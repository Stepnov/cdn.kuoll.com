(function () {

    'use strict';

    config = {}; // script config

    recording = {}; // current recording settings, all modules should use the settings directly

    function reportError() { // plain method, use it instead of console.*
    }

    var xhr; // same for other vars

    var module = (function () { // for each module have this structure
        var someInnerModuleVar;

        return {
            publicMethod: publicMethod,
            init: init, // called on web page (and embedScript) reloading
            clean: clean // called on record stop
        };
    }());

    function substituteVars() {
        window.xhr_orig = window.xhr;
        window.xhr = proxy(window.xhr);
    }

    xhr = window.xhr_orig;

    start();

}());
