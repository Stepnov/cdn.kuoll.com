
function intallFunnelListeners(rules) {
    function absoluteUrl(url) {
        var link = document.createElement("a");
        link.href = url;
        return link.href;
    }

    function checkXhrUrl(url) {
        let endPoints = config.xhrRules;
        var absUrl = absoluteUrl(url);
        for (var i = 0; i < rules.length; i++) {
            if (absUrl.indexOf(endPoints[i].filter) >= 0) {
                tracker.ecommerce({
                    convertedToStep: endPoint.advancement
                });
                // process all events
            }
        }
    }

    function installXhrRules(xhrRules) {
    }

    function onXhr(xhrRules, url) {
        var absUrl = absoluteUrl(url);
        xhrRules.map((rule) => {
            if (new RegExp(rule.filter).test(absUrl)) {
                tracker.ecommerce({
                    convertedToStep: rule.step
                });
            }
        });
    }

    function installPageRules(pageRules) {
        function onNavigation() {
            var absUrl = absoluteUrl(window.location.href);
            pageRules.map((rule) => {
                if (new RegExp(rule.filter).test(absUrl)) {
                    tracker.ecommerce({
                        convertedToStep: rule.step
                    });
                }
            });
        }

        var pushState_orig = history.pushState;
        history.pushState = function () {
            pushState_orig.apply(history, arguments);
            onNavigation();  // Some event-handling function
        };
        var replaceState_orig = history.pushState;
        history.replaceState = function () {
            replaceState_orig.apply(history, arguments);
            onNavigation();  // Some event-handling function
        };
        onNavigation();
    }



    function installClickRules(clickRules) {
        document.addEventListener("click", function(e) {
            for (var i = 0; i < clickRules.length; i++) {
                var target = e.target;
                if (target.closest(clickRules[i].filter)) {
                    tracker.ecommerce({
                        convertedToStep: clickRules[i].step
                    });
                }
            }
        });
    }

    installXhrRules(rules.filter((rule) => "xhr" == rule.ruleType));
    installPageRules(rules.filter((rule) => "page" == rule.ruleType));
    installClickRules(rules.filter((rule) => "click" == rule.ruleType));
};


