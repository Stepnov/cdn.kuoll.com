const webpack = require('webpack');
const path = require('path');
const WebpackAutoInject = require('webpack-auto-inject-version');

module.exports = {
    context: path.join(__dirname, '../'),
    entry: {
        embedScript: './src/embedScript/embedScript.js',
        bootloader: './src/embedScript/bootloader.js',
        shopifyBootloader: './src/shopify/shopifyBootloader.js',
    },

    output: {
        path: path.join(__dirname, './../build/cdn'),
        filename: '[name].js',
    },

    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: {                                          
                        presets: ['env'],
                    }
                },
            },
        ],
    },

    resolve: {
        alias: {
            config: path.join(__dirname, '../src/config', process.env.NODE_ENV)
        }
    },

    plugins: [
        new webpack.EnvironmentPlugin([
            'NODE_ENV'
        ]),
        new WebpackAutoInject({
            components: {
                AutoIncreaseVersion: false,
                InjectAsComment: false
            }
        }),
    ],
};