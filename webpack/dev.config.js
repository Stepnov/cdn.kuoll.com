const merge = require('webpack-merge');
const baseConfig = require('./base.config.js');
const path = require("path");

module.exports = merge(baseConfig, {
    devtool: 'eval-source-map',

    devServer: {
        port: '9001',
        hot: false,
        inline: false
    },

    module: {
    },

    plugins: [
    ]

});