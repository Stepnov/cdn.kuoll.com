const webpack = require('webpack');
const merge = require('webpack-merge');
const baseConfig = require('./base.config.js');
const path = require("path");
const WebpackAutoInject = require('webpack-auto-inject-version');

module.exports = merge(baseConfig, {
    entry: {
        monitoring: './src/embedScript/bootloader.js',
    },

    module: {
        rules: [
        ],
    },

    plugins: [
        // Minify CSS
        new webpack.LoaderOptionsPlugin({
            minimize: true,
        }),
        new WebpackAutoInject({
            components: {
                AutoIncreaseVersion: true
            }
        })
    ],
});